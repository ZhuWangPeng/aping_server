/*
Navicat MySQL Data Transfer

Source Server         : aping
Source Server Version : 80017
Source Host           : 127.0.0.1:3306
Source Database       : aping

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2022-11-22 01:36:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin_role`;
CREATE TABLE `sys_admin_role` (
  `admin_id` varchar(20) NOT NULL,
  `role_id` varchar(20) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_admin_role
-- ----------------------------
INSERT INTO `sys_admin_role` VALUES ('102122', 'ROLE00');

-- ----------------------------
-- Table structure for sys_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `func_id` varchar(20) NOT NULL COMMENT '菜单id',
  `parent_func_id` varchar(20) DEFAULT NULL COMMENT '父菜单id',
  `func_name` varchar(20) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '菜单url',
  `sort` varchar(3) DEFAULT NULL COMMENT '排序',
  `is_menu` varchar(1) DEFAULT NULL COMMENT '是否菜单 0-否 1-是',
  PRIMARY KEY (`func_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_function
-- ----------------------------
INSERT INTO `sys_function` VALUES ('FUNC01000', null, '首页', '/rights/homePage/query', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC02000', null, '商品管理', '/rights/goods/goods-manange', '2', '1');
INSERT INTO `sys_function` VALUES ('FUNC02001', 'FUNC02000', '广告栏位', '/rights/goods/goods-advertisement', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC02002', 'FUNC02000', '商品种类', '/rights/goods/goods-kind-query', '2', '1');
INSERT INTO `sys_function` VALUES ('FUNC02003', 'FUNC02000', '商品信息', '/rights/goods/goods-info-query', '3', '1');
INSERT INTO `sys_function` VALUES ('FUNC03000', null, '订单管理', '/rights/order/order-manange', '3', '1');
INSERT INTO `sys_function` VALUES ('FUNC03001', 'FUNC03000', '订单管理', '/rights/order/order-manange-query', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC03002', 'FUNC03000', '发货题型', '/rights/order/delivery-reminder', '2', '1');
INSERT INTO `sys_function` VALUES ('FUNC04000', null, '用户管理', '/rights/admin/admin-manange', '4', '1');
INSERT INTO `sys_function` VALUES ('FUNC04001', 'FUNC04000', '用户管理', '/rights/admin/admin-manange-query', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC04002', 'FUNC04000', '操作记录', '/rights/admin/operate-record', '2', '1');
INSERT INTO `sys_function` VALUES ('FUNC05000', null, '流水记录', '/rights/flow/flow-record', '5', '1');
INSERT INTO `sys_function` VALUES ('FUNC05001', 'FUNC05000', '充值流水', '/rights/running/recharge-flow', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC05002', 'FUNC05000', '订单流水', '/rights/running/order-flow', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC05003', 'FUNC05000', '储值流水', '/rights/running/stored-flow', '1', '1');
INSERT INTO `sys_function` VALUES ('FUNC05004', 'FUNC05000', '返利流水', null, '1', '1');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` varchar(10) NOT NULL COMMENT '角色id',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `role_memo` varchar(100) DEFAULT NULL COMMENT '角色备注',
  `inser_time` varchar(20) DEFAULT NULL COMMENT '写入时间',
  `lst_update_time` varbinary(20) DEFAULT NULL COMMENT '最后一次修改时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('ROLE00', '超级管理员', '拥有所有权限', '2022-09-18 23:30:20', 0x323032322D30392D31382032333A33303A3230);
INSERT INTO `sys_role` VALUES ('ROLE01', '管理员', '拥有部分权限', '2022-09-18 23:30:20', 0x323032322D30392D31382032333A33303A3230);

-- ----------------------------
-- Table structure for sys_role_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_function`;
CREATE TABLE `sys_role_function` (
  `role_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色id',
  `func_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`role_id`,`func_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role_function
-- ----------------------------
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC01000');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC02000');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC02001');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC02002');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC02003');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC03000');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC03001');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC03002');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC04000');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC04001');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC04002');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC05000');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC05001');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC05002');
INSERT INTO `sys_role_function` VALUES ('ROLE00', 'FUNC05003');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC01000');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC02000');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC02001');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC02002');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC02003');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC03000');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC03001');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC03002');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC05000');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC05001');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC05002');
INSERT INTO `sys_role_function` VALUES ('ROLE01', 'FUNC05003');

-- ----------------------------
-- Table structure for tb_address
-- ----------------------------
DROP TABLE IF EXISTS `tb_address`;
CREATE TABLE `tb_address` (
  `address_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '地址信息表流水号',
  `open_id` varchar(50) NOT NULL COMMENT '用户唯一标识',
  `consignee_name` varchar(20) NOT NULL COMMENT '收货人姓名',
  `consignee_tel` varchar(20) DEFAULT NULL COMMENT '收货人联系方式',
  `consignee_area` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所在地区 逗号隔开',
  `area_id` varchar(100) DEFAULT NULL COMMENT '地区id 逗号分隔',
  `consignee_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '详细地址',
  `is_default` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' COMMENT '是否设为默认地址 0-否 1-是',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入时间',
  `lst_update_time` varchar(20) NOT NULL COMMENT '上一次更新时间',
  `is_del` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`address_id`),
  KEY `index_address` (`open_id`) USING BTREE COMMENT '用于查询用户的地址'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_address
-- ----------------------------
INSERT INTO `tb_address` VALUES ('22091721293c434bc17ba743a9bb005f65ec740d53', 'da6sd4asd4asd5ad5a', '4545545', '1327205997', '', null, '1581345219@qq.com', '0', '2022-09-17 21:29:53', '2022-09-17 21:29:53', '0');
INSERT INTO `tb_address` VALUES ('22092200093002736f012f4606bd246e4f6e4e618c', 'da6sd4asd4asd5ad5a', '刘女士1', '1666666666661', '', null, '1111111111111112', '0', '2022-09-22 00:09:47', '2022-09-22 00:24:34', '1');
INSERT INTO `tb_address` VALUES ('220922002488968f04f8fc4fbebcc8e99fdb0e0d42', 'da6sd4asd4asd5ad5a', '刘女士', '166666666666', '', null, '111111111111111', '0', '2022-09-22 00:24:49', '2022-09-22 00:24:49', '0');
INSERT INTO `tb_address` VALUES ('2210030023b04b9906528f4ef4a18e1d455df35c0a', 'oiDEW0SfG3rciOQk9Pxjsno1CEKQ', '杨桂锋', '15217821745', '', null, '广东省揭阳市揭东区锡场镇华清村', '1', '2022-10-03 00:23:34', '2022-10-03 00:23:34', '1');
INSERT INTO `tb_address` VALUES ('2210061540b42b354203e24299829f3780b41313fe', 'oiDEW0SfG3rciOQk9Pxjsno1CEKQ', '朱望鹏', '11111111111', '广东省,深圳市,龙华区', '440000,440300,440309', '测试', '0', '2022-10-06 15:40:22', '2022-10-06 15:40:22', '1');
INSERT INTO `tb_address` VALUES ('2210061611affb3873a8d34dc5a3861ecd52f52a63', 'oiDEW0SfG3rciOQk9Pxjsno1CEKQ', '杨桂锋', '15217821745', '广东省,揭阳市,揭东区', '440000,445200,445203', '锡场镇华清村', '0', '2022-10-06 16:11:32', '2022-10-06 16:37:17', '0');
INSERT INTO `tb_address` VALUES ('22100616316fa446b0adb54d02b42d132f5fde955f', 'oiDEW0SfG3rciOQk9Pxjsno1CEKQ', '朱望鹏', '11111111111', '广东省,韶关市,武江区', '440000,440200,440203', '杀杀杀', '0', '2022-10-06 16:31:51', '2022-10-06 16:58:16', '0');
INSERT INTO `tb_address` VALUES ('221027145249645be2947b49e19519c1515a7cd6c8', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '杨桂锋', '15217821745', '广东省,广州市,荔湾区', '440000,440100,440103', '测试', '0', '2022-10-27 14:52:25', '2022-10-27 14:52:25', '0');
INSERT INTO `tb_address` VALUES ('2211031016b88d7920b04c4b2a825c', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '朱望鹏', '12345678901', '湖北省,武汉市,武昌区', '420000,420100,420106', '测试', '0', '2022-11-03 10:16:05', '2022-11-03 10:16:05', '1');
INSERT INTO `tb_address` VALUES ('221119220804f477c3c3', 'oMMx05BlyurLeCfabTud-wwyZXWo', '朱望鹏', '13272065997', '广东省,广州市,荔湾区', '440000,440100,440103', '哈哈', '0', '2022-11-19 22:08:29', '2022-11-19 22:08:29', '0');
INSERT INTO `tb_address` VALUES ('221119222842feb420d7df0c4ce8b757', 'oMMx05M1fM5t5gXW38BEMPwdsnu8', '杨桂锋', '15217821745', '广东省,广州市,荔湾区', '440000,440100,440103', '测试', '1', '2022-11-19 22:28:42', '2022-11-19 22:28:42', '0');

-- ----------------------------
-- Table structure for tb_admin_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_admin_info`;
CREATE TABLE `tb_admin_info` (
  `admin_id` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '管理员唯一标识',
  `admin_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '管理员姓名',
  `admin_tel` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '管理员联系方式',
  `first_login_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '第一次登录时间',
  `lst_login_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '最后一次登录时间',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_admin_info
-- ----------------------------
INSERT INTO `tb_admin_info` VALUES ('10001', '杨桂锋', '15217821745', '2022-10-19 20:21:56', '2022-10-13 22:10:46');
INSERT INTO `tb_admin_info` VALUES ('102122', '朱望鹏', '13272065997', '2022-10-02 21:50:30', '2022-10-17 23:34:52');

-- ----------------------------
-- Table structure for tb_amount_flow
-- ----------------------------
DROP TABLE IF EXISTS `tb_amount_flow`;
CREATE TABLE `tb_amount_flow` (
  `flow_id` varchar(50) NOT NULL DEFAULT '' COMMENT '储值流水主键',
  `open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '用户唯一标识',
  `order_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '订单编号',
  `spent_amount` decimal(8,2) DEFAULT NULL COMMENT '消费金额',
  `spent_status` varchar(1) DEFAULT NULL COMMENT '消费状态 1-消费中 2-消费成功',
  `kind` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' COMMENT '0-余额支付 1-提现 2-退款',
  `insert_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '写入日期',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '消费时间',
  PRIMARY KEY (`flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_amount_flow
-- ----------------------------
INSERT INTO `tb_amount_flow` VALUES ('2210260114c2f3f84ca8164adca777d29246a182f1', 'da6sd4asd4asd5ad5a', '221026003316707797', '50.00', '1', '0', '2022-10-26', '2022-10-26 01:14:59');
INSERT INTO `tb_amount_flow` VALUES ('22102601185ea9f857ee8146478552a05d6182697f', 'da6sd4asd4asd5ad5a', '221026003316707797', '230.00', '2', '0', '2022-10-26', '2022-10-26 01:18:37');
INSERT INTO `tb_amount_flow` VALUES ('22102601392c9a18342d7744f382e3c49826bebb23', 'da6sd4asd4asd5ad5a', '221026003316707797', '230.00', '2', '0', '2022-10-26', '2022-10-26 01:39:40');
INSERT INTO `tb_amount_flow` VALUES ('2210260140691bf7d7d0134101979658c2f0852fea', 'da6sd4asd4asd5ad5a', '221026003316707797', '20.00', '1', '0', '2022-10-26', '2022-10-26 01:40:03');
INSERT INTO `tb_amount_flow` VALUES ('221103214419f5459632424d9cb6d2', 'da6sd4asd4asd5ad5a', '提现', '40.00', '2', '1', '2022-11-03', '2022-11-03 21:44:47');
INSERT INTO `tb_amount_flow` VALUES ('22110321491ca990a55ac34dfc9b4f', 'da6sd4asd4asd5ad5a', '提现', '50.00', '2', '1', '2022-11-03', '2022-11-03 21:49:49');
INSERT INTO `tb_amount_flow` VALUES ('2211131443a344d87abd03458db4b9', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '22111100331a6d5731be384f8da221', '14.01', '1', '0', '2022-11-13', '2022-11-13 14:43:35');
INSERT INTO `tb_amount_flow` VALUES ('221113144491e44b46e08340e8a3ec', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '22111100331a6d5731be384f8da221', '35.99', '1', '0', '2022-11-13', '2022-11-13 14:44:27');
INSERT INTO `tb_amount_flow` VALUES ('221113144756754cfc113347cbaa4f', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '221027164686705007', '60.02', '1', '0', '2022-11-13', '2022-11-13 14:47:51');
INSERT INTO `tb_amount_flow` VALUES ('2211131551812e79ca8c2b4de0996c', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'O22111315303cd3f76fb75c4d02a9dd', '0.01', '2', '0', '2022-11-13', '2022-11-13 15:51:03');
INSERT INTO `tb_amount_flow` VALUES ('2211131611c428b133a5864053ac01', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'O221113161157e02aceaf27441eb283', '23.00', '2', '0', '2022-11-13', '2022-11-13 16:11:14');
INSERT INTO `tb_amount_flow` VALUES ('2211201804134873a6db03b9439', 'oMMx05BlyurLeCfabTud-wwyZXWo', 'O221120180352d18ac8b83171446', '0.01', '2', '0', '2022-11-20', '2022-11-20 18:04:13');
INSERT INTO `tb_amount_flow` VALUES ('22112018044680e2ccddd12a4c7', 'oMMx05BlyurLeCfabTud-wwyZXWo', 'O221120180352d18ac8b83171446', '-0.98', '2', '2', '2022-11-20', '2022-11-20 18:04:46');
INSERT INTO `tb_amount_flow` VALUES ('221120183256877a907aba54478', 'oMMx05BlyurLeCfabTud-wwyZXWo', 'O221120183245195bbac00196446', '1.41', '2', '0', '2022-11-20', '2022-11-20 18:32:56');
INSERT INTO `tb_amount_flow` VALUES ('2211201843467b0ebaaf562f4b8', 'oMMx05BlyurLeCfabTud-wwyZXWo', 'O221120183245195bbac00196446', '1.41', '2', '2', '2022-11-20', '2022-11-20 18:43:46');
INSERT INTO `tb_amount_flow` VALUES ('2211201848317f8076dd9b2b454', 'oMMx05BlyurLeCfabTud-wwyZXWo', 'O221120184824972abb0f77bf455', '1.41', '2', '0', '2022-11-20', '2022-11-20 18:48:31');
INSERT INTO `tb_amount_flow` VALUES ('2211201938499a39b790c2774ae', 'oMMx05BlyurLeCfabTud-wwyZXWo', 'O221120184824972abb0f77bf455', '0.91', '2', '2', '2022-11-20', '2022-11-20 19:38:49');

-- ----------------------------
-- Table structure for tb_express
-- ----------------------------
DROP TABLE IF EXISTS `tb_express`;
CREATE TABLE `tb_express` (
  `express_id` varchar(50) NOT NULL COMMENT '快递id',
  `express_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '快递公司编码',
  `express_name` varchar(50) DEFAULT NULL COMMENT '快递公司名称',
  `is_del` varchar(1) DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`express_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_express
-- ----------------------------
INSERT INTO `tb_express` VALUES ('2311333', null, '中通快递', '0');
INSERT INTO `tb_express` VALUES ('23113334', null, '百世快递1', '1');
INSERT INTO `tb_express` VALUES ('EP_221108234243564474', '2222', '百世快递2', '0');

-- ----------------------------
-- Table structure for tb_goods
-- ----------------------------
DROP TABLE IF EXISTS `tb_goods`;
CREATE TABLE `tb_goods` (
  `goods_id` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品id',
  `goods_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品名称',
  `img_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '商品图片id，英文逗号隔开',
  `goods_memo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '商品描述',
  `goods_detail` varchar(3000) DEFAULT '' COMMENT '商品详情',
  `goods_status` varchar(1) NOT NULL DEFAULT '0' COMMENT '0-下架 1-上架',
  `goods_price` decimal(8,2) NOT NULL COMMENT '商品价格',
  `goods_volume` int(11) DEFAULT '0' COMMENT '商品销量',
  `goods_stock` int(11) DEFAULT '0' COMMENT '商品库存 -1 时不限量',
  `goods_cost` decimal(8,2) DEFAULT NULL COMMENT '商品成本',
  `label` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '' COMMENT '标签',
  `express_fee` decimal(8,2) DEFAULT '0.00' COMMENT '快递费',
  `insert_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入日期',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入时间',
  `insert_user` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入人',
  `lst_update_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最近一次更新日期',
  `lst_update_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最近一次更新时间',
  `lst_update_user` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最近一次更新人',
  `is_del` varchar(1) DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_goods
-- ----------------------------
INSERT INTO `tb_goods` VALUES ('221021155002319', '甘肃苹果', 'IMG_2210211550515791', '新鲜水果', '<p><strong>测试富文本</strong></p><p><strong style=\"background-color: rgb(230, 0, 0); color: rgb(255, 255, 0);\">随意的样式</strong></p>', '1', '14.00', '23', '-1', '12.00', '热销', '0.00', '2022-10-21', '2022-10-21 15:50:07', '10001', '2022-10-21', '2022-10-21 15:50:07', '10001', '0');
INSERT INTO `tb_goods` VALUES ('221022132455159', '陕西苹果', 'IMG_2210221327022619', '新鲜水果', '<p><span style=\"color: rgb(250, 204, 204);\">真好吃</span></p>', '0', '23.00', '5', '10', '13.00', '热销', '0.00', '2022-10-22', '2022-10-22 13:24:37', '10001', '2022-10-22', '2022-10-22 13:24:37', '10001', '1');
INSERT INTO `tb_goods` VALUES ('221022164413775', '广东西瓜', 'IMG_2210221600652768', '新鲜水果', '<p><br></p>', '0', '34.00', '12', '222', '12.00', '爆卖', '0.00', '2022-10-22', '2022-10-22 16:44:57', '10001', '2022-10-22', '2022-10-22 16:44:57', '10001', '0');
INSERT INTO `tb_goods` VALUES ('221022204465702', '陕西苹果', 'IMG_2210222046788979', '新鲜水果', '<p><span style=\"color: rgb(230, 0, 0);\">测试</span></p>', '1', '23.00', '123', '4444', '12.00', '爆卖', '0.00', '2022-10-22', '2022-10-22 20:44:44', '10001', '2022-10-22', '2022-10-22 20:44:44', '10001', '0');
INSERT INTO `tb_goods` VALUES ('221022204972362', '陕西苹果', 'IMG_2210222039669960', '新鲜水果', '<p><em>从网上</em></p>', '1', '34.00', '12', '-1', '12.00', '爆卖', '0.00', '2022-10-22', '2022-10-22 20:49:01', '10001', '2022-10-22', '2022-10-22 20:49:01', '10001', '0');
INSERT INTO `tb_goods` VALUES ('221022210294371', '陕西苹果', 'IMG_2210222100843823', '新鲜水果', '<p><span style=\"color: rgb(255, 255, 0);\">说的是</span></p>', '0', '34.00', '0', '-1', '12.00', '热销', '0.00', '2022-10-22', '2022-10-22 21:02:46', '10001', '2022-10-22', '2022-10-22 21:02:46', '10001', '1');
INSERT INTO `tb_goods` VALUES ('221024182259857', '测试水果', 'IMG_2211031762566189', '就是个测试而已，随便写写', '<p><u>xxxxx﻿</u></p>', '1', '0.01', '156', '-1', '0.01', '测试', '0.00', '2022-10-24', '2022-10-24 18:22:47', '10001', '2022-11-20', '2022-11-20 18:48:31', '10001', '0');

-- ----------------------------
-- Table structure for tb_images
-- ----------------------------
DROP TABLE IF EXISTS `tb_images`;
CREATE TABLE `tb_images` (
  `img_id` varchar(20) NOT NULL COMMENT '图片id',
  `img_name` varchar(20) DEFAULT NULL COMMENT '图片名字',
  `img_url` varchar(100) NOT NULL COMMENT '图片地址',
  `img_address` varchar(50) NOT NULL COMMENT '图片在服务器中存储的地址',
  `belong_to` varchar(50) DEFAULT NULL COMMENT '归属菜单',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入时间',
  `is_del` varchar(1) DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_images
-- ----------------------------
INSERT INTO `tb_images` VALUES ('10', '电子营业执照.jpg', 'http://119.29.63.248:8080/images/AHnhEZtK221019221551.jpg', 'D:/images/AHnhEZtK221019221551.jpg', 'banner', '2022-10-19 22:15:51', '1');
INSERT INTO `tb_images` VALUES ('11', '图标.png', 'http://119.29.63.248:8080/images/VudStPGG221019221849.jpg', 'D:/images/VudStPGG221019221849.jpg', 'banner', '2022-10-19 22:18:49', '1');
INSERT INTO `tb_images` VALUES ('4', '捕获.PNG', 'http://119.29.63.248:8080/images/XDkBcPhJ220912232051.jpg', 'D:/images/XDkBcPhJ220912232051.jpg', 'banner', '2022-09-12 23:20:51', '1');
INSERT INTO `tb_images` VALUES ('5', '捕获.PNG', 'http://119.29.63.248:8080/images/WoZLxyUU220912232443.jpg', 'D:/images/WoZLxyUU220912232443.jpg', null, '2022-09-12 23:24:43', '0');
INSERT INTO `tb_images` VALUES ('6', 'qrCode.png', 'http://119.29.63.248:8080/images/GweqMUgX221006143738.jpg', 'D:/images/GweqMUgX221006143738.jpg', null, '2022-10-06 14:37:38', '0');
INSERT INTO `tb_images` VALUES ('7', 'qrCode.png', 'http://119.29.63.248:8080/images/csXQtCVL221006145044.jpg', 'D:/images/csXQtCVL221006145044.jpg', null, '2022-10-06 14:50:44', '0');
INSERT INTO `tb_images` VALUES ('8', 'qrCode.png', 'http://120.77.200.26:8080/images/eCqXWCEj221019215552.jpg', 'D:/images/eCqXWCEj221019215552.jpg', 'banner', '2022-10-19 21:55:52', '1');
INSERT INTO `tb_images` VALUES ('9', '图标.png', 'http://119.29.63.248:8080/images/ZbdoTSzZ221019221450.jpg', 'D:/images/ZbdoTSzZ221019221450.jpg', 'banner', '2022-10-19 22:14:50', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210192310873987', 'qrCode.png', 'http://119.29.63.248:8080/images/DjXTyoMx221019235038.jpg', 'D:/images/DjXTyoMx221019235038.jpg', 'banner', '2022-10-19 23:50:38', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201102374561', '图标.png', 'http://119.29.63.248:8081/images/IcdCsysV221020112739.jpg', 'D:/images/IcdCsysV221020112739.jpg', 'banner', '2022-10-20 11:27:39', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201119806805', '图标.png', 'http://119.29.63.248:8081/images/IyBOwGAY221020111510.jpg', 'D:/images/IyBOwGAY221020111510.jpg', 'banner', '2022-10-20 11:15:10', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201139407474', '图标.png', 'http://119.29.63.248:8081/images/ojpLkPJi221020111731.jpg', 'D:/images/ojpLkPJi221020111731.jpg', 'banner', '2022-10-20 11:17:31', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201147645475', '图标.png', 'http://119.29.63.248:8081/images/JkqIulUV221020111940.jpg', 'D:/images/JkqIulUV221020111940.jpg', 'banner', '2022-10-20 11:19:40', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201159689593', '图标.png', 'http://119.29.63.248:8081/images/usRoPCfd221020113319.jpg', 'D:/images/usRoPCfd221020113319.jpg', 'banner', '2022-10-20 11:33:19', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210201170601745', '图标.png', 'http://119.29.63.248:8081/images/IIOSMpHH221020113107.jpg', 'D:/images/IIOSMpHH221020113107.jpg', 'banner', '2022-10-20 11:31:07', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201178908466', '图标.png', 'http://119.29.63.248:8081/images/hgBltWLt221020113331.jpg', 'D:/images/hgBltWLt221020113331.jpg', 'banner', '2022-10-20 11:33:31', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210201186240718', '图标.png', 'http://119.29.63.248:8081/images/hGeWGuxv221020113019.jpg', 'D:/images/hGeWGuxv221020113019.jpg', 'banner', '2022-10-20 11:30:19', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201186804638', '图标.png', 'http://119.29.63.248:8081/images/BtnMbdWV221020112801.jpg', 'D:/images/BtnMbdWV221020112801.jpg', 'banner', '2022-10-20 11:28:01', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201188616641', '图标.png', 'http://119.29.63.248:8081/images/ZKHMaGzd221020111449.jpg', 'D:/images/ZKHMaGzd221020111449.jpg', 'banner', '2022-10-20 11:14:49', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201190209861', '图标.png', 'http://119.29.63.248:8081/images/eFuXrMqZ221020112247.jpg', 'D:/images/eFuXrMqZ221020112247.jpg', 'banner', '2022-10-20 11:22:47', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201194310214', '图标.png', 'http://119.29.63.248:8081/images/XtYTipLd221020113152.jpg', 'D:/images/XtYTipLd221020113152.jpg', 'banner', '2022-10-20 11:31:52', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210201195766608', '图标.png', 'http://119.29.63.248:8081/images/oAjKFOQa221020113116.jpg', 'D:/images/oAjKFOQa221020113116.jpg', 'banner', '2022-10-20 11:31:16', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210202233550210', '图标.png', 'http://119.29.63.248:8081/images/ZgvjScAj221020225332.jpg', 'D:/images/ZgvjScAj221020225332.jpg', 'goods', '2022-10-20 22:53:32', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202238724510', '图标.png', 'http://119.29.63.248:8081/images/ijzQzeCJ221020225334.jpg', 'D:/images/ijzQzeCJ221020225334.jpg', 'goods', '2022-10-20 22:53:34', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202249501284', '图标.png', 'http://119.29.63.248:8081/images/fGoaGzGj221020225606.jpg', 'D:/images/fGoaGzGj221020225606.jpg', 'goods', '2022-10-20 22:56:06', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202250433473', '图标.png', 'http://119.29.63.248:8081/images/jyJcpVup221020225926.jpg', 'D:/images/jyJcpVup221020225926.jpg', 'goods', '2022-10-20 22:59:26', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202254847237', '图标.png', 'http://119.29.63.248:8081/images/sqdXMhDw221020224350.jpg', 'D:/images/sqdXMhDw221020224350.jpg', 'goods', '2022-10-20 22:43:50', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202256836093', '图标.png', 'http://119.29.63.248:8081/images/jxrFApJD221020224319.jpg', 'D:/images/jxrFApJD221020224319.jpg', 'banner', '2022-10-20 22:43:19', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210202272144111', '图标.png', 'http://119.29.63.248:8081/images/zzmktAzy221020225810.jpg', 'D:/images/zzmktAzy221020225810.jpg', 'goods', '2022-10-20 22:58:10', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202274697577', '图标.png', 'http://119.29.63.248:8081/images/jlmdjZnR221020224335.jpg', 'D:/images/jlmdjZnR221020224335.jpg', 'goods', '2022-10-20 22:43:35', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202312372798', '图标.png', 'http://119.29.63.248:8081/images/HHIImvbJ221020234012.jpg', 'D:/images/HHIImvbJ221020234012.jpg', 'goods', '2022-10-20 23:40:12', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202316635488', '图标.png', 'http://119.29.63.248:8081/images/JmNjLpVb221020233643.jpg', 'D:/images/JmNjLpVb221020233643.jpg', 'goods', '2022-10-20 23:36:43', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202327250001', '图标.png', 'http://119.29.63.248:8081/images/SkMcBchq221020231234.jpg', 'D:/images/SkMcBchq221020231234.jpg', 'goods', '2022-10-20 23:12:34', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202339163513', '图标.png', 'http://119.29.63.248:8081/images/TMbEhyum221020234433.jpg', 'D:/images/TMbEhyum221020234433.jpg', 'goods', '2022-10-20 23:44:33', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202345103348', '图标.png', 'http://119.29.63.248:8081/images/eQZUBbcD221020235359.jpg', 'D:/images/eQZUBbcD221020235359.jpg', 'goods', '2022-10-20 23:53:59', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202349045602', '图标.png', 'http://119.29.63.248:8081/images/vrIqyKTj221020230025.jpg', 'D:/images/vrIqyKTj221020230025.jpg', 'goods', '2022-10-20 23:00:25', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210202356721107', '图标.png', 'http://119.29.63.248:8081/images/bMpihXhw221020234844.jpg', 'D:/images/bMpihXhw221020234844.jpg', 'goods', '2022-10-20 23:48:44', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202356768598', '图标.png', 'http://119.29.63.248:8081/images/PNKihaxH221020234621.jpg', 'D:/images/PNKihaxH221020234621.jpg', 'goods', '2022-10-20 23:46:21', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202361661499', '图标.png', 'http://119.29.63.248:8081/images/wGhttAPj221020230033.jpg', 'D:/images/wGhttAPj221020230033.jpg', 'goods', '2022-10-20 23:00:33', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210202370415820', '图标.png', 'http://119.29.63.248:8081/images/SIiBFclb221020234932.jpg', 'D:/images/SIiBFclb221020234932.jpg', 'goods', '2022-10-20 23:49:32', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202378335527', '图标.png', 'http://119.29.63.248:8081/images/KjtbLBon221020233958.jpg', 'D:/images/KjtbLBon221020233958.jpg', 'goods', '2022-10-20 23:39:58', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202378944400', '图标.png', 'http://119.29.63.248:8081/images/RAGnFtjR221020230043.jpg', 'D:/images/RAGnFtjR221020230043.jpg', 'goods', '2022-10-20 23:00:43', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210202395364956', '图标.png', 'http://119.29.63.248:8081/images/oWwGOVAd221020230031.jpg', 'D:/images/oWwGOVAd221020230031.jpg', 'goods', '2022-10-20 23:00:31', '1');
INSERT INTO `tb_images` VALUES ('IMG_2210211550515791', '图标.png', 'http://119.29.63.248:8081/images/RNLeJjTh221021155000.jpg', 'D:/images/RNLeJjTh221021155000.jpg', 'goods', '2022-10-21 15:50:00', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210221327022619', '图标.png', 'http://119.29.63.248:8081/images/qvHbkTpF221022132432.jpg', 'D:/images/qvHbkTpF221022132432.jpg', 'goods', '2022-10-22 13:24:32', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210221541932054', '图标.png', 'http://119.29.63.248:8081/images/NJwVmATs221022154926.jpg', 'D:/images/NJwVmATs221022154926.jpg', 'goods', '2022-10-22 15:49:26', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210221543632451', '图标.png', 'http://119.29.63.248:8081/images/TNvpFkfw221022155236.jpg', 'D:/images/TNvpFkfw221022155236.jpg', 'goods', '2022-10-22 15:52:36', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210221600652768', '图标.png', 'http://119.29.63.248:8080/images/UQxzwsAI221022164432.jpg', 'D:/images/UQxzwsAI221022164432.jpg', 'goods', '2022-10-22 16:44:32', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210222039669960', '图标.png', 'http://119.29.63.248:8080/images/SdctHtRl221022204856.jpg', 'D:/images/SdctHtRl221022204856.jpg', 'goods', '2022-10-22 20:48:56', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210222046788979', '图标.png', 'http://119.29.63.248:8080/images/mPnDiUuP221022204440.jpg', 'D:/images/mPnDiUuP221022204440.jpg', 'goods', '2022-10-22 20:44:40', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210222100843823', '图标.png', 'http://119.29.63.248:8080/images/QqvRwmUi221022210241.jpg', 'D:/images/QqvRwmUi221022210241.jpg', 'goods', '2022-10-22 21:02:41', '0');
INSERT INTO `tb_images` VALUES ('IMG_2210241806703940', '图标.png', 'http://119.29.63.248:8080/images/OgEofzQP221024182239.jpg', 'D:/images/OgEofzQP221024182239.jpg', 'goods', '2022-10-24 18:22:39', '1');
INSERT INTO `tb_images` VALUES ('IMG_2211031762566189', '图标.png', 'http://119.29.63.248:8080/images/ZqCClbxL221103172348.jpg', 'D:/images/ZqCClbxL221103172348.jpg', 'goods', '2022-11-03 17:23:48', '0');
INSERT INTO `tb_images` VALUES ('IMG_2211072170876855', 'qrCode.png', 'http://119.29.63.248:8080/images/zbMbBlWl221107210804.jpg', 'D:/images/zbMbBlWl221107210804.jpg', 'bottom', '2022-11-07 21:08:04', '0');
INSERT INTO `tb_images` VALUES ('IMG_2211072177338470', 'qrCode.png', 'http://119.29.63.248:8080/images/DnUbPalp221107210805.jpg', 'D:/images/DnUbPalp221107210805.jpg', 'bottom', '2022-11-07 21:08:05', '0');
INSERT INTO `tb_images` VALUES ('IMG_2211072184215710', 'qrCode.png', 'http://119.29.63.248:8080/images/bHPsdvJt221107210039.jpg', 'D:/images/bHPsdvJt221107210039.jpg', 'bottom', '2022-11-07 21:00:39', '0');
INSERT INTO `tb_images` VALUES ('IMG_2211111787563573', '图标.png', 'http://https://pfruits.cn/api/images/uzfLHLzk221111174101.jpg', 'D:/images/uzfLHLzk221111174101.jpg', 'banner', '2022-11-11 17:41:01', '0');

-- ----------------------------
-- Table structure for tb_label
-- ----------------------------
DROP TABLE IF EXISTS `tb_label`;
CREATE TABLE `tb_label` (
  `label_id` varchar(25) DEFAULT NULL COMMENT '标签id',
  `label_name` varchar(50) NOT NULL DEFAULT '' COMMENT '标签名称'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_label
-- ----------------------------

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu` (
  `menu_id` varchar(15) NOT NULL COMMENT '菜单id',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `menu_memo` varchar(200) DEFAULT NULL COMMENT '菜单备注',
  `menu_order` int(11) NOT NULL COMMENT '排序',
  `menu_status` varchar(1) DEFAULT '0' COMMENT '是否对客展示 0-否 1-是',
  `insert_date` varchar(10) NOT NULL COMMENT '写入日期 2022-09-11',
  `insert_time` varchar(20) NOT NULL COMMENT '写入时间',
  `insert_user` varchar(15) NOT NULL COMMENT '管理员id',
  `lst_update_date` varchar(10) NOT NULL COMMENT '更新日期',
  `lst_update_time` varchar(20) NOT NULL COMMENT '写入时间',
  `lst_update_user` varchar(15) NOT NULL COMMENT '最近一次更新人',
  `is_del` varchar(1) NOT NULL DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES ('220911163855875', '热销', '1002233', '1', '1', '2022-09-11', '2022-09-11 16:38:19', '1121212', '2022-10-21', '2022-10-21 01:00:05', '102122', '1');
INSERT INTO `tb_menu` VALUES ('220912234386780', '苹果', '好吃的苹果', '2', '1', '2022-09-12', '2022-09-12 23:43:31', '1121212', '2022-10-24', '2022-10-24 18:17:19', '10001', '0');
INSERT INTO `tb_menu` VALUES ('220913223725555', '李子', null, '3', '1', '2022-09-13', '2022-09-13 22:37:24', '1121212', '2022-09-13', '2022-09-13 22:37:24', '1121212', '0');
INSERT INTO `tb_menu` VALUES ('220913223826674', '凤梨', null, '3', '1', '2022-09-13', '2022-09-13 22:38:27', '1121212', '2022-10-22', '2022-10-22 19:02:26', '10001', '1');
INSERT INTO `tb_menu` VALUES ('221007183935420', '葡萄', null, '6', null, '2022-10-07', '2022-10-07 18:39:30', '102122', '2022-10-22', '2022-10-22 19:02:22', '10001', '1');
INSERT INTO `tb_menu` VALUES ('221007184781785', '葡萄', null, '6', '1', '2022-10-07', '2022-10-07 18:47:50', '102122', '2022-10-22', '2022-10-22 17:29:27', '10001', '1');
INSERT INTO `tb_menu` VALUES ('221022190200067', '热带水果', '热带水果', '1', '1', '2022-10-22', '2022-10-22 19:02:40', '10001', '2022-10-24', '2022-10-24 23:28:55', '102122', '0');
INSERT INTO `tb_menu` VALUES ('221022190267979', '热销', '当前热销商品', '0', '1', '2022-10-22', '2022-10-22 19:02:17', '10001', '2022-10-24', '2022-10-24 23:28:09', '102122', '0');

-- ----------------------------
-- Table structure for tb_menu_goods
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu_goods`;
CREATE TABLE `tb_menu_goods` (
  `menu_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单id',
  `goods_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品id',
  `goods_order` int(11) DEFAULT NULL COMMENT '商品排序',
  PRIMARY KEY (`goods_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_menu_goods
-- ----------------------------
INSERT INTO `tb_menu_goods` VALUES ('220912234386780', '221021155002319', '0');
INSERT INTO `tb_menu_goods` VALUES ('221022190267979', '221021155002319', '4');
INSERT INTO `tb_menu_goods` VALUES ('221022190267979', '221022164413775', '1');
INSERT INTO `tb_menu_goods` VALUES ('221022190267979', '221022204465702', '2');
INSERT INTO `tb_menu_goods` VALUES ('221022190267979', '221022204972362', '3');
INSERT INTO `tb_menu_goods` VALUES ('220912234386780', '221024182259857', '0');
INSERT INTO `tb_menu_goods` VALUES ('220913223725555', '221024182259857', '0');
INSERT INTO `tb_menu_goods` VALUES ('221022190200067', '221024182259857', '0');
INSERT INTO `tb_menu_goods` VALUES ('221022190267979', '221024182259857', '0');

-- ----------------------------
-- Table structure for tb_message_board
-- ----------------------------
DROP TABLE IF EXISTS `tb_message_board`;
CREATE TABLE `tb_message_board` (
  `flow_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '流水号',
  `open_id` varchar(50) NOT NULL COMMENT '用户唯一标识',
  `kind` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '0-留言 1-发货提醒',
  `message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci COMMENT '留言板留言',
  `insert_date` varchar(10) NOT NULL COMMENT '写入日期',
  `insert_time` varchar(20) NOT NULL,
  PRIMARY KEY (`flow_id`),
  KEY `index_message_borad` (`open_id`) COMMENT '用户唯一标识'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_message_board
-- ----------------------------
INSERT INTO `tb_message_board` VALUES ('2209241632ad1a5862cf724f0aaaa5b2a6946da5aa', 'sd2ada2dsadsa2dsa1dAS', '0', '水果不新鲜', '2022-09-24', '2022-09-24 16:32:17');
INSERT INTO `tb_message_board` VALUES ('2209241632da2d3e5f2b6841a5a926ddf0ad5c23b7', 'sd2ada2dsadsa2dsa1dAS', '1', '订单:[12222]请发货', '2022-09-24', '2022-09-24 16:32:47');
INSERT INTO `tb_message_board` VALUES ('2211031012765ba33c4284438ea437', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', '', '2022-11-03', '2022-11-03 10:12:26');
INSERT INTO `tb_message_board` VALUES ('221103101300bccf924ecd4332a744', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', '', '2022-11-03', '2022-11-03 10:13:03');
INSERT INTO `tb_message_board` VALUES ('2211031015dbcdec30684348e49c65', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', '', '2022-11-03', '2022-11-03 10:15:10');
INSERT INTO `tb_message_board` VALUES ('22110311436de312b187254af0bf14', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', null, '2022-11-03', '2022-11-03 11:43:45');
INSERT INTO `tb_message_board` VALUES ('2211031145df1ac3779c4c4808a50f', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', '水果真新鲜！', '2022-11-03', '2022-11-03 11:45:23');
INSERT INTO `tb_message_board` VALUES ('2211131619fe8344353b834e5490b4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '1', '发来一条发货提醒', '2022-11-13', '2022-11-13 16:19:19');

-- ----------------------------
-- Table structure for tb_operate_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_operate_record`;
CREATE TABLE `tb_operate_record` (
  `flow_id` varchar(25) NOT NULL COMMENT '流水号',
  `admin_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '管理员id',
  `open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `operate_content` varchar(255) DEFAULT NULL,
  `operate_menu` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `operate_date` varchar(10) DEFAULT NULL COMMENT '操作日期',
  `operate_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_operate_record
-- ----------------------------
INSERT INTO `tb_operate_record` VALUES ('22110317422111398615', '10001', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', null, '/user/inBack', '2022-11-03', '2022-11-03 17:42:35');
INSERT INTO `tb_operate_record` VALUES ('22110321348961556113', '102122', 'da6sd4asd4asd5ad5a', '20.20', '/user/recharge', '2022-11-03', '2022-11-03 21:34:06');
INSERT INTO `tb_operate_record` VALUES ('22110321362344137973', '102122', 'da6sd4asd4asd5ad5a', '20', '/user/withdrawal', '2022-11-03', '2022-11-03 21:36:56');
INSERT INTO `tb_operate_record` VALUES ('22110321393358503224', '102122', 'da6sd4asd4asd5ad5a', '20', '/user/withdrawal', '2022-11-03', '2022-11-03 21:39:21');
INSERT INTO `tb_operate_record` VALUES ('22110321448074217745', '102122', 'da6sd4asd4asd5ad5a', '20', '/user/withdrawal', '2022-11-03', '2022-11-03 21:44:47');
INSERT INTO `tb_operate_record` VALUES ('22110321460647431742', '102122', 'da6sd4asd4asd5ad5a', '100', '/user/recharge', '2022-11-03', '2022-11-03 21:46:18');

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `order_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单流水号',
  `open_id` varchar(50) NOT NULL COMMENT '用户唯一标识',
  `order_price` decimal(8,2) NOT NULL COMMENT '订单价格',
  `refund_price` decimal(8,2) DEFAULT '0.00' COMMENT '订单退款金额',
  `order_status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单状态 0-待付款 1-待发货 2-部分发货 3-全部发货 4-已完成 5-取消订单 6-确认收货 7-部分退款 8-全部退款\r\n',
  `order_memo` varchar(500) DEFAULT NULL COMMENT '订单备注',
  `address_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '地址id',
  `insert_date` varchar(10) NOT NULL COMMENT '创建日期',
  `insert_time` varchar(20) NOT NULL COMMENT '创建时间',
  `expire_time` varchar(20) DEFAULT NULL COMMENT '过期时间',
  `wx_pay_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '支付时间',
  `wx_pay_price` decimal(8,2) DEFAULT '0.00' COMMENT '支付价格',
  `am_pay_time` varchar(20) DEFAULT NULL,
  `am_pay_price` decimal(8,2) DEFAULT '0.00' COMMENT '余额支付价格',
  `all_deliver_time` varchar(20) DEFAULT NULL COMMENT '全部发货时间',
  `confirm_receipt_time` varchar(20) DEFAULT NULL COMMENT '确认收货时间',
  `finish_order_time` varchar(20) DEFAULT NULL COMMENT '完成订单时间',
  PRIMARY KEY (`order_id`),
  KEY `INDEX_OPEN_ID` (`open_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_order
-- ----------------------------

-- ----------------------------
-- Table structure for tb_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `tb_order_detail`;
CREATE TABLE `tb_order_detail` (
  `flow_id` varchar(50) NOT NULL COMMENT '流水号',
  `cart_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '购物车中商品流水号',
  `open_id` varchar(50) NOT NULL COMMENT '用户唯一标识',
  `order_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单id',
  `goods_id` varchar(18) NOT NULL COMMENT '商品id',
  `img_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '图片id',
  `goods_name` varchar(20) DEFAULT NULL COMMENT '商品名称',
  `goods_price` decimal(8,2) DEFAULT NULL COMMENT '价格',
  `goods_num` int(11) DEFAULT NULL COMMENT '商品数量',
  `insert_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '写入时间',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '写入时间',
  PRIMARY KEY (`flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for tb_order_express
-- ----------------------------
DROP TABLE IF EXISTS `tb_order_express`;
CREATE TABLE `tb_order_express` (
  `express_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '快递单号',
  `order_id` varchar(50) DEFAULT NULL COMMENT '订单id',
  `express_id` varchar(50) DEFAULT NULL COMMENT '快递公司id',
  `express_fee` varchar(255) DEFAULT NULL COMMENT '运费',
  `goods_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商品id',
  `goods_num` int(11) DEFAULT NULL COMMENT '商品数量',
  `deliver_date` varchar(10) DEFAULT NULL COMMENT '发货日期',
  `deliver_time` varchar(20) DEFAULT NULL COMMENT '发货时间',
  `insert_time` varchar(20) DEFAULT NULL COMMENT '写入时间',
  `is_del` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`express_number`,`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_order_express
-- ----------------------------
INSERT INTO `tb_order_express` VALUES ('20056531341311', 'O221120184824972abb0f77bf455', '2311333', '12', '221024182259857', '100', '2022-11-20', '2022-11-20 23:33:00', '2022-11-20 19:53:01', '0');
INSERT INTO `tb_order_express` VALUES ('20056531341311898', 'O221120184824972abb0f77bf455', '2311333', '12', '221024182259857', '56', '2022-11-20', '2022-11-20 23:33:00', '2022-11-20 19:53:01', '0');
INSERT INTO `tb_order_express` VALUES ('56311332132', '2210260033167077971', '1000', '12.89', '2210211550023192', '11', '2022-11-07', '2022-11-07 23:33:00', '2022-11-13 15:03:14', '0');
INSERT INTO `tb_order_express` VALUES ('563113321325555', '2210260033167077971', '1000', '12.89', '2210211550023192', '11', '2022-11-07', '2022-11-07 23:33:00', '2022-11-13 15:05:53', '0');
INSERT INTO `tb_order_express` VALUES ('563113321325555', '2210260033167077971', '1000', '12.89', '221022204465702', '11', '2022-11-07', '2022-11-07 23:33:00', '2022-11-13 15:05:53', '0');
INSERT INTO `tb_order_express` VALUES ('5631133213788', '2210260033167077971', '2311333', '1', '221022204465702', '1', '2022-11-08', '2022-11-08 22:23:59', '2022-11-08 22:24:05', '0');

-- ----------------------------
-- Table structure for tb_rebate_flow
-- ----------------------------
DROP TABLE IF EXISTS `tb_rebate_flow`;
CREATE TABLE `tb_rebate_flow` (
  `order_id` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号',
  `open_id` varchar(50) NOT NULL COMMENT '被返利人的openId',
  `order_open_id` varchar(50) NOT NULL COMMENT '下订单的openId',
  `rebate_money` decimal(8,2) NOT NULL,
  `rebate_status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '返利状态 1-返利中 2-返利成功',
  `insert_date` varchar(10) NOT NULL COMMENT '写入日期',
  `insert_time` varchar(20) NOT NULL COMMENT '写入时间',
  PRIMARY KEY (`order_id`),
  KEY `INDEX_OPENID` (`open_id`) USING BTREE COMMENT 'openId索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_rebate_flow
-- ----------------------------
INSERT INTO `tb_rebate_flow` VALUES ('221007115542086087', 'da6sd4asd4asd5ad5a', 'oiDEW0SfG3rciOQk9Pxjsno1CEKQ', '1.87', '1', '2022-10-22', '2022-10-22 12:40:10');
INSERT INTO `tb_rebate_flow` VALUES ('22111100331a6d5731be384f8da221', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '1.40', '1', '2022-11-11', '2022-11-11 00:33:55');
INSERT INTO `tb_rebate_flow` VALUES ('2211110051f9ab3447acd748428a29', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.00', '1', '2022-11-11', '2022-11-11 00:51:17');
INSERT INTO `tb_rebate_flow` VALUES ('2211110059565578e7cbdc429fb210', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.00', '1', '2022-11-11', '2022-11-11 00:59:02');
INSERT INTO `tb_rebate_flow` VALUES ('22111101055009e04151fa478393a7', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.00', '1', '2022-11-11', '2022-11-11 01:05:47');
INSERT INTO `tb_rebate_flow` VALUES ('2211110107f4b1f532399b423c8ee4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '1.02', '1', '2022-11-11', '2022-11-11 01:07:49');
INSERT INTO `tb_rebate_flow` VALUES ('2211110109bc80334a040f40529d1a', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.00', '1', '2022-11-11', '2022-11-11 01:09:09');
INSERT INTO `tb_rebate_flow` VALUES ('2211110112d5ca33b0882b4d369f67', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.00', '1', '2022-11-11', '2022-11-11 01:12:29');
INSERT INTO `tb_rebate_flow` VALUES ('O2211140020f7318b8549da449db4db', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.10', '1', '2022-11-14', '2022-11-14 00:54:46');
INSERT INTO `tb_rebate_flow` VALUES ('O2211140028de397cc902bb4131b026', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.10', '1', '2022-11-14', '2022-11-14 01:02:26');
INSERT INTO `tb_rebate_flow` VALUES ('O221114004965193d5aa0954fbda760', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.10', '1', '2022-11-14', '2022-11-14 00:49:50');
INSERT INTO `tb_rebate_flow` VALUES ('O221120184824972abb0f77bf455', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', 'oMMx05BlyurLeCfabTud-wwyZXWo', '0.15', '1', '2022-11-20', '2022-11-20 18:48:31');

-- ----------------------------
-- Table structure for tb_recharge_flow
-- ----------------------------
DROP TABLE IF EXISTS `tb_recharge_flow`;
CREATE TABLE `tb_recharge_flow` (
  `flow_id` varchar(50) NOT NULL COMMENT '充值流水号',
  `open_id` varchar(255) NOT NULL COMMENT '用户标识',
  `recharge_amount` decimal(8,2) NOT NULL COMMENT '充值金额',
  `recharge_status` varchar(1) NOT NULL COMMENT '充值状态 1-充值中 2-充值成功',
  `insert_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '充值日期',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '充值时间',
  PRIMARY KEY (`flow_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_recharge_flow
-- ----------------------------
INSERT INTO `tb_recharge_flow` VALUES ('22110321347fa05de3122f45cb86ea', 'da6sd4asd4asd5ad5a', '20.20', '2', '2022-11-03', '2022-11-03 21:34:06');
INSERT INTO `tb_recharge_flow` VALUES ('22110321462476f379e0604802873e', 'da6sd4asd4asd5ad5a', '100.00', '2', '2022-11-03', '2022-11-03 21:46:18');
INSERT INTO `tb_recharge_flow` VALUES ('R2211131452722b49ed901b4711ae7d', 'sd2ada2dsadsa2dsa1dAS', '100.05', '1', '2022-11-13', '2022-11-13 14:52:22');
INSERT INTO `tb_recharge_flow` VALUES ('R22111314535f1b5d23e7b24bc1aab0', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '100.00', '1', '2022-11-13', '2022-11-13 14:53:18');
INSERT INTO `tb_recharge_flow` VALUES ('R2211140137f34999819308462b91be', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '1', '2022-11-14', '2022-11-14 01:37:17');
INSERT INTO `tb_recharge_flow` VALUES ('R2211140142483ec26e21f44825b1d9', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '1', '2022-11-14', '2022-11-14 01:42:01');
INSERT INTO `tb_recharge_flow` VALUES ('R22111401478ce776d4c3c1421baffb', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '1', '2022-11-14', '2022-11-14 01:47:32');
INSERT INTO `tb_recharge_flow` VALUES ('R2211140149fc9c84e1ae504c44aa67', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '1', '2022-11-14', '2022-11-14 01:49:50');
INSERT INTO `tb_recharge_flow` VALUES ('R2211140153d8e8a912823e441a85df', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '2', '2022-11-14', '2022-11-14 01:53:49');
INSERT INTO `tb_recharge_flow` VALUES ('R221114020248eee691f0964300b0da', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '2', '2022-11-14', '2022-11-14 02:02:31');
INSERT INTO `tb_recharge_flow` VALUES ('R2211140204b2c6780e057b43ee9f30', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '2', '2022-11-14', '2022-11-14 02:04:21');
INSERT INTO `tb_recharge_flow` VALUES ('R221114021718ef072d11554263a3d3', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '2', '2022-11-14', '2022-11-14 02:17:22');
INSERT INTO `tb_recharge_flow` VALUES ('R22111402183d5c8f9835974954a917', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0.01', '2', '2022-11-14', '2022-11-14 02:18:07');

-- ----------------------------
-- Table structure for tb_shopping_cart
-- ----------------------------
DROP TABLE IF EXISTS `tb_shopping_cart`;
CREATE TABLE `tb_shopping_cart` (
  `cart_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '购物车流水号',
  `open_id` varchar(50) NOT NULL COMMENT '用户唯一标识',
  `goods_id` varchar(18) NOT NULL,
  `goods_num` int(11) NOT NULL COMMENT '商品数量',
  `insert_time` varchar(20) NOT NULL COMMENT '添加购物车加入时间',
  `lst_update_time` varchar(20) NOT NULL COMMENT '最后一次更新时间',
  `is_del` varchar(1) DEFAULT '0' COMMENT '0-否 1-是',
  PRIMARY KEY (`cart_id`),
  KEY `index_cart` (`open_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_shopping_cart
-- ----------------------------

-- ----------------------------
-- Table structure for tb_sys_param
-- ----------------------------
DROP TABLE IF EXISTS `tb_sys_param`;
CREATE TABLE `tb_sys_param` (
  `param_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '参数编码',
  `param_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '参数名称',
  `param_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '参数值',
  `param_memo` varchar(255) DEFAULT NULL COMMENT '备注',
  `enable` varchar(255) DEFAULT NULL,
  `lst_admin_id` varchar(15) DEFAULT NULL,
  `lst_modify_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `is_del` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '0' COMMENT '是否删除 0-否 1-是',
  PRIMARY KEY (`param_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_sys_param
-- ----------------------------
INSERT INTO `tb_sys_param` VALUES ('GRANT_TYPE', '获取AccessToken参数', 'client_credential', '获取AccessToken参数', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('IMG_URl', '图片请求地址', 'https://pfruits.cn/api', '图片请求地址', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('REBATE_PERCENTAGE', '返利百分比', '10%', '订单金额100*10%等于返利的金额', '1', '102122', '2022-10-22 12:06:22', '0');
INSERT INTO `tb_sys_param` VALUES ('WX_CODE_URl', '获取无限制微信小程序码URL', 'https://api.weixin.qq.com/wxa/getwxacodeunlimit', '获取无限制微信小程序码URL', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('WX_GET_ACCESS_TOKEN_URL', '微信获取accessTokenURL', 'https://api.weixin.qq.com/cgi-bin/token', '微信获取accessTokenURL', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('WX_GET_OPENID_URL', '微信获取openId', 'https://api.weixin.qq.com/sns/jscode2session', '微信获取openId', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('WX_HEAD_IMG', '微信头像图片Url', 'https://pfruits.cn/api/images/head.jpg', '微信头像图片Url', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('WX_ORDER_DESCRIPTION', '微信商品购买下单描述', '阿平果业-购买商品', '微信下单描述', '1', '102122', '2022-10-23 12:02:15', '0');
INSERT INTO `tb_sys_param` VALUES ('WX_RECHARGE_DESCRIPTION', '微信充值下单描述', '阿平果业-余额充值', '微信充值下单描述', '1', '102122', '2022-10-23 12:02:15', '0');

-- ----------------------------
-- Table structure for tb_user_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_info`;
CREATE TABLE `tb_user_info` (
  `open_id` varchar(50) NOT NULL COMMENT '用户唯一标识',
  `wechat_name` varchar(255) DEFAULT NULL COMMENT '用户微信名',
  `account_balances` decimal(8,2) DEFAULT '0.00' COMMENT '账户余额',
  `inviter_open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '邀请人openId',
  `is_back` varchar(1) DEFAULT '0' COMMENT '是否加入黑名单 0-否 1-是',
  `first_login_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '第一次登录日期',
  `first_login_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '第一次登录时间',
  `lst_login_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最近一次登录日期',
  `lst_login_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后一次登录时间',
  PRIMARY KEY (`open_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_user_info
-- ----------------------------
INSERT INTO `tb_user_info` VALUES ('oMMx05BlyurLeCfabTud-wwyZXWo', '平锋用户3dac3496fe', '0.91', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', '2022-11-19', '2022-11-19 22:07:37', '2022-11-20', '2022-11-20 19:11:59');
INSERT INTO `tb_user_info` VALUES ('oMMx05La01jR0zv3Rzfyq9fIvdV4', '平锋用户d98e974edd', '1.03', 'oMMx05La01jR0zv3Rzfyq9fIvdV4', '0', '2022-11-11', '2022-11-11 16:16:23', '2022-11-20', '2022-11-20 18:30:27');
INSERT INTO `tb_user_info` VALUES ('oMMx05M1fM5t5gXW38BEMPwdsnu8', '平锋用户6fa493d06b', '0.00', '', '0', '2022-11-19', '2022-11-19 22:28:17', '2022-11-19', '2022-11-19 22:28:17');

-- ----------------------------
-- Table structure for tb_user_visit_stat
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_visit_stat`;
CREATE TABLE `tb_user_visit_stat` (
  `data_date` varchar(10) NOT NULL COMMENT '数据日期',
  `user_num` int(11) NOT NULL COMMENT '用户总数',
  `today_add_user_num` int(11) NOT NULL COMMENT '当日新增用户',
  `today_visit_num` int(11) DEFAULT NULL COMMENT '当日访问量',
  `today_sales_volume` int(11) DEFAULT NULL COMMENT '当日销量',
  `today_sales_value` decimal(12,2) DEFAULT NULL COMMENT '当日销售额',
  `insert_time` varchar(20) DEFAULT NULL COMMENT '写入时间',
  PRIMARY KEY (`data_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_user_visit_stat
-- ----------------------------
INSERT INTO `tb_user_visit_stat` VALUES ('2022-11-13', '1', '0', '50', '5', '173.23', '2022-11-14 03:06:00');
INSERT INTO `tb_user_visit_stat` VALUES ('2022-11-16', '1', '0', '1', '0', '0.00', '2022-11-17 01:00:00');

-- ----------------------------
-- Table structure for tb_wx_pay_flow
-- ----------------------------
DROP TABLE IF EXISTS `tb_wx_pay_flow`;
CREATE TABLE `tb_wx_pay_flow` (
  `order_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '订单编号',
  `open_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户唯一标识',
  `wechat_amount` decimal(8,2) NOT NULL COMMENT '微信支付金额',
  `wechat_order_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '微信支付的订单编号',
  `pay_status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '支付状态 1-支付中 2-支付成功',
  `pay_kind` varchar(1) DEFAULT '' COMMENT '支付种类 1-商品购买订单 2-充值',
  `insert_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入时间',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入时间',
  PRIMARY KEY (`order_id`),
  UNIQUE KEY `INDEX_WECHAT_FLOW` (`order_id`) USING BTREE COMMENT '订单唯一索引'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_wx_pay_flow
-- ----------------------------

-- ----------------------------
-- Table structure for tb_wx_refund_flow
-- ----------------------------
DROP TABLE IF EXISTS `tb_wx_refund_flow`;
CREATE TABLE `tb_wx_refund_flow` (
  `out_refund_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商户退款单号',
  `order_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '商户订单号',
  `refund_money` decimal(8,2) DEFAULT NULL COMMENT '退款金额',
  `total_money` decimal(8,2) DEFAULT NULL COMMENT '原订单金额',
  `actual_money` decimal(8,2) DEFAULT NULL COMMENT '实际退款金额',
  `refund_status` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '退款状态 1-退款中 2-退款关闭 3-退款异常 4-退款成功',
  `refund_kind` varchar(1) DEFAULT NULL COMMENT '退款类别 1-移动端取消订单 2-管理端退款',
  `refund_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '微信支付退款单号',
  `transaction_id` varchar(32) DEFAULT NULL COMMENT '微信支付订单号',
  `refund_time` varchar(20) DEFAULT NULL COMMENT '退款时间',
  `insert_date` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入日期',
  `insert_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '写入时间',
  PRIMARY KEY (`out_refund_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tb_wx_refund_flow
-- ----------------------------
