package com.aping.scheduler;

import cn.hutool.core.date.DateUtil;
import com.aping.common.constant.SysConstant;
import com.aping.common.redis.RedisHandle;
import com.aping.common.redis.RedisLockHelper;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.StringUtil;
import com.aping.manage.index.dao.IndexDao;
import com.aping.manage.index.dao.VisitDao;
import com.aping.manage.index.dto.StatDto;
import com.aping.manage.index.vo.StatVo;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Component
@Slf4j
@Api(tags = "用户访问量定时任务")
@EnableScheduling
public class UserVisitScheduler {

    @Resource
    private RedisHandle redisHandle;
    @Resource
    private IndexDao indexDao;
    @Resource
    private VisitDao visitDao;
    @Resource
    private RedisLockHelper redisLockHelper;

    @Scheduled(cron = "0 0 1 ? * *")
    @Transactional
    public void execute() {
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "USER_VISIT_SCHEDULER";
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return;
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            // 处理访问量
            String yestDayDate = DateUtil.format(
                    DateUtil.offsetDay(new Date(),-1),"yyyy-MM-dd");
            log.error(">>>>>>[{}]用户访问数据写库开始<<<<<<",yestDayDate);
            String redisKey = SysConstant.APING_TODAY_VISIT_NUM;
            if(redisHandle.hasMapKey(redisKey, yestDayDate)){
                StatVo statVo = indexDao.queryStatVo(yestDayDate);
                StatDto statDto = new StatDto();
                BeanUtils.copyProperties(statVo,statDto);
                String visitNum = redisHandle.getMapField(redisKey,yestDayDate);
                if(StringUtil.isEmpty(visitNum)){
                    visitNum = "0";
                }
                statDto.setTodayVisitNum(visitNum);
                statDto.setInsertTime(CommonUtil.getSeverTime());
                visitDao.addUserVisit(statDto);
                redisHandle.removeMapField(redisKey, yestDayDate);
                log.error(">>>>>>[{}]用户访问数据写库完成<<<<<<",yestDayDate);
            }else {
                // 发送短息告诉管理员数据丢失
                log.error(">>>>>>[{}]用户访问数据丢失<<<<<<",yestDayDate);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error(">>>>>>用户访问数据写入数据库失败<<<<<<");
        } finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }
}
