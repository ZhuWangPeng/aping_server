package com.aping.scheduler;

import com.aping.common.redis.RedisLockHelper;
import com.aping.mob.order.service.OrderService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
@Slf4j
@Api(tags = "清理数据")
public class CleanUpDataScheduler {

    @Resource
    private OrderService orderService;
    @Resource
    private RedisLockHelper redisLockHelper;
    // 清除未支付成功的流水 查询订单是否已经被取消
    // 全部发货后，经过7*24小时自动确认收货或用户确认收货，
    // 收货后状态变成”确认收货“状态，此时还可以发起退款，
    // 24小时后变成”已完成“状态，此时不能发起退款
    // todo 清除24小时后充值还是状态还是为充值中的订单
    // todo 处理过期订单 一个小时执行一次
    // 7*24小时自动确认收货 24小时后变成”已完成“状态 一个小时执行一次 以发货时间为准
    // 在全部发货后，经过7*24小时自动确认收货或用户确认收货，（定时任务一个小时跑一次）
    // 收货后状态变成”已收货“状态，此时还可以发起退款，24小时后变成”已完成“状态，此时不能发起退款
    // 已完成时将返利流水金额加到余额中
    // todo 清除24小时后微信支付流水中还是支付中的流水
    // todo 清除过期订单 一小时执行一次
    // 0 0 0/1 ? * *
    @Scheduled(cron = "0 0 0/1 ? * *")
    @Transactional
    public void execute() {
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "CLEANl_UP_DATA_SCHEDULER";
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return;
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            // 处理过期订单
            orderService.dealExpireNoPay();
            // 订单自动确认收货
            orderService.autoConfirmReceipt();
            // 订单自动已完成并处理余额
            orderService.autoFinishOrder();
        }catch (Exception e){
            e.printStackTrace();
            log.error(">>>>>>数据清理失败<<<<<<");
        }finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }
}
