package com.aping.mob.order.controller;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.mob.order.dto.OrderDto;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.valid.CancelOrder;
import com.aping.mob.order.valid.OrderAdd;
import com.aping.mob.order.valid.OrderDetail;
import com.aping.platform.requestJson.annotation.RequestJson;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/order")
@Api(tags = "订单")
@Slf4j
public class OrderController {
    @Resource
    private OrderService service;

    @ApiOperation(value = "查询订单")
    @PostMapping("/query")
    public Result queryList(@RequestBody OrderDto dto){
        return service.queryList(dto);
    }


    @ApiOperation(value = "生成订单")
    @PostMapping("/add")
    public Result addOrder(@RequestBody @Validated(value = OrderAdd.class) OrderDto dto){
        return service.addOrder(dto);
    }


    @ApiOperation(value = "取消订单")
    @PostMapping("/cancel")
    public Result cancelOrder(@RequestJson("orderId")String orderId){
        return service.cancelOrder(orderId);
    }


    @ApiOperation(value = "查询订单详情")
    @PostMapping("/queryOrderDetail")
    public Result queryOrderDetail(@RequestBody @Validated(value = OrderDetail.class)  OrderDto dto){
        return service.queryOrderDetail(dto.getOrderId(), dto.getOpenId());
    }

    @ApiOperation(value = "确认收货")
    @PostMapping("/confirmReceipt")
    public Result confirmReceipt(@RequestJson("orderId") String orderId){
        return service.confirmReceipt(orderId);
    }

    @ApiOperation(value = "处理过期订单")
    @GetMapping("/dealExpireNoPay")
    public Result dealExpireNoPay(){
        service.dealExpireNoPay();
        return Result.resultInstance().success();
    }

}
