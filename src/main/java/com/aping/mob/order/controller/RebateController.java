package com.aping.mob.order.controller;

import com.aping.common.model.Result;
import com.aping.mob.order.dto.RebateDto;
import com.aping.mob.order.service.RebateService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/rebate")
@Api(tags = "返利")
@Slf4j
public class RebateController {
    @Resource
    private RebateService rebateService;

    @ApiOperation(value = "查询返利")
    @PostMapping("/query")
    public Result queryList(@RequestBody @Validated RebateDto dto){
        return rebateService.queryList(dto);
    }

}
