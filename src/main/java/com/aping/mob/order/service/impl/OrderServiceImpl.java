package com.aping.mob.order.service.impl;

import cn.hutool.core.date.DateUtil;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisLockHelper;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.mob.flow.vo.WeChatVo;
import com.aping.manage.goods.service.ImageService;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.cart.dao.CartDao;
import com.aping.mob.flow.dao.AmountDao;
import com.aping.mob.flow.dao.WeChatDao;
import com.aping.mob.flow.service.AmountService;
import com.aping.mob.flow.vo.AmountVo;
import com.aping.mob.mall.service.MallService;
import com.aping.mob.order.dao.OrderDao;
import com.aping.mob.order.dao.RebateDao;
import com.aping.mob.order.dto.OrderDetailDto;
import com.aping.mob.order.dto.OrderDto;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.*;
import com.aping.mob.user.dao.AddressDao;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.user.dto.UserInfoDto;
import com.aping.mob.user.vo.AddressVo;
import com.aping.mob.user.vo.UserInfoVo;
import com.aping.mob.wechat.service.WeChatPayService;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDao orderDao;
    @Resource
    private CartDao cartDao;
    @Resource
    private AddressDao addressDao;
    @Resource
    private MallService mallService;
    @Resource
    private ImageService imageService;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private RebateDao rebateDao;
    @Resource
    private SysParamService sysParamService;
    @Resource
    private RedisLockHelper redisLockHelper;
    @Resource
    private AmountDao amountDao;
    @Resource
    private WeChatDao weChatDao;
    @Resource
    private WeChatPayService weChatPayService;
    @Resource
    private AmountService amountService;
    @Resource
    private RebateService rebateService;

    @Override
    public Result queryList(OrderDto dto) {
        if(dto.getPageSize() == 0){
            dto.setPageSize(SysConstant.page_size);
        }
        String openId = dto.getOpenId();;
        String nowTime = CommonUtil.getSeverTime();
        PageHelper.startPage(dto.getPageNum(),dto.getPageSize());
        List<OrderVo> orderVoList = orderDao.queryOrder(openId, dto.getOrderStatus(),nowTime);
        List<String> orderIdList = orderVoList.stream().map(OrderVo::getOrderId).collect(Collectors.toList());
        List<OrderExpressVo> expressVoList = null;
        List<ExpressDetailVo> list;
        if(!orderIdList.isEmpty()){
            expressVoList  = orderDao.queryExpress(orderIdList);
            list = orderDao.queryExpressDetail(orderIdList);
            expressVoList.forEach(orderExpressVo -> {
                orderExpressVo.setList(list.stream().filter(expressDetailVo ->
                        orderExpressVo.getExpressNumber().equals(
                                expressDetailVo.getExpressNumber())).collect(Collectors.toList()));
            });
        }
        if(!orderVoList.isEmpty()){
            // 订单详情列表
            List<OrderDetailVo> detailVoList = orderDao.queryOrderDetail(openId);
            // 地址列表
            List<AddressVo> addressVoList = addressDao.query(openId);
            HashMap<String,AddressVo> addressVoHashMap = new HashMap<>();
            addressVoList.forEach(addressVo -> {
                addressVoHashMap.put(addressVo.getAddressId(),addressVo);
            });
            List<OrderExpressVo> finalExpressVoList = expressVoList;
            orderVoList.forEach(orderVo -> {
                orderVo.setOrderDetailVoList(detailVoList.stream().filter(orderDetailVo ->
                                orderDetailVo.getOrderId().equals(orderVo.getOrderId()))
                        .collect(Collectors.toList()));
                orderVo.setAddressVo(addressVoHashMap.get(orderVo.getAddressId()));
                if(!orderIdList.isEmpty()){
                    orderVo.setExpressVoList(finalExpressVoList.stream().filter(orderExpressVo ->
                                    orderExpressVo.getOrderId().equals(orderVo.getOrderId()))
                            .collect(Collectors.toList()));
                }
            });
        }
        return Result.resultInstance().success(new PageInfo<>(orderVoList));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addOrder(OrderDto dto) {
        UserInfoVo userInfoVo = userInfoDao.queryByOpenId(dto.getOpenId());
        if("1".equals(userInfoVo.getIsBack())){
            return Result.resultInstance().fail(SysCodeEnum.GOODS_STOCK_ERROR);
        }
        dto.setOrderId("O"+IdUtil.getUUID());
        String orderId = dto.getOrderId();
        dto.setInsertDate(CommonUtil.getSeverDate());
        dto.setInsertTime(CommonUtil.getSeverTime());
        dto.setExpireTime(CommonUtil.getOrderExpireTime());
        List<OrderDetailDto> detailVoList = cartDao.queryList(dto.getCartIdList());
        for (OrderDetailDto detailVo : detailVoList){
            detailVo.setOrderId(orderId);
            detailVo.setFlowId(IdUtil.getUUID());
            detailVo.setImgUrl(imageService.getImgUrl(detailVo.getImgId().split(",")[0]));
            detailVo.setInsertDate(CommonUtil.getSeverDate());
            detailVo.setInsertTime(CommonUtil.getSeverTime());
            if(mallService.checkGoodsStock(detailVo.getGoodsId(),detailVo.getGoodsNum())){
                return Result.resultInstance().fail("1006",detailVo.getGoodsName() + "库存不足");
            }
        }
        if(!detailVoList.isEmpty()){
            orderDao.addOrder(dto);
            orderDao.addOrderDetail(detailVoList);
            // 删除用于生成订单的购物车的商品
            cartDao.delCartList(dto.getCartIdList());
            return Result.resultInstance().success("200","成功",orderId);
        }
        return Result.resultInstance().fail(SysCodeEnum.ADD_ORDER_FAIL);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result cancelOrder(String orderId) {
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "CANCEL_ORDER_" + orderId;
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return Result.resultInstance().fail(SysCodeEnum.SYSTEM_BUSY);
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            OrderVo orderVo = orderDao.queryOrderById(orderId);
            Result result = Result.resultInstance().success();
            //  0 和 1 状态可取消订单 当部分发货或完全发货无法取消订单
            if("0".equals(orderVo.getOrderStatus())){
                // 释放订单
                releaseOrder(new ArrayList<>(Collections.singleton(orderId)));
                return Result.resultInstance().success();
            }else if("1".equals(orderVo.getOrderStatus())){
                // 查询订单是否有余额支付流水
                AmountVo amountVo = amountDao.queryByParam(orderId,"2","0");
                // 查询订单是否有成功支付的微信支付流水
                WeChatVo weChatVo = weChatDao.queryByParam(orderId,"2","1");
                if(ObjectUtils.isEmpty(weChatVo) && ObjectUtils.isNotEmpty(amountVo)){
                    // 生成余额退款流水 并将用户的余额增加
                    // 从余额中扣 生成一条扣除储值流水并返回支付成功
                    String openId = orderVo.getOpenId();
                    // 余额支付金额
                    BigDecimal spentAmount = new BigDecimal(amountVo.getSpentAmount());
                    amountService.createAmountFlow(openId, orderId, spentAmount,"2");
                    // 增加账户余额
                    BigDecimal accountBalances = new BigDecimal(userInfoDao.getAmount(openId));
                    BigDecimal balance = accountBalances.add(spentAmount);
                    userInfoDao.updateUserAmount(openId, String.valueOf(balance));
                    rebateService.cancelRebate(orderId);
                    // 释放订单
                    releaseOrder(new ArrayList<>(Collections.singleton(orderId)));
                    // 处理销量和库存
                    dealCancelVolumeAndStock(orderId);
                } else if (ObjectUtils.isNotEmpty(weChatVo)) {
                    // 生成微信退款流水 以及退款 生成余额退款流水 并将用户的余额增加
                    result = weChatPayService.wxPayRefund(orderId,
                            new BigDecimal(weChatVo.getWechatAmount()),
                            "1",orderVo.getOrderPrice(),"1");
                }
                return result;
            }
            else {
                return Result.resultInstance().fail(SysCodeEnum.CANCEL_ORDER_FAIL);
            }
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().fail(SysCodeEnum.CANCEL_ORDER_ERROR);
        }finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }

    @Override
    public Result queryOrderDetail(String orderId, String openId) {
        OrderVo orderVo = orderDao.queryOrderById(orderId);
        Date nowDate = DateUtil.date();
        int temp = DateUtil.compare(nowDate,DateUtil.parse(
                orderVo.getExpireTime(),"yyyy-MM-dd HH:mm:ss"));
        if("0".equals(orderVo.getOrderStatus()) && temp > 0){
            orderVo = null;
        }
        if(ObjectUtils.isEmpty(orderVo)){
            return Result.resultInstance().success(SysCodeEnum.ORDER_EXPIRE_CANCEL);
        }else {
            List<OrderDetailVo> detailVoList = orderDao.queryOrderDetailByOrderId(orderId, openId);
            AddressVo addressVo = addressDao.queryById(orderVo.getAddressId());
            orderVo.setAddressVo(addressVo);
            orderVo.setOrderDetailVoList(detailVoList);
            return Result.resultInstance().success(orderVo);
        }
    }

    /**
     * 确认收货
     * @param orderId 订单编号
     * @return 结果集
     */
    @Override
    public Result confirmReceipt(String orderId) {
        OrderVo orderVo = orderDao.queryOrderById(orderId);
        if("3".equals(orderVo.getOrderStatus())){
            orderDao.updateOrderByParam(orderId,"6",
                    CommonUtil.getSeverTime(),CommonUtil.getFinishOrderTime());
            return Result.resultInstance().success();
        }else {
            return Result.resultInstance().fail(SysCodeEnum.NO_RECEIPT);
        }
    }

    /**
     *  付款成功后处理库存跟销量
     */
    @Transactional(rollbackFor = Exception.class)
    public void dealSuccessVolumeAndStock(String orderId){
        List<VsVo>  voList =  orderDao.queryVolumeAndStock(orderId);
        voList.forEach(vsVo -> {
            int goodsNum = Integer.parseInt(vsVo.getGoodsNum());
            // 库存不为-1时减少库存
            if(!"-1".equals(vsVo.getGoodsStock())){
                int goodsStock = Integer.parseInt(vsVo.getGoodsStock());
                vsVo.setGoodsStock(String.valueOf(goodsStock - goodsNum));
            }
            // 增加销售量
            int goodsVolume = Integer.parseInt(vsVo.getGoodsVolume());
            vsVo.setGoodsVolume(String.valueOf(goodsVolume + goodsNum));
            vsVo.setLstUpdateDate(CommonUtil.getSeverDate());
            vsVo.setLstUpdateTime(CommonUtil.getSeverTime());
        });
        if(!voList.isEmpty()){
            orderDao.batchVoAdSk(voList);
        }
    }

    /**
     *  取消订单后处理库存跟销量
     */
    @Transactional(rollbackFor = Exception.class)
    public void dealCancelVolumeAndStock(String orderId){
        List<VsVo>  voList =  orderDao.queryVolumeAndStock(orderId);
        voList.forEach(vsVo -> {
            int goodsNum = Integer.parseInt(vsVo.getGoodsNum());
            // 库存不为-1时增加库存
            if(!"-1".equals(vsVo.getGoodsStock())){
                int goodsStock = Integer.parseInt(vsVo.getGoodsStock());
                vsVo.setGoodsStock(String.valueOf(goodsStock + goodsNum));
            }
            // 减少销售量
            int goodsVolume = Integer.parseInt(vsVo.getGoodsVolume());
            vsVo.setGoodsVolume(String.valueOf(goodsVolume - goodsNum));
            vsVo.setLstUpdateDate(CommonUtil.getSeverDate());
            vsVo.setLstUpdateTime(CommonUtil.getSeverTime());
        });
        if(!voList.isEmpty()){
            orderDao.batchVoAdSk(voList);
        }
    }

    /**
     * 处理还未付款的过期订单
     * 将相同的商品数量累加到最新时间的那一个
     * 批量取消订单 将状态置为5
     * 查询用户购物车将商品编号形同的排序后加在最新时间的那一个
     */
    @Transactional(rollbackFor = Exception.class)
    public void dealExpireNoPay(){
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "BATCH_DEAL_EXPIRE_ORDER";
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return;
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            // 处理访问量
            long timeOne = System.currentTimeMillis();
            log.info(">>>>>>开始处理待支付已过期的订单<<<<<<");
            List<String> orderIdList = orderDao.queryExpireOrder(CommonUtil.getSeverTime());
            log.info(">>>>>>此次处理共有{}个待支付已过期的订单<<<<<<", orderIdList.size());
            releaseOrder(orderIdList);
            log.info(">>>>>>处理过期订单总共耗时{}ms<<<<<<", System.currentTimeMillis() - timeOne);
        }catch (Exception e){
            e.printStackTrace();
            log.error(">>>>>>过期订单处理失败<<<<<<");
        } finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }

    /**
     * 释放订单
     * 处理释放后购物车中出现相同的商品问题
     * @param orderIdList 订单编号列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void releaseOrder(List<String> orderIdList){
        if(!orderIdList.isEmpty()){
            List<ExpireVo> expireGoodsNum = orderDao.queryExpireGoodsNum(orderIdList);
            Map<String, Integer> map = new HashMap<>();
            expireGoodsNum.forEach(expireVo -> {
                map.put(expireVo.getOpenId() + "_" + expireVo.getGoodsId(),expireVo.getGoodsNum());
            });
            List<ExpireVo> operateCart = orderDao.queryOperateCart(orderIdList);
            operateCart.forEach(expireVo -> {
                int tempOne = map.get(expireVo.getOpenId() + "_" + expireVo.getGoodsId());
                if("0".equals(expireVo.getIsDel())){
                    int tempTwo = expireVo.getGoodsNum();
                    expireVo.setGoodsNum(tempOne + tempTwo);
                }else {
                    expireVo.setGoodsNum(tempOne);
                }
                expireVo.setLstUpdateTime(CommonUtil.getSeverTime());
            });
            int contOne = orderDao.batchUpdateExpireCart(operateCart);
            int contTwo = orderDao.batchCancelOrder(orderIdList);
            log.info(">>>>>>更新购物车{}条数据<<<<<<",contOne);
            log.info(">>>>>>取消{}条订单数据<<<<<<",contTwo);
        }else {
            log.info(">>>>>>不存在待支付已过期的订单<<<<<<");
        }
    }

    /**
     *  在全部发货后即状态为3时
     *  且当前时间比确认收货时间大
     *  则系统进行自动确认收货
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void autoConfirmReceipt(){
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "AUTO_CONFIRM_RECEIPT";
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return;
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            List<String> orderIdList = orderDao.queryAutoConfirmReceipt(CommonUtil.getSeverTime());
            // 将其变成确认收货 并写入完成时间 6 finishOrderTime
            List<AutoOrderVo> autoOrderVoList = new ArrayList<>();
            for(String orderId : orderIdList){
                AutoOrderVo autoOrderVo = new AutoOrderVo();
                autoOrderVo.setOrderId(orderId);
                autoOrderVo.setOrderStatus("6");
                autoOrderVo.setFinishOrderTime(CommonUtil.getFinishOrderTime());
            }
            if(!autoOrderVoList.isEmpty()){
                int count = orderDao.batchAutoConfirmReceipt(autoOrderVoList);
                log.info(">>>>>>订单自动确认收货共{}条数据<<<<<<",count);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error(">>>>>>订单自动确认收货失败<<<<<<");
        } finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }

    /**
     *  24小时后变成”已完成“状态
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void autoFinishOrder(){
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "AUTO_FINISH_ORDER";
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return;
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            List<String> orderIdList = orderDao.queryAutoFinishOrder(CommonUtil.getSeverTime());
            if(!orderIdList.isEmpty()){
                int count = orderDao.batchAutoFinishOrder(orderIdList);
                log.info(">>>>>>订单自动已完成{}条数据<<<<<<",count);
                // 处理返利 将返利加上到余额上
                List<RebateVo> rebateVoList = orderDao.queryRebateList(orderIdList);
                List<UserInfoDto> balanceList = new ArrayList<>();
                rebateVoList.forEach(rebateVo -> {
                    // 增加账户余额
                    BigDecimal accountBalances = new BigDecimal(userInfoDao.getAmount(rebateVo.getOpenId()));
                    BigDecimal balance = accountBalances.add(new BigDecimal(rebateVo.getRebateMoney()));
                    UserInfoDto userInfoDto = new UserInfoDto();
                    userInfoDto.setOpenId(rebateVo.getOpenId());
                    userInfoDto.setAccountBalances(String.valueOf(balance));
                    balanceList.add(userInfoDto);
                });
                int countOne = orderDao.batchUpdateRebateFinish(orderIdList);
                int countTwo = userInfoDao.batchUpdateUserAmount(balanceList);
                log.info(">>>>>>订单自动已完成{}条数据<<<<<<",count);
                log.info(">>>>>>已完成返利{}条数据<<<<<<",countOne);
                log.info(">>>>>>返利完成增加余额{}条数据<<<<<<",countTwo);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error(">>>>>>订单自动已完成失败<<<<<<");
        } finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }

}
