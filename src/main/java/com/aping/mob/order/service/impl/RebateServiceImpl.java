package com.aping.mob.order.service.impl;

import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.StringUtil;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.order.dao.RebateDao;
import com.aping.mob.order.dto.RebateDto;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.RebateVo;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.user.vo.UserInfoVo;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class RebateServiceImpl implements RebateService {

    @Resource
    private RebateDao rebateDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SysParamService sysParamService;

    @Override
    public Result queryList(RebateDto dto) {
        if(dto.getPageSize() == 0){
            dto.setPageSize(SysConstant.page_size);
        }
        PageHelper.startPage(dto.getPageNum(),dto.getPageSize());
        List<RebateVo> rebateVoList = rebateDao.queryList(dto);
        String middleMoney = rebateDao.queryTotalMoney(dto.getOpenId(), "1");
        String successMoney = rebateDao.queryTotalMoney(dto.getOpenId(), "2");
        Map<String,String> map = new HashMap<>();
        Map<String,Object> resultMap = new HashMap<>();
        map.put("successMoney",successMoney);
        map.put("middleMoney",middleMoney);
        resultMap.put("money",map);
        resultMap.put("pageInfo",new PageInfo<>(rebateVoList));
        return Result.resultInstance().success(resultMap);
    }

    /**
     * 生成返利流水
     * @param openId 用户唯一标识
     * @param orderId 订单编号
     * @param orderPrice  订单金额
     */
    @Override
    public void createRebate(String openId,String orderId,String orderPrice){
        String inviterOpenId = userInfoDao.queryInviterOpenId(openId);
        if(StringUtil.isNotEmpty(inviterOpenId)){
            String percentage = sysParamService.getSysParamRedis("REBATE_PERCENTAGE").replace("%","");
            BigDecimal scale = new BigDecimal(percentage).divide(new BigDecimal("100"),2, RoundingMode.DOWN);
            BigDecimal dOrderPrice = new BigDecimal(orderPrice);
            BigDecimal minPrice = new BigDecimal("0.01").divide(scale,RoundingMode.DOWN);
            if(dOrderPrice.compareTo(minPrice) >= 0){
                String rebateMoney = dOrderPrice.multiply(scale).setScale(2,RoundingMode.DOWN).toString();
                RebateDto dto = new RebateDto();
                dto.setOrderId(orderId);
                dto.setOpenId(inviterOpenId);
                dto.setOrderOpenId(openId);
                dto.setRebateMoney(rebateMoney);
                dto.setRebateStatus("1");
                dto.setInsertDate(CommonUtil.getSeverDate());
                dto.setInsertTime(CommonUtil.getSeverTime());
                rebateDao.addRebate(dto);
            }
        }
    }

    @Override
    public void cancelRebate(String orderId) {
        rebateDao.deleteRebate(orderId);
        log.info(">>>>>>[{}]-返利流水被删除<<<<<<",orderId);
    }

    @Override
    public void updateRebate(String orderId, String orderPrice,String refundMoney) {
        RebateVo rebateVo = rebateDao.queryByOrderId(orderId);
        if(ObjectUtils.isNotEmpty(rebateVo)){
            String percentage = sysParamService.getSysParamRedis("REBATE_PERCENTAGE").replace("%","");
            BigDecimal scale = new BigDecimal(percentage).divide(new BigDecimal("100"),2, RoundingMode.DOWN);
            BigDecimal dOrderPrice = new BigDecimal(orderPrice).subtract(new BigDecimal(refundMoney));
            BigDecimal minPrice = new BigDecimal("0.01").divide(scale,RoundingMode.DOWN);
            if(dOrderPrice.compareTo(minPrice) >= 0){
                String rebateMoney = dOrderPrice.multiply(scale).setScale(2,RoundingMode.DOWN).toString();
                rebateDao.updateRebate(orderId,rebateMoney);
            }else {
                rebateDao.deleteRebate(orderId);
            }
        }
        log.info(">>>>>>[{}]-返利流水被更新<<<<<",orderId);
    }


}
