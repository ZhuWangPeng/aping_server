package com.aping.mob.order.service;

import com.aping.common.model.Result;
import com.aping.mob.order.dto.RebateDto;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

public interface RebateService {
    Result queryList(RebateDto dto);

    void createRebate(String openId,String orderId,String orderPrice);

    void cancelRebate(String orderId);

    void updateRebate(String orderId, String orderPrice,String refundMoney);
}
