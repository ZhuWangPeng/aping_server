package com.aping.mob.order.service;

import com.aping.common.model.Result;
import com.aping.mob.order.dto.OrderDto;
import com.aping.platform.requestJson.annotation.RequestJson;
import com.github.pagehelper.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface OrderService {

    Result queryList(OrderDto dto);

    Result addOrder(OrderDto dto);

    Result cancelOrder(String orderId);

    Result queryOrderDetail(String orderId,String openId);

    void dealExpireNoPay();

    Result confirmReceipt(String orderId);

    void dealSuccessVolumeAndStock(String orderId);
    void dealCancelVolumeAndStock(String orderId);

    void releaseOrder(List<String> orderIdList);

    void autoConfirmReceipt();

    void autoFinishOrder();
}
