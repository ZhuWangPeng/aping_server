package com.aping.mob.order.dao;

import com.aping.mob.order.dto.RebateDto;
import com.aping.mob.order.vo.RebateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RebateDao {

    List<RebateVo> queryList(RebateDto dto);

    RebateVo queryByOrderId(String orderId);

    String queryTotalMoney(@Param("openId")String openId,
                           @Param("rebateStatus")String rebateStatus);

    void addRebate(RebateDto rebateDto);

    void deleteRebate(String orderId);

    void updateRebate(@Param("orderId") String orderId,
                      @Param("rebateMoney") String rebateMoney);
}
