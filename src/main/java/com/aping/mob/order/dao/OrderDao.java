package com.aping.mob.order.dao;

import com.aping.mob.order.dto.OrderDetailDto;
import com.aping.mob.order.dto.OrderDto;
import com.aping.mob.order.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface OrderDao {
    List<OrderVo> queryOrder(@Param("openId") String openId,
                        @Param("orderStatus") String orderStatus,
                             @Param("nowTime") String nowTime);
    void addOrder(OrderDto dto);

    List<OrderDetailVo> queryOrderDetail(@Param("openId") String openId);
    void addOrderDetail(@Param("detailDtoList")List<OrderDetailDto> detailDtoList);

    OrderVo queryOrderById(@Param("orderId") String orderId);
    List<OrderDetailVo> queryOrderDetailByOrderId(@Param("orderId") String orderId,
                                                  @Param("openId") String openId);

    List<VsVo> queryVolumeAndStock(@Param("orderId") String orderId);

    void updateOrderAmPayInfo(@Param("orderId") String orderId,
                        @Param("amPayTime")String amPayTime,
                        @Param("amPayPrice")String amPayPrice);

    void updateOrderAmPayInfoByCom(@Param("orderId") String orderId,
                              @Param("amPayTime")String amPayTime,
                              @Param("amPayPrice")String amPayPrice);

    void updateOrderWxPayInfo(@Param("orderId") String orderId,
                            @Param("wxPayTime")String amPayTime,
                            @Param("wxPayPrice")String amPayPrice);
    // 批量更新库存和销量
    void batchVoAdSk(@Param("list")List<VsVo> list);

    List<String> queryExpireOrder(@Param("nowTime") String nowTime);

    List<ExpireVo> queryExpireGoodsNum(@Param("list") List<String> orderIdList);

    List<ExpireVo> queryOperateCart(@Param("list") List<String> orderIdList);

    int batchUpdateExpireCart(@Param("list") List<ExpireVo> list);

    int batchCancelOrder(@Param("list") List<String> orderIdList);

    void updateOrderStatus(@Param("orderId") String orderId,
                           @Param("orderStatus") String orderStatus,
                           @Param("refundPrice") String refundPrice,
                           @Param("confirmReceiptTime") String confirmReceiptTime);

    void updateOrderByParam(@Param("orderId") String orderId,
                           @Param("orderStatus") String orderStatus,
                           @Param("confirmReceiptTime") String confirmReceiptTime,
                           @Param("finishOrderTime") String finishOrderTime);

    List<OrderExpressVo> queryExpress(@Param("list") List<String> orderIdList);
    List<ExpressDetailVo> queryExpressDetail(@Param("list") List<String> orderIdList);

    List<String> queryAutoConfirmReceipt(@Param("confirmReceiptTime") String confirmReceiptTime);

    int batchAutoConfirmReceipt(@Param("list") List<AutoOrderVo> autoOrderVoList);

    List<String> queryAutoFinishOrder(@Param("finishOrderTime") String finishOrderTime);

    int batchAutoFinishOrder(@Param("list") List<String> orderIdList);

    List<RebateVo> queryRebateList(@Param("list") List<String> orderIdList);

    int batchUpdateRebateFinish(@Param("list") List<String> orderIdList);

    String queryLstDeliver(@Param("orderId")String orderId);
}
