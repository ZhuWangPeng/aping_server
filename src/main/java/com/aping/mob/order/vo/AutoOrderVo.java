package com.aping.mob.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AutoOrderVo {
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("订单状态 0-待付款 1-待发货 2-部分发货 3-待发货 4-已完成")
    private String orderStatus;
    @ApiModelProperty("订单完成时间")
    private String finishOrderTime;
}
