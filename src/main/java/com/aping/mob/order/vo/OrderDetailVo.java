package com.aping.mob.order.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.poi.hpsf.Decimal;
@Data
public class OrderDetailVo {
    private String flowId;
    @ApiModelProperty("用户标识")
    private String openId;
    @ApiModelProperty("订单编号")
    @JsonIgnore
    private String orderId;
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品图片地址,选第一张")
    private String imgUrl;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品数量")
    private int goodsNum;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
