package com.aping.mob.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrderExpressVo {
    @ApiModelProperty("快递单号")
    private String expressNumber;
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("快递公司id")
    private String expressId;
    @ApiModelProperty("快递公司id")
    private String expressName;
    @ApiModelProperty("快递费用")
    private String expressFee;
    @ApiModelProperty("发货日期")
    private String deliverDate;
    @ApiModelProperty("发货时间")
    private String deliverTime;
    @ApiModelProperty("写入时间")
    private String insertTime;
    List<ExpressDetailVo> list;
}
