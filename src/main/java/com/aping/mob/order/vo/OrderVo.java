package com.aping.mob.order.vo;

import com.aping.mob.user.vo.AddressVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class OrderVo {
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("用户标识")
    private String openId;
    @ApiModelProperty("订单价格")
    private String orderPrice;
    @ApiModelProperty("退款金额")
    private String refundPrice;
    @ApiModelProperty("订单状态 0-待付款 1-待发货 2-部分发货 3-待发货 4-已完成")
    private String orderStatus;
    @ApiModelProperty("订单备注")
    private String orderMemo;
    @ApiModelProperty("地址id")
    private String addressId;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("订单过期时间")
    private String expireTime;
    @ApiModelProperty("支付时间")
    private String payTime;
    @ApiModelProperty("支付金额")
    private String payPrice;
    @ApiModelProperty("订单详情列表")
    private List<OrderDetailVo> OrderDetailVoList;
    @ApiModelProperty("地址信息")
    private AddressVo addressVo;
    @ApiModelProperty("快递列表")
    private List<OrderExpressVo> expressVoList;
}
