package com.aping.mob.order.vo;

import lombok.Data;

@Data
public class ExpireVo {
    private String cartId;
    private String openId;
    private String goodsId;
    private int goodsNum;
    private String isDel;
    private String lstUpdateTime;
}
