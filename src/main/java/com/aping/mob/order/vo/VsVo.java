package com.aping.mob.order.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class VsVo {
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品数量")
    private String goodsNum;
    @ApiModelProperty("商品库存")
    private String goodsStock;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("最后一次更新日期")
    private String lstUpdateDate;
    @ApiModelProperty("最后一次更新时间")
    private String lstUpdateTime;
}
