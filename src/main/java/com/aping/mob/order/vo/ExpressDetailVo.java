package com.aping.mob.order.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ExpressDetailVo {
    @JsonIgnore
    private String expressNumber;
    @ApiModelProperty("商品id")
    private String goodsId;
    private String goodsName;
    @ApiModelProperty("商品数量")
    private int goodsNum;
}
