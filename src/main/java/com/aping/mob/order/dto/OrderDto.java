package com.aping.mob.order.dto;

import com.aping.mob.order.valid.CancelOrder;
import com.aping.mob.order.valid.OrderAdd;
import com.aping.mob.order.valid.OrderDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
@Data
public class OrderDto{
    @ApiModelProperty("订单编号")
    @NotBlank(message = "订单编号不可为空",groups = {OrderDetail.class})
    private String orderId;
    @ApiModelProperty("用户标识")
    @NotBlank(message = "openId不可为空",groups = {OrderDetail.class})
    private String openId; // refund_price
    @ApiModelProperty("订单价格")
    private String orderPrice;
    @ApiModelProperty("退款金额")
    private String refundPrice;
    @ApiModelProperty("订单状态 0-待付款 " +
            "1-待发货 2-部分发货 " +
            "3-全部发货 4-已完成 5-取消订单 " +
            "6-确认收货 7-部分退款 8-全部退款")
    private String orderStatus;
    @ApiModelProperty("订单备注")
    private String orderMemo;
    @ApiModelProperty("地址id")
    private String addressId;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("订单过期时间")
    private String expireTime;
    @ApiModelProperty("支付时间")
    private String payTime;
    @ApiModelProperty("支付金额")
    private String payPrice;
    @ApiModelProperty("购物车id列表")
    @NotNull(message = "购物车id列表不可为空",groups = OrderAdd.class)
    private List<String> cartIdList;
    private int pageNum;
    private int pageSize;
}
