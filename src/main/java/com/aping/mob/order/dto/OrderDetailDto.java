package com.aping.mob.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class OrderDetailDto {
    @ApiModelProperty("流水号")
    private String flowId;
    @ApiModelProperty("购物车流水号")
    private String cartId;
    @ApiModelProperty("用户标识")
    private String openId;
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品图片地址,选第一张")
    private String imgUrl;
    private String imgId;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品数量")
    private int goodsNum;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
