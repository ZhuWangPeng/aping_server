package com.aping.mob.cart.service;

import com.aping.common.model.Result;
import com.aping.mob.cart.dto.CartDto;

import java.util.List;

public interface CartService {
    Result query(String openId, int pageNum);

    Result addCart(CartDto dto);

    Result updateCart(CartDto dto);

    Result delCart(String cartId);

    void delCartList(List<String> flowIdList);
}
