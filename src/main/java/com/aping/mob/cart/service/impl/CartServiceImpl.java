package com.aping.mob.cart.service.impl;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import com.aping.common.redis.RedisLockHelper;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.StringUtil;
import com.aping.manage.goods.service.ImageService;
import com.aping.mob.cart.dao.CartDao;
import com.aping.mob.cart.dto.CartDto;
import com.aping.mob.cart.service.CartService;
import com.aping.mob.cart.vo.CartVo;
import com.aping.mob.mall.service.MallService;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CartServiceImpl implements CartService {

    @Resource
    private CartDao dao;
    @Resource
    private MallService mallService;
    @Resource
    private ImageService imageService;
    @Resource
    private RedisLockHelper redisLockHelper;

    @Override
    public Result query(String openId, int pageNum) {
        if(pageNum == 0){
            pageNum = 1;
        }
        int pageStart = (pageNum - 1) * SysConstant.page_size;
        int total = dao.count(openId);
        List<CartVo> list = dao.query(openId,pageStart,SysConstant.page_size);
        list.forEach(cartVo -> {
            if(StringUtil.isNotEmpty(cartVo.getImgId())){
                cartVo.setImgUrlList(imageService.getImgUrlList(cartVo.getImgId()));
            }
        });
        Map<String,Object> map = new HashMap<>();
        map.put("total",total);
        map.put("list",list);
        return Result.resultInstance().success(map);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addCart(CartDto dto) {
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "APING_ADD_CART_" + dto.getGoodsId();
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return Result.resultInstance().fail(SysCodeEnum.SYSTEM_BUSY);
            }
            // 查询该商品的库存
            if (mallService.checkGoodsStock(dto.getGoodsId(), dto.getGoodsNum())) {
                return Result.resultInstance().fail(SysCodeEnum.GOODS_STOCK_ERROR);
            }
            // 同一个商品信息保存第二条
            CartVo cartVo = dao.queryCart(dto.getOpenId(), dto.getGoodsId());
            if (ObjectUtils.isNotEmpty(cartVo)) {
                if (dto.getGoodsNum() == 0) {
                    dao.delCart(cartVo.getCartId());
                } else {
                    dto.setCartId(cartVo.getCartId());
                    dto.setGoodsNum(dto.getGoodsNum());
                    dto.setLstUpdateTime(CommonUtil.getSeverTime());
                    dao.updateCart(dto);
                }
            } else {
                if (dto.getGoodsNum() == 0) {
                    return Result.resultInstance().success(SysCodeEnum.GOODS_NUM_IS_ZERO);
                }
                // 正常写入
                dto.setCartId(IdUtil.getUUID());
                dto.setInsertTime(CommonUtil.getSeverTime());
                dto.setLstUpdateTime(CommonUtil.getSeverTime());
                dao.addCart(dto);
            }
            redisLockHelper.unlock("APING_ADD_CART_" + dto.getGoodsId(), String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", "APING_ADD_CART_" + dto.getGoodsId());
            return Result.resultInstance().success();
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().fail(SysCodeEnum.ADD_CART_ERROR);
        }finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }

    @Override
    public Result updateCart(CartDto dto) {
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        dao.updateCart(dto);
        return Result.resultInstance().success();
    }

    @Override
    public Result delCart(String cartId) {
        dao.delCart(cartId);
        return Result.resultInstance().success("商品删除成功");
    }

    @Override
    public void delCartList(List<String> flowIdList) {
        dao.delCartList(flowIdList);
    }
}
