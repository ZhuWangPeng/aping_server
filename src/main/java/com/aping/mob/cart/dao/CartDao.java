package com.aping.mob.cart.dao;

import com.aping.mob.order.dto.OrderDetailDto;
import com.aping.mob.cart.dto.CartDto;
import com.aping.mob.cart.vo.CartVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CartDao {

    List<OrderDetailDto> queryList(@Param("cartIdList") List<String> cartIdList);

    List<CartVo> query(@Param("openId") String openId,
                       @Param("pageStart") int pageStart,
                       @Param("pageSize") int pageSize);

    void addCart(CartDto dto);

    CartVo queryCart(@Param("openId") String openId,
                     @Param("goodsId") String goodsId);

    void updateCart(CartDto dto);

    void delCart(@Param("cartId") String cartId);

    void delCartList(@Param("cartIdList") List<String> cartIdList);

    Integer count(@Param("openId") String openId);

    void drawCart(@Param("orderId") String orderId);
}
