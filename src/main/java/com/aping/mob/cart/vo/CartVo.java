package com.aping.mob.cart.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CartVo {
    @ApiModelProperty("流水号")
    private String cartId;
//    @ApiModelProperty("用户唯一标识")
//    private String openId;
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品数量")
    private int goodsNum;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("商品库存")
    private String goodsStock;
    @ApiModelProperty("商品图片地址，英文逗号隔开")
    @JsonIgnore
    private String imgId;
    private List<String> imgUrlList;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
