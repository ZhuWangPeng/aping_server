package com.aping.mob.cart.controller;

import com.aping.common.model.Result;
import com.aping.mob.cart.dto.CartDto;
import com.aping.mob.cart.service.CartService;
import com.aping.mob.cart.valid.CartQuery;
import com.aping.platform.requestJson.annotation.RequestJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/mob/cart")
@Api(tags = "购物车")
@Slf4j
public class CartController {

    @Resource
    private CartService service;

    @ApiOperation(value = "购物车列表")
    @PostMapping("/query")
    public Result query(@RequestBody @Validated(value = {CartQuery.class}) CartDto cartDto){
        return service.query(cartDto.getOpenId(), cartDto.getPageNum());
    }

    @ApiOperation(value = "添加到购物车")
    @PostMapping("/add")
    public Result addCart(@RequestBody CartDto dto){
        return service.addCart(dto);
    }

    @ApiOperation(value = "修改购物车")
    @PostMapping("/update")
    public Result updateCart(@RequestBody CartDto dto){
        return service.updateCart(dto);
    }

    @ApiOperation(value = "删除购物车中的商品")
    @PostMapping("/del")
    public Result delCart(@RequestJson("cartId") String cartId){
        return service.delCart(cartId);
    }

}
