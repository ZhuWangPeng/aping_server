package com.aping.mob.cart.dto;

import com.aping.mob.cart.valid.CartQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CartDto {
    @ApiModelProperty("购物车主键")
    private String cartId;
    @ApiModelProperty("用户唯一标识")
    @NotBlank(message = "openId不能为空",groups = {CartQuery.class})
    private String openId;
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品数量")
    private int goodsNum;
    @NotNull(message = "页码不能为空",groups = {CartQuery.class})
    private int pageNum;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("是否删除")
    private String isDel;
    @ApiModelProperty("最后一次更新时间")
    private String lstUpdateTime;
}
