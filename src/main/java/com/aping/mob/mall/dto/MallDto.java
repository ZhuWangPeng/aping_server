package com.aping.mob.mall.dto;

import com.aping.mob.mall.valid.GoodsDetail;
import com.aping.mob.mall.valid.MallPageQuery;
import com.aping.mob.mall.valid.MallQuery;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MallDto {
    private String openId;
    private int pageNum;
    @NotBlank(message = "菜单id不可为空",groups = {MallPageQuery.class})
    private String menuId;
    @NotBlank(message = "商品id不可为空",groups = {GoodsDetail.class})
    private String goodsId;
    private String goodsName;
}
