package com.aping.mob.mall.controller;

import com.aping.common.model.Result;
import com.aping.mob.mall.dto.MallDto;
import com.aping.mob.mall.service.MallService;
import com.aping.mob.mall.valid.GoodsDetail;
import com.aping.mob.mall.valid.MallPageQuery;
import com.aping.mob.mall.valid.MallQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/mall")
@Api(tags = "商城")
@Slf4j
public class MallController {

    @Resource
    private MallService mallService;


    @ApiOperation(value = "广告栏位列表")
    @GetMapping("/getBannerList")
    public Result getBannerList(){
        return mallService.getBannerList();
    }


    @ApiOperation(value = "商城列表")
    @PostMapping("/queryList")
    public Result queryList(@RequestBody MallDto dto){
        return mallService.queryList(dto.getOpenId(), dto.getGoodsName());
    }

    @ApiOperation(value = "页码商城列表")
    @PostMapping("/queryGoodsList")
    public Result queryGoodsList(@RequestBody @Validated(value = MallPageQuery.class) MallDto dto){
        return mallService.queryGoodsList(dto);
    }

    @ApiOperation(value = "模糊查询商品")
    @PostMapping("/queryGoodsLike")
    public Result queryGoods(@RequestBody String goodsId){
        return null;
    }

    @ApiOperation(value = "查询详情")
    @PostMapping("/queryGoodsDetail")
    public Result queryGoodsDetail(@RequestBody  @Validated(value = GoodsDetail.class) MallDto dto){
        return mallService.queryGoodsDetail(dto.getGoodsId(),dto.getOpenId());
    }


}
