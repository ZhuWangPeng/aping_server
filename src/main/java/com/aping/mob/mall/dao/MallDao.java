package com.aping.mob.mall.dao;

import com.aping.mob.mall.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MallDao {
    List<ImageVo> queryBannerList();

    List<MenuVo> queryMenuList();

    List<GoodsVo> queryGoodsList(@Param("openId")String openId, @Param("goodsName")String goodsName);

    List<GoodsVo> queryGoodsListByPageNum(@Param("menuId")String menuId,
                                          @Param("openId")String openId,
                                          @Param("goodsName")String goodsName,
                                          @Param("pageStart") int pageStart,
                                          @Param("pageEnd") int pageEnd);

    // 查询商品详情
    GoodsDetailVo queryGoodsDetail(@Param("goodsId")String goodsId,@Param("openId")String openId);

    // 查询商品库存
    GoodsVo queryGoodsStock(@Param("goodsId")String goodsId);
    List<GoodsVo> queryGoodsStockList(@Param("goodsIdList")List<String> goodsIdList);

    List<GoodsCountVo> count();


}
