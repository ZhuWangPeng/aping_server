package com.aping.mob.mall.service;

import com.aping.common.model.Result;
import com.aping.mob.mall.dto.MallDto;


public interface MallService {

    Result getBannerList();

    Result queryList(String openId,String goodsName);

    Result queryGoodsList(MallDto dto);

    Result queryGoodsDetail(String goodsId,String openId);

    boolean checkGoodsStock(String goodsId,int goodsNum);
}
