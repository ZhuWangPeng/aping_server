package com.aping.mob.mall.service.impl;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.StringUtil;
import com.aping.manage.goods.service.ImageService;
import com.aping.mob.mall.dao.MallDao;
import com.aping.mob.mall.dto.MallDto;
import com.aping.mob.mall.service.MallService;
import com.aping.mob.mall.vo.*;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MallServiceImpl implements MallService {

    @Resource
    private MallDao mallDao;
    @Resource
    private RedisHandle redisHandle;
    @Resource
    private ImageService imageService;

    @Override
    public Result getBannerList() {
        List<ImageVo> list;
        if(redisHandle.exists(SysConstant.REDIS_KEY_BANNER_IMAGE_URL)){
            list = (List<ImageVo>) redisHandle.get(SysConstant.REDIS_KEY_BANNER_IMAGE_URL);
        }else {
            list = mallDao.queryBannerList();
            initBannerList();
        }
        return Result.resultInstance().success(list);
    }

    @Override
    public Result queryList(String openId,String goodsName) {
        Map<String,String> goodsCountMap = new HashMap<>();
        List<MenuVo> menuVoList = mallDao.queryMenuList();
        List<GoodsVo> goodsVoList = mallDao.queryGoodsList(openId,goodsName);
        List<GoodsCountVo> goodsCountVoList = mallDao.count();
        goodsCountVoList.forEach(goodsCountVo -> {
            goodsCountMap.put(goodsCountVo.getMenuId(),goodsCountVo.getTotal());
        });
        goodsVoList.forEach(goodsVo -> {
            if(StringUtil.isNotEmpty(goodsVo.getImgId())){
                goodsVo.setImgUrlList(imageService.getImgUrlList(goodsVo.getImgId()));
            }
        });
        menuVoList.forEach(menuVo -> {
            menuVo.setGoodsList(goodsVoList.stream().filter(goodsVo ->
                    goodsVo.getMenuId().equals(menuVo.getMenuId())).collect(Collectors.toList()));
            String total = "0";
            if(StringUtil.isNotEmpty(goodsCountMap.get(menuVo.getMenuId()))){
                total  = goodsCountMap.get(menuVo.getMenuId());
            }
            menuVo.setTotal(total);
            menuVo.setPageNum("1");
        });
        return Result.resultInstance().success(menuVoList);
    }

    @Override
    public Result queryGoodsList(MallDto dto) {
        int pageStart = (dto.getPageNum() - 1) * SysConstant.page_size;
        int pageEnd = dto.getPageNum()  * SysConstant.page_size;
        List<GoodsVo> goodsVoList = mallDao.queryGoodsListByPageNum(
                dto.getMenuId(), dto.getOpenId(), dto.getGoodsName(), pageStart, pageEnd);
        goodsVoList.forEach(goodsVo -> {
            if(StringUtil.isNotEmpty(goodsVo.getImgId())){
                goodsVo.setImgUrlList(imageService.getImgUrlList(goodsVo.getImgId()));
            }
        });
        return Result.resultInstance().success(goodsVoList);
    }

    @Override
    public Result queryGoodsDetail(String goodsId,String openId) {
        GoodsDetailVo goodsDetailVo = mallDao.queryGoodsDetail(goodsId,openId);
        if(ObjectUtils.isEmpty(goodsDetailVo)){
            return Result.resultInstance().fail(SysCodeEnum.GOODS_OFF_THE_SHELF);
        }
        if("0".equals(goodsDetailVo.getGoodsStatus()) || "1".equals(goodsDetailVo.getIsDel())){
            return Result.resultInstance().fail(SysCodeEnum.GOODS_OFF_THE_SHELF);
        }
        if(StringUtil.isNotEmpty(goodsDetailVo.getImgId())){
            goodsDetailVo.setImgUrlList(imageService.getImgUrlList(goodsDetailVo.getImgId()));
        }
        goodsDetailVo.setImgUrlList(imageService.getImgUrlList(goodsDetailVo.getImgId()));
        return Result.resultInstance().success(goodsDetailVo);
    }

    @Override
    public boolean checkGoodsStock(String goodsId,int goodsNum) {
        boolean flag = false; // false 库存足够 true 库存不足
        GoodsVo goodsVo = mallDao.queryGoodsStock(goodsId);
        // goodsStock = -1 不限制库存
        if("-1".equals(goodsVo.getGoodsStock())){
            return false;
        }
        if(Integer.parseInt(goodsVo.getGoodsStock()) < goodsNum){
            flag = true;
        }
        return flag;
    }

    public void initBannerList(){
        List<ImageVo> list = mallDao.queryBannerList();
        if (CollectionUtils.isNotEmpty(list)) {
            if(redisHandle.exists(SysConstant.REDIS_KEY_BANNER_IMAGE_URL)){
                redisHandle.remove(SysConstant.REDIS_KEY_BANNER_IMAGE_URL);
            }
            redisHandle.set(SysConstant.REDIS_KEY_BANNER_IMAGE_URL,list);
        }
    }
}
