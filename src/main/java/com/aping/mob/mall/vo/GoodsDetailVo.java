package com.aping.mob.mall.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GoodsDetailVo {
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品图片地址，英文逗号隔开")
    @JsonIgnore
    private String imgId;
    private List<String> imgUrlList;
    @ApiModelProperty("商品描述")
    private String goodsMemo;
    @ApiModelProperty("商品详情 富文本")
    private String goodsDetail;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品库存")
    private String goodsStock;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("商品状态 0-下架 1-上架")
    private String goodsStatus;
    private String isDel;
    @ApiModelProperty("商品数量")
    private String goodsNum;
    @ApiModelProperty("标签id")
    private String label;
}
