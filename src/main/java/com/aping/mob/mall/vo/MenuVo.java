package com.aping.mob.mall.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class MenuVo {
    @ApiModelProperty("菜单id")
    private String menuId;
    @ApiModelProperty("菜单名称")
    private String menuName;
    @ApiModelProperty("菜单排序")
    private String menuOrder;
    @ApiModelProperty("商品数量")
    private String total;
    @ApiModelProperty("页码")
    private String pageNum;
    @ApiModelProperty("商品列表")
    private List<GoodsVo> goodsList;
}
