package com.aping.mob.mall.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LabelVo {
    @ApiModelProperty("图片id")
    private String labelId;
    @ApiModelProperty("图片id")
    private String labelName;
}
