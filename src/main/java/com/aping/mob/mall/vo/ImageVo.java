package com.aping.mob.mall.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImageVo {
    @ApiModelProperty("图片id")
    private String imgId;
    @ApiModelProperty("图片url")
    private String imgUrl;
}
