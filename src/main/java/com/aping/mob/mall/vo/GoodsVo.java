package com.aping.mob.mall.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GoodsVo {
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("所属菜单id")
    private String menuId;
    @ApiModelProperty("商品图片地址，英文逗号隔开")
    @JsonIgnore
    private String imgId;
    private List<String> imgUrlList;
    @ApiModelProperty("商品描述")
    private String goodsMemo;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品库存")
    private String goodsStock;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("商品排序")
    private String goodsOrder;
    @ApiModelProperty("商品数量")
    private String goodsNum;
    @ApiModelProperty("标签")
    private String label;
}
