package com.aping.mob.mall.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GoodsCountVo {
    @ApiModelProperty("菜单id")
    private String menuId;
    @ApiModelProperty("商品数量")
    private String total;
}
