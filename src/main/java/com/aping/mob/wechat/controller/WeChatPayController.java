package com.aping.mob.wechat.controller;

import com.aping.common.model.Result;
import com.aping.mob.wechat.service.WeChatPayService;
import com.aping.platform.requestJson.annotation.RequestJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

@RestController
@RequestMapping("/mob/weChat")
@Api(tags = "微信支付")
@Slf4j
public class WeChatPayController {

    @Resource
    private WeChatPayService weChatPayService;


//    @ApiOperation(value = "微信下单")
//    @PostMapping("/wxPayOrder")
//    public Result wxPayOrder(@RequestJson("secret") String secret){
//        return weChatPayService.wxPayOrder(secret);
//    }

//    @ApiOperation(value = "支付成功后回调")
//    @PostMapping("/wxQueryPay")
//    public Result wxQueryPay(@RequestJson("secret") String secret){
//        return weChatPayService.wxQueryPay(secret);
//    }

    @ApiOperation(value = "微信退款")
    @GetMapping("/wxPayRefund")
    public Result wxPayOrder(String orderId, BigDecimal refundMoney, String payKind,
                             String actualMoney,String refundKind){
        return weChatPayService.wxPayRefund(orderId,refundMoney,payKind,actualMoney,refundKind);
    }


}
