package com.aping.mob.wechat.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WxRefundDto {
    @ApiModelProperty("商户退款单号")
    private String outRefundNo;
    @ApiModelProperty("商户订单号")
    private String orderId;
    @ApiModelProperty("微信支付退款单号")
    private String refundMoney;
    @ApiModelProperty("原订单金额")
    private String totalMoney;
    @ApiModelProperty("实际退款金额")
    private String actualMoney;
    @ApiModelProperty("退款状态 1-退款中 2-退款关闭 3-退款异常 4-退款成功")
    private String refundStatus;
    @ApiModelProperty("退款类别 1-移动端取消订单 2-管理端退款")
    private String refundKind;
    @ApiModelProperty("微信支付退款单号")
    private String refundId;
    @ApiModelProperty("微信支付订单号")
    private String transactionId;
    @ApiModelProperty("退款时间")
    private String refundTime;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
