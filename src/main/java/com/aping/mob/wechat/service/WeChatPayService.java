package com.aping.mob.wechat.service;


import cn.hutool.core.util.ObjectUtil;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.properties.WxPayProperties;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.RSAUtil;
import com.aping.mob.flow.dao.AmountDao;
import com.aping.mob.flow.service.AmountService;
import com.aping.mob.flow.vo.AmountVo;
import com.aping.mob.flow.vo.WeChatVo;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.flow.dao.WeChatDao;
import com.aping.mob.flow.dto.WeChatDto;
import com.aping.mob.order.dao.OrderDao;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.OrderVo;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.wechat.dto.WxRefundDto;
import com.aping.mob.wechat.vo.WxRefundVo;
import com.github.binarywang.wxpay.bean.request.WxPayRefundV3Request;
import com.github.binarywang.wxpay.bean.request.WxPayUnifiedOrderV3Request;
import com.github.binarywang.wxpay.bean.result.WxPayRefundV3Result;
import com.github.binarywang.wxpay.bean.result.WxPayUnifiedOrderV3Result;
import com.github.binarywang.wxpay.bean.result.enums.TradeTypeEnum;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class WeChatPayService{

    @Resource
    private OrderDao orderDao;
    @Resource
    private SysParamService sysParamService;
    @Resource
    private WxPayProperties properties;
    @Resource
    private WeChatDao weChatDao;
    @Resource
    private AmountService amountService;
    @Resource
    private WxPayService wxPayService;
    @Resource
    private OrderService orderService;
    @Resource
    private RebateService rebateService;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private AmountDao amountDao;
    @Value("${aping.secret.mob-private-key}")
    private String privateKey;
    @Value("${wx.refund.notify_url}")
    private String wxRefundNotifyUrl;


    private Map<String,String> getPayParams(String prepayId) {
        Map<String,String> param = new HashMap<>();
        try {
            String content = new String(Files.readAllBytes(Paths.get("D:/cert/apiclient_key.pem")), StandardCharsets.UTF_8);
            String privateKey = content.replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replaceAll("\\s+", "");
            long timestamp = System.currentTimeMillis() / 1000;
            String nonceStr = RandomStringUtils.randomAlphabetic(32);
            String appId = properties.getAppId();
            String sb =  appId + "\n"
                    + timestamp + "\n"
                    + nonceStr + "\n"
                    + "prepay_id="+ prepayId + "\n";
            String paySign = RSAUtil.sign(sb.getBytes(),privateKey);
            param.put("appId",appId);
            param.put("timeStamp", String.valueOf(timestamp));
            param.put("nonceStr",nonceStr);
            param.put("package","prepay_id=" + prepayId);
            param.put("signType","RSA");
            param.put("paySign",paySign);
            return param;
        }catch (Exception e){
            e.printStackTrace();
            return param;
        }
    }

    /**
     * JSAPI(V3)下单
     * @param openId 用户
     * @param orderId 订单编号
     * @param orderPrice 订单价格（单位：分）
     * @param description 订单描述
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public Result wxPayOrder(String openId, String orderId,
                             BigDecimal orderPrice, String description) {
        createWxPayFlow(openId,orderId,orderPrice);
        Map<String,String> paramMap;
        orderPrice = orderPrice.multiply(new BigDecimal("100")).setScale(0, RoundingMode.DOWN);
        WxPayUnifiedOrderV3Request v3Request = new WxPayUnifiedOrderV3Request();
        WxPayUnifiedOrderV3Request.Amount amount = new WxPayUnifiedOrderV3Request.Amount();
        amount.setTotal(Integer.parseInt(orderPrice.toString())).setCurrency("CNY");
        WxPayUnifiedOrderV3Request.Payer payer = new WxPayUnifiedOrderV3Request.Payer();
        payer.setOpenid(openId);
        v3Request.setDescription(description)
                .setOutTradeNo(orderId)
                .setAmount(amount)
                .setPayer(payer);
        try {
            WxPayUnifiedOrderV3Result orderV3Result = wxPayService.unifiedOrderV3(TradeTypeEnum.JSAPI, v3Request);
            paramMap = getPayParams(orderV3Result.getPrepayId());
            return Result.resultInstance().success(SysCodeEnum.WECHAT_PAY,paramMap);
        } catch (WxPayException e) {
            e.printStackTrace();
            log.info("JSAPI 下单失败：{}",e.getLocalizedMessage());
            return Result.resultInstance().success(SysCodeEnum.JSAPI_ERROR,e.getMessage());
        }
    }

    /**
     * 生成微信支付流水
     * @param openId 客户唯一标识
     * @param orderId 订单id
     * @param orderPrice 订单价格
     */
    private void createWxPayFlow(String openId,String orderId,BigDecimal orderPrice){
        WeChatDto weChatDto = new WeChatDto();
        weChatDto.setOpenId(openId);
        weChatDto.setOrderId(orderId);
        weChatDto.setWechatAmount(String.valueOf(orderPrice));
        weChatDto.setPayStatus("1");
        weChatDto.setInsertDate(CommonUtil.getSeverDate());
        weChatDto.setInsertTime(CommonUtil.getSeverTime());
        String kind = orderId.substring(0,1);
        switch (kind){
            case "O":
                weChatDto.setPayKind("1");
                break;
            case "R":
                weChatDto.setPayKind("2");
                break;
        }
        weChatDao.addWxPayFlow(weChatDto);
    }

    /**
     * 退款接口（新版V3）
     *
     * @param orderId  商户订单号
     * @param payKind  微信支付种类  1-商品购买订单 2-充值
     * @param refundMoney      退款金额 (单位：分)
     * @param actualMoney      实际退款金额 (单位：元)
     * @return 退款成功或退款处理中：{0:Y，1:商户单号，2:微信单号，3:退款单号，4:订单金额(分)，5:退款金额（分），6:退款时间}<br>
     * 订单异常：{0:N，1:订单状态，2:订单描述}
     * 退款错误：{0:E，1:错误代码，2:错误描述}
     */
    @Transactional(rollbackFor = Exception.class)
    public Result wxPayRefund(String orderId, BigDecimal refundMoney,
                              String payKind,String actualMoney,
                              String refundKind) {
        WxRefundDto wxRefundDto = createWxRefundFlow(orderId,refundMoney,
                payKind,actualMoney,refundKind);
        if(ObjectUtil.isEmpty(wxRefundDto)){
            // 为空说明没有支付成功的微信支付流水
            log.error(">>>>>>[{}]-未查询到有支付成功的微信支付流水<<<<<",orderId);
            return Result.resultInstance().fail(SysCodeEnum.WX_REFUND_ERROR);
        }
        //  订单总金额（单位：分）
        Integer total = Integer.parseInt(
                new BigDecimal(wxRefundDto.getTotalMoney())
                        .multiply(new BigDecimal("100"))
                        .setScale(0, RoundingMode.DOWN).toString());
        // 退款金额（单位：分）
        Integer refund = Integer.parseInt(
                new BigDecimal(wxRefundDto.getRefundMoney())
                        .multiply(new BigDecimal("100"))
                        .setScale(0, RoundingMode.DOWN).toString());
        // 商户退款单号
        String outRefundNo = wxRefundDto.getOutRefundNo();
        // 构造请求参数
        WxPayRefundV3Request wxPayRefundV3Request = new WxPayRefundV3Request();
        WxPayRefundV3Request.Amount amount = new WxPayRefundV3Request.Amount();
        amount.setTotal(total).setRefund(refund).setCurrency("CNY");
        wxPayRefundV3Request
                .setNotifyUrl(wxRefundNotifyUrl)
                .setOutTradeNo(orderId)
                .setOutRefundNo(outRefundNo)
                .setAmount(new WxPayRefundV3Request.Amount()
                        .setTotal(total)
                        .setRefund(refund)
                        .setCurrency("CNY")
                );
        try {
            // 执行请求并返回信息
            WxPayRefundV3Result refundV3Result = wxPayService.refundV3(wxPayRefundV3Request);
            // 退款处理中 || 退款成功
            String refundStatus = refundV3Result.getStatus();
            log.info(">>>>>>[{}]-退款状态为：{}<<<<<<",orderId,refundStatus);
            WxRefundDto dto;
            switch (refundStatus){
                // 退款关闭
                case "CLOSED":
                    log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款关闭");
                    // 更新状态
                    dto = new WxRefundDto();
                    dto.setOrderId(orderId);
                    dto.setRefundStatus("2");
                    weChatDao.updateWxRefundFlow(dto);
                    break;
                // 退款成功
                case "SUCCESS":
                    // 更新退款流水
                    log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款成功");
                    successRefund(refundV3Result);
                    break;
                // 退款异常
                case "ABNORMAL":
                    log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款异常");
                    dto = new WxRefundDto();
                    dto.setOrderId(orderId);
                    dto.setRefundStatus("3");
                    weChatDao.updateWxRefundFlow(dto);
                    break;
                default:
                    log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款中");
                    break;
            }
            return Result.resultInstance().success();
        }catch (WxPayException e){
            e.printStackTrace();
            log.info("微信退款失败：{}",e.getLocalizedMessage());
            return Result.resultInstance().success(SysCodeEnum.WX_REFUND_ERROR,e.getMessage());
        }
    }


    private WxRefundDto  createWxRefundFlow(String orderId,
                                            BigDecimal refundMoney,
                                            String payKind,
                                            String actualMoney,
                                            String refundKind){
        WeChatVo weChatVo = weChatDao.queryByParam(orderId,"2",payKind);
        WxRefundDto refundDto = null;
        if(ObjectUtil.isNotEmpty(weChatVo)){
            refundDto = new WxRefundDto();
            refundDto.setOutRefundNo("WX_REFUND_" + IdUtil.getSpecLengthId(5));
            refundDto.setOrderId(orderId);
            refundDto.setRefundMoney(refundMoney.toString());
            refundDto.setTotalMoney(weChatVo.getWechatAmount());
            refundDto.setActualMoney(actualMoney);
            refundDto.setRefundStatus("1");
            refundDto.setRefundKind(refundKind);
            refundDto.setInsertDate(CommonUtil.getSeverDate());
            refundDto.setInsertTime(CommonUtil.getSeverTime());
            weChatDao.addWxRefundFlow(refundDto);
        }
        return refundDto;
    }

    public void successRefund(WxPayRefundV3Result refundV3Result){
        String orderId = refundV3Result.getOutTradeNo();
        String outRefundNo = refundV3Result.getOutRefundNo();
        WxRefundVo wxRefundVo = weChatDao.queryRefundByRefundNo(outRefundNo);
        String refundM = wxRefundVo.getRefundMoney();
        String refundActualM = wxRefundVo.getActualMoney();
        BigDecimal refund = new BigDecimal(refundActualM).
                subtract(new BigDecimal(refundM));
        OrderVo orderVo = orderDao.queryOrderById(orderId);
        if("2".equals(wxRefundVo.getRefundKind())){
            //  管理端退款 判断实际金额与微信退款金额比较
            if(!refundM.equals(refundActualM)){
                BigDecimal orderPrice = new BigDecimal(orderVo.getOrderPrice());
                BigDecimal refundPrice = new BigDecimal(orderVo.getRefundPrice());
                amountService.amRefund(orderVo,orderPrice,refundPrice,refund);
            }else {
                log.info(">>>>>[{}]-管理端退款此次无余额退款<<<<<",orderId);
            }
        }else if ("1".equals(wxRefundVo.getRefundKind())) {
            // 移动端取消订单
            if(!refundM.equals(refundActualM)) {
                // 如果退款金额不等于实际金额 说明余额还要退
                // 余额将退 actualMoney - refundMoney
                String openId = orderVo.getOpenId();
                // 生成余额退款流水
                amountService.createAmountFlow(openId, orderId, refund, "2");
                // 增加账户余额
                BigDecimal accountBalances = new BigDecimal(userInfoDao.getAmount(openId));
                BigDecimal balance = accountBalances.add(refund);
                userInfoDao.updateUserAmount(openId, String.valueOf(balance));
            }
            rebateService.cancelRebate(orderId);
            // 释放订单
            orderService.releaseOrder(new ArrayList<>(Collections.singleton(orderId)));
            // 处理销量和库存
            orderService.dealCancelVolumeAndStock(orderId);
        }
        String successTime = CommonUtil.getSuccessTime(
                refundV3Result.getSuccessTime());
        WxRefundDto dto = new WxRefundDto();
        dto.setOrderId(orderId);
        dto.setRefundId(refundV3Result.getRefundId());
        dto.setTransactionId(refundV3Result.getTransactionId());
        dto.setRefundTime(successTime);
        dto.setRefundStatus("4");
        weChatDao.updateWxRefundFlow(dto);
    }


//    @Override
//    public Result wxQueryPay(String secret) {
//        WxPayOrderQueryV3Result v3Result = null;
//        try {
//            v3Result = this.wxPayService.queryOrderV3(transactionId, orderNo);
//        } catch (WxPayException e) {
//            log.error("查询微信后台订单失败：{}", e.getMessage());
//            return Result.restResult(6001, e.getMessage(), null);
//        }
//        // 根据业务需要，更新商户平台订单状态
//        if(v3Result.getPayState.equels("SUCCES")){
//            // 业务需求
//        }
//        return Result.resultInstance().success();
//    }
}
