package com.aping.mob.flow.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
public class RechargeDto {
    @ApiModelProperty("充值流水号")
    private String flowId;
    @ApiModelProperty("用户唯一标识")
    @NotBlank(message = "用户唯一标识不能为空")
    private String openId;
    @ApiModelProperty("充值金额")
    @NotBlank(message = "充值金额不能为空")
    private String rechargeAmount;
    @ApiModelProperty("充值状态 1-充值中 2-充值成功")
    private String rechargeStatus;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
