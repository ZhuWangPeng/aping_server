package com.aping.mob.flow.dao;

import com.aping.mob.flow.vo.WeChatVo;
import com.aping.mob.flow.dto.WeChatDto;
import com.aping.mob.wechat.dto.WxRefundDto;
import com.aping.mob.wechat.vo.WxRefundVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WeChatDao {

    void addWxPayFlow(WeChatDto dto);

    void updateWxPayFlow(WeChatDto dto);
    void updateWxPayFlowByOrderId(WeChatDto dto);

    WeChatVo queryByParam(@Param("orderId") String orderId,
                            @Param("payStatus")  String payStatus,
                            @Param("payKind")  String payKind);


    void addWxRefundFlow(WxRefundDto wxRefundDto);

    void updateWxRefundFlow(WxRefundDto wxRefundDto);

    String queryRefundActualMoney(String orderId);

    WxRefundVo queryRefundByRefundNo(String outRefundNo);

}
