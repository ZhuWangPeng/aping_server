package com.aping.mob.flow.dao;

import com.aping.mob.flow.dto.RechargeDto;
import com.aping.mob.flow.vo.RechargeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RechargeDao {

    void addRecharge(RechargeDto dto);

    void updateRecharge(RechargeDto dto);

    RechargeVo queryByFlowId(@Param("flowId") String flowId);

}
