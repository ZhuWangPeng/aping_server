package com.aping.mob.flow.dao;

import com.aping.mob.flow.dto.AmountDto;
import com.aping.mob.flow.vo.AmountVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AmountDao {

    void addAmount(AmountDto dto);

    void updateAmount(AmountDto dto);

    AmountVo queryByParam(@Param("orderId") String orderId,
                            @Param("spentStatus") String spentStatus,
                            @Param("kind") String kind);
}
