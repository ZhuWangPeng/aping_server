package com.aping.mob.flow.controller;

import com.aping.common.model.Result;
import com.aping.mob.flow.dto.RechargeDto;
import com.aping.mob.flow.service.RechargeService;
import com.aping.platform.requestJson.annotation.RequestJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/recharge")
@Api(tags = "充值流水")
@Slf4j
public class RechargeController {
    @Resource
    private RechargeService rechargeService;

    @ApiOperation(value = "新增充值流水")
    @PostMapping("/addFlow")
    public Result addFlow(@RequestBody @Validated RechargeDto rechargeDto){
        return rechargeService.addFlow(rechargeDto);
    }

//    @ApiOperation(value = "修改充值流水")
//    @PostMapping("/updateRecharge")
//    public Result updateRecharge(@RequestJson("secret") String secret){
//        return rechargeService.updateRecharge(secret);
//    }
}
