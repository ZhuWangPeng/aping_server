package com.aping.mob.flow.controller;

import com.aping.common.model.Result;
import com.aping.mob.flow.service.AmountService;
import com.aping.platform.requestJson.annotation.RequestJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/amount")
@Api(tags = "储值流水")
@Slf4j
public class AmountController {

    @Resource
    private AmountService amountService;

    @ApiOperation(value = "修改储值流水")
    @PostMapping("/updateAmount")
    public Result updateAmount(@RequestJson("secret") String secret){
        return amountService.updateAmount(secret);
    }

}
