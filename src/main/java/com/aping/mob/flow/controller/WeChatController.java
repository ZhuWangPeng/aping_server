package com.aping.mob.flow.controller;

import com.aping.common.model.Result;
import com.aping.mob.flow.service.WeChatService;
import com.aping.platform.requestJson.annotation.RequestJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/weChat")
@Api(tags = "微信支付流水")
@Slf4j
public class WeChatController {

    @Resource
    private WeChatService weChatService;

//    @ApiOperation(value = "修改微信支付流水")
//    @PostMapping("/updateWeChat")
//    public Result updateWeChat(@RequestJson("secret") String secret){
//        return weChatService.updateWeChat(secret);
//    }

}
