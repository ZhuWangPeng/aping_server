package com.aping.mob.flow.service;

import com.aping.common.model.Result;
import com.aping.manage.order.vo.ManOrderVo;
import com.aping.mob.flow.dto.AmountDto;
import com.aping.mob.order.vo.OrderVo;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;

public interface AmountService {
    Result updateAmount(String secret);

    void createAmountFlow(String openId, String orderId,
                          BigDecimal money,String kind);

    void amRefund(OrderVo orderVo, BigDecimal orderPrice,
                  BigDecimal refundPrice, BigDecimal refund);
}
