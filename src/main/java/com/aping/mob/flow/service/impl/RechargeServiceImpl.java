package com.aping.mob.flow.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.RSAUtil;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.flow.dao.RechargeDao;
import com.aping.mob.flow.dto.RechargeDto;
import com.aping.mob.flow.dto.WeChatDto;
import com.aping.mob.flow.service.RechargeService;
import com.aping.mob.wechat.service.WeChatPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.ReactiveRedisConnection;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Map;

@Service
@Slf4j
public class RechargeServiceImpl implements RechargeService {

    @Resource
    private RechargeDao rechargeDao;
    @Resource
    private WeChatPayService weChatPayService;
    @Resource
    private SysParamService sysParamService;
    @Value("${aping.secret.mob-private-key}")
    private String privateKey;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addFlow(RechargeDto rechargeDto) {
        rechargeDto.setFlowId("R"+IdUtil.getUUID());
        rechargeDto.setRechargeStatus("1");
        rechargeDto.setInsertDate(CommonUtil.getSeverDate());
        rechargeDto.setInsertTime(CommonUtil.getSeverTime());
        rechargeDao.addRecharge(rechargeDto);
        String openId = rechargeDto.getOpenId();
        String orderId = rechargeDto.getFlowId();
        BigDecimal orderPrice = new BigDecimal(rechargeDto.getRechargeAmount());
        String description = sysParamService.
                getSysParamRedis("WX_RECHARGE_DESCRIPTION");
        return weChatPayService.wxPayOrder(openId,orderId,orderPrice,description);
    }

    @Override
    public Result updateRecharge(String secret) {
        try {
            secret = secret.replace(" ", "+");
            String str = RSAUtil.decryptData(secret, privateKey);
            Map<String, String> map = JSONObject.parseObject(str, Map.class);
            RechargeDto rechargeDto = new RechargeDto();
            rechargeDto.setFlowId(map.get("rechargeFlowId"));
            rechargeDto.setRechargeStatus("2");
            rechargeDao.updateRecharge(rechargeDto);
            return Result.resultInstance().success();
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().success(SysCodeEnum.RECHARGE_UPDATE_ERROR);
        }
    }
}
