package com.aping.mob.flow.service;

import com.aping.common.model.Result;
import com.aping.mob.flow.dto.RechargeDto;
import com.aping.platform.requestJson.annotation.RequestJson;
import org.springframework.web.bind.annotation.RequestBody;

public interface RechargeService {

    Result addFlow(RechargeDto rechargeDto);

    Result updateRecharge(String secret);
}
