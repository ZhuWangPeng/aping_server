package com.aping.mob.flow.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.RSAUtil;
import com.aping.mob.flow.dao.WeChatDao;
import com.aping.mob.flow.dto.WeChatDto;
import com.aping.mob.flow.service.WeChatService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
@Slf4j
public class WeChatServiceImpl implements WeChatService {

    @Resource
    private WeChatDao weChatDao;
    @Value("${aping.secret.mob-private-key}")
    private String privateKey;

    @Override
    public Result updateWeChat(String secret) {
        try {
            secret = secret.replace(" ", "+");
            String str = RSAUtil.decryptData(secret, privateKey);
            Map<String, String> map = JSONObject.parseObject(str, Map.class);
            WeChatDto weChatDto = new WeChatDto();
//            weChatDto.setFlowId(map.get("weChatFlowId"));
            weChatDto.setWechatOrderId(map.get("wechatOrderId"));
            weChatDto.setPayStatus("2");
            weChatDao.updateWxPayFlow(weChatDto);
            return Result.resultInstance().success();
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().success(SysCodeEnum.WECHAT_UPDATE_ERROR);
        }
    }
}
