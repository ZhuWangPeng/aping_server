package com.aping.mob.flow.service;

import com.aping.common.model.Result;
import com.aping.mob.flow.dto.WeChatDto;
import org.springframework.web.bind.annotation.RequestBody;

public interface WeChatService {
    Result updateWeChat(String secret);
}
