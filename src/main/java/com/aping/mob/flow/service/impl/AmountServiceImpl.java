package com.aping.mob.flow.service.impl;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.RSAUtil;
import com.aping.manage.order.vo.ManOrderVo;
import com.aping.mob.flow.dao.AmountDao;
import com.aping.mob.flow.dto.AmountDto;
import com.aping.mob.flow.service.AmountService;
import com.aping.mob.order.dao.OrderDao;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.OrderVo;
import com.aping.mob.user.dao.UserInfoDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

@Service
@Slf4j
public class AmountServiceImpl implements AmountService {

    @Resource
    private AmountDao amountDao;
    @Resource
    private AmountService amountService;
    @Resource
    private OrderDao orderDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private RebateService rebateService;
    @Resource
    private OrderService orderService;
    @Value("${aping.secret.mob-private-key}")
    private String privateKey;

    @Override
    public Result updateAmount(String secret) {
        try {
            secret = secret.replace(" ", "+");
            String flowId = RSAUtil.decryptData(secret, privateKey);
            AmountDto amountDto = new AmountDto();
            amountDto.setFlowId(flowId);
            amountDto.setSpentStatus("2");
            amountDao.updateAmount(amountDto);
            return Result.resultInstance().success();
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().success(SysCodeEnum.AMOUNT_UPDATE_ERROR);
        }
    }

    /**
     * 生成余额支付流水
     * @param openId 用户标识
     * @param orderId 订单编号
     * @param money 金额
     * @param kind 余额支付情况 0-余额支付订单 1-余额提现 2-订单退款
     */
    @Override
    public void createAmountFlow(String openId, String orderId, BigDecimal money,String kind){
        AmountDto amountDto = new AmountDto();
        amountDto.setFlowId(IdUtil.getUUID());
        amountDto.setOpenId(openId);
        amountDto.setOrderId(orderId);
        amountDto.setSpentAmount(money.toString());
        amountDto.setSpentStatus("2");
        amountDto.setKind(kind);
        amountDto.setInsertDate(CommonUtil.getSeverDate());
        amountDto.setInsertTime(CommonUtil.getSeverTime());
        amountDao.addAmount(amountDto);
    }

    /**
     *
     * @param orderVo 订单对象
     * @param orderPrice 订单金额
     * @param refundPrice 订单已退金额
     * @param refund 要退款金额(BigDecimal)
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void amRefund(OrderVo orderVo, BigDecimal orderPrice,
                         BigDecimal refundPrice, BigDecimal refund){
        String refundMoney = String.valueOf(refund);
        // 用户唯一标识
        String openId = orderVo.getOpenId();
        // 订单编号
        String orderId = orderVo.getOrderId();
        // 余额支付金额
        amountService.createAmountFlow(openId, orderId, refund, "2");
        // 将已退金额加上要退款的金额
        refundPrice = refundPrice.add(refund);
        // 增加账户余额
        BigDecimal accountBalances = new BigDecimal(userInfoDao.getAmount(openId));
        BigDecimal balance = accountBalances.add(refund);
        userInfoDao.updateUserAmount(openId, String.valueOf(balance));
        if (orderPrice.compareTo(refundPrice) == 0) {
            // 更新订单状态为全部退款
            orderDao.updateOrderStatus(orderId, "8",
                    String.valueOf(refundPrice),null);
            // 删除返利 余额全部退款
            rebateService.cancelRebate(orderId);
        } else {
            // 更新订单为部分退款 // todo 时间是否更新
            String deliverTime = orderDao.queryLstDeliver(orderId);
            orderDao.updateOrderStatus(orderId, "7",
                    String.valueOf(refundPrice),CommonUtil.getConfirmReceiptTime(deliverTime));
            rebateService.updateRebate(orderId, orderVo.getOrderPrice(), refundMoney);
        }
    }
}
