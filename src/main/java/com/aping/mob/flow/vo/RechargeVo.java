package com.aping.mob.flow.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RechargeVo {
    @ApiModelProperty("充值流水号")
    private String flowId;
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("充值金额")
    private String rechargeAmount;
    @ApiModelProperty("充值状态 1-充值中 2-充值成功")
    private String rechargeStatus;
//    @ApiModelProperty("微信支付订单")
//    private String wechatOrderId;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
