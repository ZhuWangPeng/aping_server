package com.aping.mob.flow.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AmountVo {
    @ApiModelProperty("储值流水号")
    private String flowId;
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("消费金额")
    private String spentAmount;
    @ApiModelProperty("消费状态 1-消费中 2-消费成功")
    private String spentStatus;
    @ApiModelProperty("余额支付情况 0-余额支付订单 1-余额提现 2-订单退款")
    private String kind;
}
