package com.aping.mob.user.controller;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.MsgDto;
import com.aping.mob.user.service.MsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/msg")
@Api(tags = "留言")
@Slf4j
public class MsgController {

    @Resource
    private MsgService msgService;

    @ApiOperation(value = "新增或修改留言")
    @PostMapping("/add")
    public Result addMsg(@RequestBody MsgDto dto){
        return msgService.addMsg(dto);
    }


}
