package com.aping.mob.user.controller;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.UserAmountDto;
import com.aping.mob.user.service.MobUserService;
import com.aping.platform.requestJson.annotation.RequestJson;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/user")
@Api(tags = "用户")
@Slf4j
public class MobUserController {

    @Resource
    private MobUserService mobUserService;

    @ApiOperation(value = "支付")
    @PostMapping("/pay")
    public Result pay(@RequestJson("secret") String secret){
        return mobUserService.pay(secret);
    }

    @ApiOperation(value = "获取账户余额")
    @PostMapping("/getAmount")
    public Result getAmount(@RequestJson("openId") String openId){
        return mobUserService.getAmount(openId);
    }

    @ApiOperation(value = "余额明细")
    @PostMapping("/getAmountFlowList")
    public Result getAmountFlowList(@RequestBody @Validated UserAmountDto dto){
        return mobUserService.getAmountFlowList(dto);
    }

}


