package com.aping.mob.user.controller;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.AddressDto;
import com.aping.mob.user.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/address")
@Api(tags = "收货地址")
@Slf4j
public class AddressController {
    @Resource
    private AddressService service;

    @ApiOperation(value = "地址列表")
    @PostMapping("/query")
    public Result query(@RequestBody AddressDto dto){
        return service.query(dto.getOpenId());
    }


    @ApiOperation(value = "新增")
    @PostMapping("/add")
    public Result add(@RequestBody AddressDto dto){
        return service.add(dto);
    }


    @ApiOperation(value = "修改地址")
    @PostMapping("/update")
    public Result update(@RequestBody AddressDto dto){
        return service.update(dto);
    }


    @ApiOperation(value = "删除地址")
    @PostMapping("/del")
    public Result del(@RequestBody AddressDto dto){
        return service.del(dto.getAddressId());
    }

}
