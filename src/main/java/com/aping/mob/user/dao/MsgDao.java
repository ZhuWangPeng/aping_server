package com.aping.mob.user.dao;

import com.aping.mob.user.dto.MsgDto;
import com.aping.mob.user.vo.MsgVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface MsgDao {
    MsgVo query(@Param("openId") String openId);

    void addMsg(MsgDto dto);
}
