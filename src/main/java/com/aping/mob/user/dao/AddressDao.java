package com.aping.mob.user.dao;

import com.aping.mob.user.dto.AddressDto;
import com.aping.mob.user.vo.AddressVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface AddressDao {
    List<AddressVo> query(@Param("openId") String openId);
    void addAddress(AddressDto dto);
    void updateAddress(AddressDto dto);
    void delAddress(@Param("addressId") String addressId);

    void updateDefault();

    AddressVo queryById(@Param("addressId") String addressId);
}
