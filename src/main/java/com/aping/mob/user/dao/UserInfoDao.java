package com.aping.mob.user.dao;

import com.aping.mob.user.dto.UserAmountDto;
import com.aping.mob.user.dto.UserInfoDto;
import com.aping.mob.user.vo.AmountDetailVo;
import com.aping.mob.user.vo.UserInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserInfoDao {

    void insertUserInfo(UserInfoDto userInfoDto);
    void updateUserInfo(UserInfoDto userInfoDto);

    UserInfoVo queryByOpenId(String openId);

    void updateUserAmount(@Param("openId") String openId,@Param("accountBalances") String accountBalances);

    String getAmount(String openId);

    List<AmountDetailVo> getAmountFlowList(@Param("openId") String openId);

    String queryInviterOpenId(String openId);

    int batchUpdateUserAmount(@Param("list") List<UserInfoDto> userInfoDtoList);
}
