package com.aping.mob.user.service;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.UserAmountDto;
import com.github.pagehelper.Page;

public interface MobUserService {
    Result pay(String secret);

    Result getAmount(String openId);

    Result getAmountFlowList(UserAmountDto dto);

}
