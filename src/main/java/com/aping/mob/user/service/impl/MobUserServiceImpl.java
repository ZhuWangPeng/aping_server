package com.aping.mob.user.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisLockHelper;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.RSAUtil;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.flow.dao.AmountDao;
import com.aping.mob.flow.dao.WeChatDao;
import com.aping.mob.flow.service.AmountService;
import com.aping.mob.order.dao.OrderDao;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.OrderVo;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.user.dto.UserAmountDto;
import com.aping.mob.user.service.MobUserService;
import com.aping.mob.user.vo.AmountDetailVo;
import com.aping.mob.wechat.service.WeChatPayService;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class MobUserServiceImpl implements MobUserService {

    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private OrderDao orderDao;
    @Resource
    private RedisLockHelper redisLockHelper;
    @Resource
    private AmountDao amountDao;
    @Resource
    private WeChatDao weChatDao;
    @Resource
    private WeChatPayService weChatPayService;
    @Resource
    private SysParamService sysParamService;
    @Resource
    private RebateService rebateService;
    @Resource
    private AmountService amountService;
    @Resource
    private OrderService orderService;
    @Value("${aping.secret.mob-private-key}")
    private String privateKey;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result pay(String secret) {
        Long time = System.currentTimeMillis() + 30 * 1000;
        // 解密
        secret = secret.replace(" ", "+");
        String str = RSAUtil.decryptData(secret, privateKey);
        Map<String,String> resultMap = JSONObject.parseObject(str,Map.class);
        String orderId = resultMap.get("orderId");
        // 0-不从余额扣除 1-从余额扣除
        String deductStatus = resultMap.get("deductStatus");
        String description = sysParamService.
                getSysParamRedis("WX_ORDER_DESCRIPTION");
        try {
            if (redisLockHelper.lock("APING_GET_PAY_" + orderId, String.valueOf(time))) {
                log.info(">>>>>>支付锁成功<<<<<<");
                OrderVo orderVo = orderDao.queryOrderById(orderId);
                if(ObjectUtils.isEmpty(orderVo)){
                    return Result.resultInstance().fail(SysCodeEnum.NO_QUERY_ORDER);
                }
                String openId = orderVo.getOpenId();
                // 订单价格
                BigDecimal orderPrice = new BigDecimal(orderVo.getOrderPrice());
                // 账户余额
                BigDecimal accountBalances = new BigDecimal(userInfoDao.getAmount(openId));
                // 从余额中扣除
                if("1".equals(deductStatus) && accountBalances.compareTo(new BigDecimal("0")) > 0){
                    // 余额足够
                    if (accountBalances.compareTo(orderPrice) >= 0) {
                        // 从余额中扣 生成一条扣除储值流水并返回支付成功
                        amountService.createAmountFlow(openId,orderId,orderPrice,"0");
                        // 更新余额 余额扣除订单价格
                        BigDecimal rest = accountBalances.subtract(orderPrice);
                        userInfoDao.updateUserAmount(openId, String.valueOf(rest));
                        // 更新订单余额支付信息
                        orderDao.updateOrderAmPayInfo(orderId,CommonUtil.getSeverTime(),String.valueOf(orderPrice));
                        // 生成返利流水
                        rebateService.createRebate(openId,orderId, String.valueOf(orderPrice));
                        orderService.dealSuccessVolumeAndStock(orderId);
                        return Result.resultInstance().success(SysCodeEnum.AMOUNT_PAY_SUCCESS);
                    } else {
                        // 余额不足 组合支付
                        // 生成一条支付中的微信支付流水 一条消费中的储值流水
                        // 由微信支付的钱
                        BigDecimal wechatAmount = orderPrice.subtract(accountBalances);
                        // 微信支付剩余的价格
                        return weChatPayService.wxPayOrder(openId,orderId,wechatAmount,description);
                    }
                } else {
                    // 余额为0 生成一条支付中的微信支付流水
                    return weChatPayService.wxPayOrder(openId,orderId,orderPrice,description);
                }
            }else {
                log.info(">>>>>>获取支付锁失败<<<<<<");
                return Result.resultInstance().success(SysCodeEnum.SYS_BUSY);
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
            return Result.resultInstance().success(SysCodeEnum.SYS_ERROR);
        }finally {
            redisLockHelper.unlock("APING_GET_PAY_" + orderId, String.valueOf(time));
            log.info(">>>>>>支付锁释放成功<<<<<<");
        }
    }

    @Override
    public Result getAmount(String openId) {
        String accountBalances = userInfoDao.getAmount(openId);
        return Result.resultInstance().success(SysCodeEnum.SUCCESS,accountBalances);
    }


    @Override
    public Result getAmountFlowList(UserAmountDto dto) {
//        if(dto.getPageNum() == 0){
//            dto.setPageNum(1);
//        }
//        dto.setPageStart((dto.getPageNum() - 1) * SysConstant.page_size);
//        dto.setPageSpace(SysConstant.page_size);
        if(dto.getPageSize() == 0){
            dto.setPageSize(SysConstant.page_size);
        }
        PageHelper.startPage(dto.getPageNum(),dto.getPageSize());
        List<AmountDetailVo> amountDetailVoList = userInfoDao.getAmountFlowList(dto.getOpenId());
        return Result.resultInstance().success(new PageInfo<>(amountDetailVoList));
    }
}
