package com.aping.mob.user.service.impl;

import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.mob.user.dao.AddressDao;
import com.aping.mob.user.dto.AddressDto;
import com.aping.mob.user.service.AddressService;
import com.aping.mob.user.vo.AddressVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class AddressServiceImpl implements AddressService {

    @Resource
    private AddressDao dao;

    @Override
    public Result query(String openId) {
        List<AddressVo> list = dao.query(openId);
        return Result.resultInstance().success(list);
    }

    @Override
    public Result add(AddressDto dto) {
        dto.setAddressId(IdUtil.getUUID());
        dto.setInsertTime(CommonUtil.getSeverTime());
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        if("1".equals(dto.getIsDefault())){
            dao.updateDefault();
        }
        dao.addAddress(dto);
        return Result.resultInstance().success();
    }

    @Override
    public Result update(AddressDto dto) {
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        if("1".equals(dto.getIsDefault())){
            dao.updateDefault();
        }
        dao.updateAddress(dto);
        return Result.resultInstance().success();
    }

    @Override
    public Result del(String addressId) {
        dao.delAddress(addressId);
        return Result.resultInstance().success();
    }
}
