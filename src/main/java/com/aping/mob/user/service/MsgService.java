package com.aping.mob.user.service;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.MsgDto;

public interface MsgService {
    Result queryMsg(String openId);

    Result addMsg(MsgDto dto);
}
