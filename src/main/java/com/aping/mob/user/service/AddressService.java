package com.aping.mob.user.service;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.AddressDto;

public interface AddressService {
    Result query(String openId);
    Result add(AddressDto dto);
    Result update(AddressDto dto);
    Result del(String flowId);
}
