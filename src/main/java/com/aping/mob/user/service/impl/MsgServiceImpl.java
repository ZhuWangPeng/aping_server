package com.aping.mob.user.service.impl;

import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.mob.user.dao.MsgDao;
import com.aping.mob.user.dto.MsgDto;
import com.aping.mob.user.service.MsgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class MsgServiceImpl implements MsgService {

    @Resource
    private MsgDao msgDao;

    @Override
    public Result queryMsg(String openId) {
        return null;
    }

    @Override
    public Result addMsg(MsgDto dto) {
        dto.setFlowId(IdUtil.getUUID());
        dto.setInsertDate(CommonUtil.getSeverDate());
        dto.setInsertTime(CommonUtil.getSeverTime());
        msgDao.addMsg(dto);
        return Result.resultInstance().success();
    }
}
