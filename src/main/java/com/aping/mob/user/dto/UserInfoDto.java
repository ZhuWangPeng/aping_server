package com.aping.mob.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserInfoDto {
    @ApiModelProperty("用户唯一标识")
    private String openId;
    private String code;
    @ApiModelProperty("用户微信名")
    private String wechatName;
    @ApiModelProperty("邀请人唯一标识")
    private String inviterOpenId;
    @ApiModelProperty("账户余额")
    private String accountBalances;
    @ApiModelProperty("是否已加入黑名单 0-否 1-是")
    private String isBack;
    @ApiModelProperty("用户首次登录日期")
    private String firstLoginDate;
    @ApiModelProperty("用户首次登录时间")
    private String firstLoginTime;
    @ApiModelProperty("用户最后一次登录日期")
    private String lstLoginDate;
    @ApiModelProperty("用户最后一次登录时间")
    private String lstLoginTime;
}
