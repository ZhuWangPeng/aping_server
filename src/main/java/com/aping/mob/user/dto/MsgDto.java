package com.aping.mob.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MsgDto {
    @ApiModelProperty("流水号")
    private String flowId;
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("留言信息")
    private String message;
    @ApiModelProperty("0-留言 1-发货提醒")
    private String kind;
    @ApiModelProperty("发布日期")
    private String insertDate;
    @ApiModelProperty("发布时间")
    private String insertTime;
}
