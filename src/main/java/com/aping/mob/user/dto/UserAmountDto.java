package com.aping.mob.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserAmountDto {
    @ApiModelProperty("用户标识")
    @NotBlank(message = "openId不可为空")
    private String openId;
    private int pageNum;
    private int pageSize;
}
