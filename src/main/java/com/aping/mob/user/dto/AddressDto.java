package com.aping.mob.user.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class AddressDto {
    @ApiModelProperty("流水号")
    private String addressId;
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("地区id 逗号隔开")
    private String areaId;
    @ApiModelProperty("收货人姓名")
    private String consigneeName;
    @ApiModelProperty("收货人联系方式")
    private String consigneeTel;
    @ApiModelProperty("所在地区")
    private String consigneeArea;
    @ApiModelProperty("详细地址")
    private String consigneeAddress;
    @ApiModelProperty("是否为默认地址")
    private String isDefault;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("最后一次更新时间")
    private String lstUpdateTime;
    @ApiModelProperty("是否删除")
    private String isDel;
}
