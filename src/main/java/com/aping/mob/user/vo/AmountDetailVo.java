package com.aping.mob.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AmountDetailVo {
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("金额")
    private String money;
    @ApiModelProperty("状态 1-增加 2-减少")
    private String amountStatus;
    @ApiModelProperty("1-返利 2-充值 3-余额支付订单 4-余额提现 5-余额退款")
    private String kind;
    @ApiModelProperty("时间")
    private String insertTime;
}
