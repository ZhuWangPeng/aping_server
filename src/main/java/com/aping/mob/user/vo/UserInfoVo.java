package com.aping.mob.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserInfoVo {
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("用户微信名")
    private String wechatName;
    @ApiModelProperty("用户微信id")
    private String wechatId;
    @ApiModelProperty("邀请人唯一标识")
    private String inviterOpenId;
    @ApiModelProperty("账户余额")
    private String accountBalances;
    @ApiModelProperty("是否已加入黑名单 0-否 1-是")
    private String isBack;
    @ApiModelProperty("用户首次登录日期")
    private String firstLoginDate;
    @ApiModelProperty("用户首次登录时间")
    private String firstLoginTime;
    @ApiModelProperty("用户最近一次登录日期")
    private String lstLoginDate;
    @ApiModelProperty("用户最近一次登录时间")
    private String lstLoginTime;
}
