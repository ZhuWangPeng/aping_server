package com.aping.mob.user.vo;

import lombok.Data;

@Data
public class MsgVo {
    private String openId;
    private String message;
}
