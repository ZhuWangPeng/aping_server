package com.aping.mob.qr.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.properties.AccessTokenProperties;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.StringUtil;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.qr.dao.QrDao;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.*;

@Service
@Slf4j
public class QrService {

    @Resource
    private RestTemplate restTemplate;
    @Resource
    private RedisHandle redisHandle;
    @Resource
    private SysParamService sysParamService;
    @Resource
    private AccessTokenProperties properties;
    @Resource
    private QrDao qrDao;
    private final static long ACCESS_TOKEN_EXPIRE_TIME = 2 * 3600L;

    public Result generateCode(String openId){
        String accessToken = getAccessToken();
        if(StringUtil.isEmpty(accessToken)){
            return Result.resultInstance().success(SysCodeEnum.ACCESS_TOKEN_ERROR);
        }
        String url = sysParamService.getSysParamRedis("WX_CODE_URl");
        url = url + "?access_token="+accessToken;
        Map<String,Object> param = new HashMap<>();
        param.put("scene", openId);
        param.put("page","pages/index/index");
        param.put("check_path",false);
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        HttpEntity<Map<String,Object>> requestEntity = new HttpEntity<>(param, headers);
        ResponseEntity<byte[]> entity = restTemplate.postForEntity(url, requestEntity, byte[].class);
        if(ObjectUtil.isEmpty(entity.getBody()) && Objects.requireNonNull(entity.getBody()).length > 0){
            return Result.resultInstance().success(SysCodeEnum.GENERATE_CODE_ERROR);
        }else {
            byte[] result = entity.getBody();
            assert result != null;
            return Result.resultInstance().success((Object) Base64.toBase64String(result));
        }
    }

    public Result getBottomImg(){
        List<String> list = qrDao.queryBottomImg();
        if(!list.isEmpty()){
            int randomInt = RandomUtil.randomInt(0,list.size());
            return Result.resultInstance().success((Object) list.get(randomInt));
        }else {
            return Result.resultInstance().success(SysCodeEnum.BOTTOM_ERROR);
        }
    }

    private String getAccessToken(){
        if(!redisHandle.exists("access_token")){
            String urlAccessToken = sysParamService.getSysParamRedis("WX_GET_ACCESS_TOKEN_URL");
            urlAccessToken = urlAccessToken + "?grant_type={grantType}&appid={appId}&secret={appSecret}";
            Map<String,String> params = new HashMap<String,String>(){{
                put("grantType",properties.getGrantType());
                put("appId",properties.getAppId());
                put("appSecret",properties.getAppSecret());
            }};
            Map map = restTemplate.getForObject(urlAccessToken, Map.class, params);
            assert map != null;
            String accessToken = (String) map.get("access_token");
            if(StringUtil.isNotEmpty(accessToken)){
                redisHandle.set("access_token",accessToken,ACCESS_TOKEN_EXPIRE_TIME);
                return accessToken;
            }else {
                log.info(">>>>>>获取accessToken失败:{}<<<<<<", map);
                return null;
            }
        }else {
            return (String) redisHandle.get("access_token");
        }
    }

}
