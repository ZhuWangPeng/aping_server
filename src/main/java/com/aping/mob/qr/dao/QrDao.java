package com.aping.mob.qr.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface QrDao {
    List<String> queryBottomImg();
}
