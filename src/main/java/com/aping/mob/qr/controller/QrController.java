package com.aping.mob.qr.controller;

import com.aping.common.model.Result;
import com.aping.mob.qr.service.QrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/mob/qr")
@Api(tags = "二维码")
@Slf4j
public class QrController {

    @Resource
    private QrService qrService;

    @ApiOperation(value = "生成小程序码")
    @GetMapping("/generateCode")
    public Result generateCode(@RequestParam("openId") String openId){
        return qrService.generateCode(openId);
    }

    @ApiOperation(value = "获取底部图片")
    @GetMapping("/getBottomImg")
    public Result getBottomImg(){
        return qrService.getBottomImg();
    }


}
