package com.aping.test.service;

import com.alibaba.fastjson.JSONObject;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class TestService {

    @Resource
    private RedisHandle redisHandle;

    /**
     * 清除缓存
     * @param key key
     * @return Result
     */
    public Result removeRedis(String key){
        redisHandle.remove(key);
        return Result.resultInstance().success();
    }

    /**
     *
     * @param key key
     * @param value 值
     * @return Result
     */
    public Result addRedis(String key,Object value){
        redisHandle.set(key,value);
        return Result.resultInstance().success();
    }

    /**
     *
     * @param key key
     * @param value 值
     * @return Result
     */
    public Result addMapRedis(String key, String field, Object value){
        redisHandle.addMap(key, field, value);
        return Result.resultInstance().success();
    }

    /**
     *
     * @param key key
     * @param value 值
     * @return Result
     */
    public Result getRedis(String key){
        String str = (String) redisHandle.get(key);
        return Result.resultInstance().success(str);
    }

    /**
     *
     * @param key key
     * @return Result
     */
    public Result getMapRedis(String key, String field){
        String str = redisHandle.getMapField(key, field);
        return Result.resultInstance().success(str);
    }

}
