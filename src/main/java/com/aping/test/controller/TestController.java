package com.aping.test.controller;

import com.aping.common.model.Result;
import com.aping.test.service.TestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/test")
@Api(tags = "测试接口")
@Slf4j
public class TestController {

    @Resource
    private TestService testService;

    @ApiOperation(value = "移除缓存")
    @GetMapping("/removeRedis")
    public Result removeRedis(@RequestParam("key") String key){
        return testService.removeRedis(key);
    }

    @ApiOperation(value = "增加缓存")
    @GetMapping("/addRedis")
    public Result addRedis(@RequestParam("key") String key,@RequestParam("value") Object value){
        return testService.addRedis(key,value);
    }

    @ApiOperation(value = "增加Map缓存")
    @GetMapping("/addMapRedis")
    public Result addMapRedis(@RequestParam("key") String key,
                              @RequestParam("field") String field,
                              @RequestParam("value") Object value){
        return testService.addMapRedis(key,field,value);
    }

    @ApiOperation(value = "增加Map缓存")
    @GetMapping("/getRedis")
    public Result getRedis(@RequestParam("key") String key){
        return testService.getRedis(key);
    }

    @ApiOperation(value = "增加Map缓存")
    @GetMapping("/getMapRedis")
    public Result getMapRedis(@RequestParam("key") String key,
                              @RequestParam("field") String field){
        return testService.getMapRedis(key,field);
    }

}
