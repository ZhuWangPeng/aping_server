package com.aping.manage.secret.service;

import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.DESUtil;
import com.aping.common.utils.RSAUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class SecretService {

    @Value("${aping.secret.mob-public-key}")
    private String rsaMobPublicKey;
    @Value("${aping.secret.man-public-key}")
    private String rsaManPublicKey;
    @Value("${aping.secret.des-user-token}")
    private String desMobPublicKey;
    @Value("${aping.secret.des-admin-token}")
    private String desManPublicKey;

    /**
     * RSA移动端公钥加密Map
     * @param map 加密数据
     * @return 加密后的字符串
     */
    public Result rsaMobEncryptMap(Map<String,String> map){
        String str = JSONObject.toJSONString(map);
        String publicData = RSAUtil.encryptedData(str,rsaMobPublicKey);
        return Result.resultInstance().success(SysCodeEnum.SUCCESS,publicData);
    }

    /**
     * RSA移动端公钥加密String
     * @param str 加密数据
     * @return 加密后的字符串
     */
    public Result rsaMobEncryptStr(String str){
        String publicData = RSAUtil.encryptedData(str,rsaMobPublicKey);
        return Result.resultInstance().success(publicData);
    }

    /**
     * RSA管理端公钥加密Map
     * @param map 加密数据
     * @return 加密后的字符串
     */
    public Result rsaManEncryptMap(Map<String,String> map){
        String str = JSONObject.toJSONString(map);
        String publicData = RSAUtil.encryptedData(str,rsaManPublicKey);
        return Result.resultInstance().success(SysCodeEnum.SUCCESS,publicData);
    }

    /**
     * RSA管理端公钥加密String
     * @param str 加密数据
     * @return 加密后的字符串
     */
    public Result rsaManEncryptStr(String str){
        String publicData = RSAUtil.encryptedData(str,rsaManPublicKey);
        return Result.resultInstance().success(SysCodeEnum.SUCCESS,publicData);
    }

    /**
     * DES管理端秘钥加密Map
     * @param map 加密数据
     * @return 加密后的字符串
     */
    public Result desMobEncryptMap(Map<String,String> map){
        String str = JSONObject.toJSONString(map);
        String publicData = DESUtil.encrypt3DESToHex(str,desMobPublicKey);
        return Result.resultInstance().success(SysCodeEnum.SUCCESS,publicData);
    }

    /**
     * DES移动端秘钥加密String
     * @param str 加密数据
     * @return 加密后的字符串
     */
    public Result desMobEncryptStr(String str){
        String publicData = DESUtil.encrypt3DESToHex(str,desMobPublicKey);
        return Result.resultInstance().success(SysCodeEnum.SUCCESS,publicData);
    }

    /**
     * DES管理端秘钥加密Map
     * @param map 加密数据
     * @return 加密后的字符串
     */
    public Result desManEncryptMap(Map<String,String> map){
        String str = JSONObject.toJSONString(map);
        String publicData = DESUtil.encrypt3DESToHex(str,desManPublicKey);
        return Result.resultInstance().success(publicData);
    }

    /**
     * DES管理端秘钥加密String
     * @param str 加密数据
     * @return 加密后的字符串
     */
    public Result desManEncryptStr(String str){
        String publicData = DESUtil.encrypt3DESToHex(str,desManPublicKey);
        return Result.resultInstance().success(publicData);
    }



}
