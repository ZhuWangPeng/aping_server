package com.aping.manage.secret.controller;

import com.aping.common.model.Result;
import com.aping.manage.secret.service.SecretService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping("/secret")
@Api(tags = "秘钥")
@Slf4j
public class SecretController {

    @Resource
    private SecretService service;

    @ApiOperation(value = "RSA移动端公钥加密Map")
    @PostMapping("/rsaMobEncryptMap")
    public Result rsaMobEncryptMap(@RequestBody Map<String,String> map){
        return service.rsaMobEncryptMap(map);
    }

    @ApiOperation(value = "RSA移动端公钥加密String")
    @GetMapping("/rsaMobEncryptStr")
    public Result rsaMobEncryptStr(@RequestParam String str){
        return service.rsaMobEncryptStr(str);
    }

    @ApiOperation(value = "RSA管理端公钥加密Map")
    @PostMapping("/rsaManEncryptMap")
    public Result rsaManEncryptMap(@RequestBody Map<String,String> map){
        return service.rsaManEncryptMap(map);
    }

    @ApiOperation(value = "RSA管理端公钥加密String")
    @GetMapping("/rsaManEncryptStr")
    public Result rsaManEncryptStr(@RequestParam String str){
        return service.rsaManEncryptStr(str);
    }

//    @ApiOperation(value = "DES移动端端秘钥加密Map")
//    @GetMapping("/rsaMobEncryptMap")
//    public Result desMobEncryptMap(@RequestBody Map<String,String> map){
//        return service.desMobEncryptMap(map);
//    }
//
//
//    @ApiOperation(value = "DES移动端端秘钥加密String")
//    @GetMapping("/desMobEncryptStr")
//    public Result desMobEncryptStr(@RequestParam String str){
//        return service.desMobEncryptStr(str);
//    }
//
//    @ApiOperation(value = "DES管理端秘钥加密Map")
//    @GetMapping("/rsaMobEncryptMap")
//    public Result desManEncryptMap(@RequestBody Map<String,String> map){
//        return service.desManEncryptMap(map);
//    }
//
//
//    @ApiOperation(value = "DES管理端秘钥加密String")
//    @GetMapping("/desManEncryptStr")
//    public Result desManEncryptStr(@RequestParam String str){
//        return service.desManEncryptStr(str);
//    }
}
