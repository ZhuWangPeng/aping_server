package com.aping.manage.index.service;

import com.aping.common.model.Result;
import com.aping.manage.index.dto.StatDto;

public interface IndexService {
    Result getStatData(StatDto statDto);
}
