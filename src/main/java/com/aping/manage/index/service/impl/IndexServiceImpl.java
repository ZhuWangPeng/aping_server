package com.aping.manage.index.service.impl;

import cn.hutool.core.date.DateUtil;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.StringUtil;
import com.aping.manage.index.dao.IndexDao;
import com.aping.manage.index.dto.StatDto;
import com.aping.manage.index.service.IndexService;
import com.aping.manage.index.vo.StatVo;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class IndexServiceImpl implements IndexService {

    @Resource
    private IndexDao indexDao;
    @Resource
    private RedisHandle redisHandle;

    @Override
    public Result getStatData(StatDto statDto) {
        String currDate = CommonUtil.getSeverDate();
        String date;
        String lstWeekDay = DateUtil.format(
                DateUtil.offsetDay(new Date(),-6),"yyyy-MM-dd");
        if(StringUtil.isNotEmpty(statDto.getStartDate())){
            lstWeekDay = statDto.getStartDate();
        }
        if(StringUtil.isNotEmpty(statDto.getStartDate())){
            date = statDto.getStartDate();
        }else {
            date = currDate;
        }
        List<StatVo> statVoList = indexDao.queryList(date,lstWeekDay);
        StatVo totalVo = indexDao.queryTotal();
        String redisKey = SysConstant.APING_TODAY_VISIT_NUM;
        StatVo todayVo = new StatVo();
        for (StatVo statVo:statVoList) {
            if(statVo.getDataDate().equals(currDate)){
                todayVo = statVo;
                if(redisHandle.hasMapKey(redisKey, currDate)){
                    statVo.setTodayVisitNum(redisHandle.getMapField(redisKey, currDate));
                }else {
                    statVo.setTodayVisitNum("0");
                }
            }
        }
        if(StringUtil.isEmpty(todayVo.getTodayVisitNum())){
            todayVo.setTodayVisitNum("0");
        }
        String totalVisit = String.valueOf(Integer.parseInt(
                todayVo.getTodayVisitNum()) + Integer.parseInt(totalVo.getTodayVisitNum()));
        totalVo.setTodayVisitNum(totalVisit);
        totalVo.setTodayAddUserNum("0");
        totalVo.setDataDate("全部总量");
        Map<String,Object> map = new HashMap<>();
        map.put("totalVisit",totalVo);
        map.put("todayVisit",todayVo);
        map.put("list",statVoList);
        return Result.resultInstance().success(map);
    }
}
