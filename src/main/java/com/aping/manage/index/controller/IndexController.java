package com.aping.manage.index.controller;

import com.aping.common.model.Result;
import com.aping.manage.index.dto.StatDto;
import com.aping.manage.index.service.IndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/index")
@Api(tags = "首页")
@Slf4j
public class IndexController {

    @Resource
    private IndexService indexService;

    @ApiOperation(value = "首页获取当天统计数据")
    @GetMapping("/getStatData")
    public Result getStatData(StatDto statDto){
        return indexService.getStatData(statDto);
    }
}
