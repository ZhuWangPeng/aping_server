package com.aping.manage.index.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.annotations.Mapper;

@Data
public class StatDto {
    @ApiModelProperty("数据日期")
    private String dataDate;
    @ApiModelProperty("用户总数")
    private String userNum;
    @ApiModelProperty("当日新增用户")
    private String todayAddUserNum;
    @ApiModelProperty("当日访问量 统计getUserToken接口")
    private String todayVisitNum;
    @ApiModelProperty("当日销量")
    private String todaySalesVolume;
    @ApiModelProperty("当日销售额")
    private String todaySalesValue;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("起始日期")
    private String startDate;
    @ApiModelProperty("结束日期")
    private String endDate;
}
