package com.aping.manage.index.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StatVo {
    @ApiModelProperty("数据日期")
    private String dataDate;
    @ApiModelProperty("用户总数")
    private String userNum;
    @ApiModelProperty("当日新增用户")
    private String todayAddUserNum;
    @ApiModelProperty("当日访问量 统计getUserToken接口")
    private String todayVisitNum;
    @ApiModelProperty("当日销售量")
    private String todaySalesVolume;
    @ApiModelProperty("当日销售额")
    private String todaySalesValue;
    @ApiModelProperty("微信支付")
    private String wxPayPrice;
}
