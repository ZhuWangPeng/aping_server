package com.aping.manage.index.dao;

import com.aping.manage.index.dto.StatDto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VisitDao {
   void addUserVisit(StatDto statDto);
}
