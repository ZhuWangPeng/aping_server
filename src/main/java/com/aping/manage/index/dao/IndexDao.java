package com.aping.manage.index.dao;

import com.aping.manage.index.vo.StatVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface IndexDao {
    List<StatVo> queryList(@Param("currDate") String currDate,@Param("lstWeekDate") String lstWeekDate);

    StatVo queryStatVo(@Param("currDate") String currDate);

    StatVo queryTotal();
}
