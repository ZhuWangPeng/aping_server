package com.aping.manage.sysparam.dao;

import com.aping.manage.sysparam.bean.SysParamBean;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysParamDao {

	String getSysParam(@Param("paramCode") String code);

	List<SysParamBean> getSysParamList();

	void addSysParam(SysParamBean bean);

	List<SysParamBean> queryList(SysParamBean bean);

	void editSysParam(SysParamBean bean);

	void deleteSysParam(SysParamBean bean);

	void addSysParamRecord(SysParamBean bean);

	SysParamBean getSysParamByParamCode(@Param("paramCode") String code);

}
