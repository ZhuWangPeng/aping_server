package com.aping.manage.sysparam.service.impl;

import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.CommonUtil;
import com.aping.manage.user.service.LoginService;
import com.aping.manage.sysparam.dao.SysParamDao;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.manage.sysparam.bean.SysParamBean;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class SysParamServiceImpl implements SysParamService {
	
	@Resource
	private SysParamDao dao;
	@Resource
	private RedisHandle redisHandle;
	@Resource
	private LoginService loginService;

	@Override
	public String getSysParam(String paramCode) {
		return dao.getSysParam(paramCode);
	}

	@Override
	public String getSysParamRedis(String paramCode) {
		String paramValue;
		try {
			paramValue = redisHandle.getMapField(SysConstant.REDIS_KEY_APING_SYS_PARAM, paramCode);
			if(StringUtils.isEmpty(paramValue)) {
				paramValue = getSysParam(paramCode);
				initSysParam();
			}
			return paramValue;
		}catch (Exception e){
			log.info(">>>>>>获取系统参数错误:{}<<<<<<",e.getMessage());
			paramValue = getSysParam(paramCode);
			initSysParam();
			return paramValue;
		}
	}

	/**
	 * 查询
	 */
	@Override
	public Result queryList(SysParamBean bean, Page<Integer> page) {
		PageHelper.startPage(page.getPageNum(), page.getPageSize());
		List<SysParamBean> list = dao.queryList(bean);
		return Result.resultInstance().success(new PageInfo<>(list));
	}

	/**
	 * 修改
	 * @param bean
	 * @return
	 */
	@Override
	public Result editSysParam(SysParamBean bean) {
//		if (StringUtils.isEmpty(CommonUtil.getCurrentUserId())) {
//			return Result.resultInstance().fail("请先登录！");
//		}
//		if("MESSAGE_SWITCH".equals(bean.getParamCode())
//				|| "UP_INTERESTS_SWITCH".equals(bean.getParamCode())
//				|| "MON_INTERESTS_SWITCH".equals(bean.getParamCode())
//				|| "JOB_MESSAGE_SWITCH".equals(bean.getParamCode())
//				|| "JOB_UP_INTERESTS_SWITCH".equals(bean.getParamCode())
//				|| "INTERESTS_FUSE_SWITCH".equals(bean.getParamCode())
//		){
//			if(!"1".equals(bean.getParamValue()) && !"0".equals(bean.getParamValue())){
//				throw new MyException(SysConstant.API_RESULT_PARAMS_ERROR,"开关阈值只能为0(关)或1(开)");
//			}
//		}
		try {
			bean.setLstAdminId(loginService.getAdminId());
			bean.setLstModifyTime(CommonUtil.getSeverTime());
			dao.editSysParam(bean);
			//修改redis里的参数信息
			initSysParam();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("修改失败",e);
			return Result.resultInstance().fail("修改失败" + e.getMessage());
		}
		return Result.resultInstance().success("修改成功");
	}
	/**
	 * 删除
	 */
	@Override
	public Result deleteSysParam(SysParamBean bean) {
//		if (StringUtils.isEmpty(CommonUtil.getCurrentUserId())) {
//			return Result.resultInstance().fail("请先登录！");
//		}
//		if("MESSAGE_SWITCH".equals(bean.getParamCode())
//				|| "UP_INTERESTS_SWITCH".equals(bean.getParamCode())
//				|| "MON_INTERESTS_SWITCH".equals(bean.getParamCode())
//				|| "JOB_MESSAGE_SWITCH".equals(bean.getParamCode())
//				|| "JOB_UP_INTERESTS_SWITCH".equals(bean.getParamCode())
//				|| "INTERESTS_FUSE_SWITCH".equals(bean.getParamCode())
//		){
//			throw new MyException(SysConstant.API_RESULT_PARAMS_ERROR,"开关不可删除");
//		}
//		if("CUST_UP_TASKID".equals(bean.getParamCode())
//				|| "MON_SWITCH_CLOSE_PROMPT".equals(bean.getParamCode())
//				|| "MON_WARN_VALUE".equals(bean.getParamCode())
//				|| "UP_WARN_VALUE".equals(bean.getParamCode())
//				|| "MON_FUSE_VALUE".equals(bean.getParamCode())
//				|| "UP_FUSE_VALUE".equals(bean.getParamCode())
//				|| "QYPT_SM_MSG_NO".equals(bean.getParamCode())
//				|| "QYPT_SM_MSG_RECEIVE_PHONE".equals(bean.getParamCode())
//				|| "QYPT_SM_MSG_SEND_PATH".equals(bean.getParamCode())
//		){
//			throw new MyException(SysConstant.API_RESULT_PARAMS_ERROR,"不可删除");
//		}
		try {
			bean.setLstAdminId(loginService.getAdminId());
			bean.setLstModifyTime(CommonUtil.getSeverTime());
			dao.deleteSysParam(bean);
			//修改redis里的参数信息
			initSysParam();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("删除失败",e);
			return Result.resultInstance().fail("删除失败" + e.getMessage());
		}
		return Result.resultInstance().success("删除成功");
	}

	@Override
	public void initSysParam(){
		List<SysParamBean> list = dao.getSysParamList();
		if (CollectionUtils.isNotEmpty(list)) {
			if(redisHandle.exists(SysConstant.REDIS_KEY_APING_SYS_PARAM)){
				redisHandle.remove(SysConstant.REDIS_KEY_APING_SYS_PARAM);
			}
			list.forEach(item -> {
				redisHandle.addMap(SysConstant.REDIS_KEY_APING_SYS_PARAM, item.getParamCode(), item.getParamValue());
			});
		}
	}
}
