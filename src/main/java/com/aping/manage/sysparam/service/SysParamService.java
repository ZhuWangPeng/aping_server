package com.aping.manage.sysparam.service;

import com.aping.common.model.Result;
import com.aping.manage.sysparam.bean.SysParamBean;
import com.github.pagehelper.Page;


public interface SysParamService {

	String getSysParam(String paramCode);
	String getSysParamRedis(String paramCode);
	Result queryList(SysParamBean bean, Page<Integer> page);
	Result editSysParam(SysParamBean bean);
	Result deleteSysParam(SysParamBean bean);
	void initSysParam();
}
