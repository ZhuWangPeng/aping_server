package com.aping.manage.sysparam.controller;

import com.aping.common.model.Result;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.manage.sysparam.bean.SysParamBean;
import com.aping.manage.sysparam.valid.SysParamDel;
import com.aping.manage.sysparam.valid.SysParamEdit;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "参数管理")
@RestController
@Slf4j
@RequestMapping(value = "/sysParam")
public class SysParamController {
	@Autowired
	private SysParamService service;
	/**
	 * 查询列表
	 * @param bean
	 * @param page
	 * @return
	 */
	@ApiOperation("查询列表")
	@GetMapping(value = "/queryList")
	public Result queryList(SysParamBean bean, Page<Integer> page){
		return service.queryList(bean,page);
	}

	/**
	 * 修改参数
	 * @param bean
	 * @return
	 */
	@ApiOperation("修改参数")
	@PostMapping(value = "/editSysParam")
	public Result editSysParam(@RequestBody @Validated(value = SysParamEdit.class) SysParamBean bean){
		return service.editSysParam(bean);
	}
	/**
	 * 删除参数
	 * @param bean
	 * @return
	 */
	@ApiOperation("删除参数")
	@PostMapping(value = "/deleteSysParam")
	public Result deleteSysParam(@RequestBody @Validated(value = SysParamDel.class) SysParamBean bean){
		return service.deleteSysParam(bean);
	}
	

}
