package com.aping.manage.sysparam.bean;

import com.aping.manage.sysparam.valid.SysParamDel;
import com.aping.manage.sysparam.valid.SysParamEdit;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class SysParamBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -388948910570265793L;
	@ApiModelProperty("参数编码")
	@NotBlank(message = "参数编码不可为空",groups = {SysParamEdit.class, SysParamDel.class})
	private String paramCode;
	@ApiModelProperty("参数名称")
	private String paramName;
	@ApiModelProperty("参数值")
	private String paramValue;
	@ApiModelProperty("备注")
	private String paramMemo;
	@ApiModelProperty("是否可编辑 1：是 0：否")
	private String enable;
	@ApiModelProperty("是否删除 1：是 0：否")
	@JsonIgnore
	private String isDel;
	@ApiModelProperty("最近一次修改人的id")
	@JsonIgnore
	private String lstAdminId;
//	@ApiModelProperty("最近一次修改日期")
//	private String lastModifyDate;
	@ApiModelProperty("最近一次修改时间")
	private String lstModifyTime;
}
