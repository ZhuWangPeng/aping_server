package com.aping.manage.sysparam.init;

import com.aping.manage.goods.service.ImageService;
import com.aping.manage.sysparam.service.SysParamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Order(value=1)
public class ParamsInitRunner implements CommandLineRunner{
	
	private static final Logger log = LoggerFactory.getLogger(ParamsInitRunner.class);
	
	@Resource
	private SysParamService sysParamServcie;
	@Resource
	private ImageService imageService;

	@Override
	public void run(String... args) throws Exception {
		long initParam = System.currentTimeMillis();
		sysParamServcie.initSysParam();
		log.info("*****系统参数初始化成功,耗时{}ms*****",System.currentTimeMillis() - initParam);
		long initImageUr = System.currentTimeMillis();
		imageService.initImageUrl();
		log.info("*****图片地址初始化成功,耗时{}ms*****",System.currentTimeMillis() - initImageUr);
		long initBannerList = System.currentTimeMillis();
		imageService.initBannerList();
		log.info("*****广告栏位初始化成功,耗时{}ms*****",System.currentTimeMillis() - initBannerList);
	} 
		
}
