package com.aping.manage.goods.dao;

import com.aping.manage.goods.dto.ImageInfoDto;
import com.aping.manage.goods.vo.ImageInfoVo;
import com.aping.manage.goods.vo.ImageVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ImageDao {
    int addImageInfo(ImageInfoDto dto);

    ImageVo queryByImgUrl(@Param("imgUrl")String imgUrl);

    List<ImageInfoVo> queryList(ImageInfoDto dto);
    List<ImageVo> queryBanner();

    List<ImageVo> queryBottom();

    void delImages(@Param("imgId") String imgId);

    String getImageUrl(@Param("imgId") String imgId);
}
