package com.aping.manage.goods.dao;


import com.aping.manage.goods.dto.MenuDto;
import com.aping.manage.goods.vo.MenuListVo;
import com.aping.manage.goods.vo.MenuVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuDao {
    void addMenu(MenuDto dto);

    List<MenuVo> queryList(MenuDto dto);

    List<MenuListVo> queryMenuList();

    void updateMenu(MenuDto dto);
}
