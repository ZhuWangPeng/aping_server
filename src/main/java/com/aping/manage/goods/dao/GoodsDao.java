package com.aping.manage.goods.dao;

import com.aping.manage.goods.dto.GoodsDto;
import com.aping.manage.goods.dto.MenuGoodsDto;
import com.aping.manage.goods.vo.GoodsDetailVo;
import com.aping.manage.goods.vo.GoodsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface GoodsDao {
    List<GoodsVo> queryList(GoodsDto dto);

    GoodsDetailVo queryGoodsDetail(String goodsId);

    void addGoods(GoodsDto dto);

    void updateGoods(GoodsDto dto);

    void delGoods(String goodsId);

}
