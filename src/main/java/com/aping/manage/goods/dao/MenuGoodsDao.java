package com.aping.manage.goods.dao;

import com.aping.manage.goods.dto.MenuGoodsDto;
import com.aping.manage.goods.vo.GoodsOrderVo;
import com.aping.manage.goods.vo.MenuOrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MenuGoodsDao {

    List<MenuOrderVo> queryMenuOrder();

    List<GoodsOrderVo> queryGoodsOrder(String menuId);

    void addMenuGoods(@Param("menuGoodsList") List<MenuGoodsDto> menuGoodsList);

     void delMenuGoods(String goodsId);

     void delMenuGoodsByMenuId(String menuId);

     void delMenuGoodsByMenuIdList(@Param("menuIdList") List<String> menuIdList);

     void updateMenuOrder(@Param("menuOrderVoList")List<MenuOrderVo> menuOrderVoList);

}
