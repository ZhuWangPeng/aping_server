package com.aping.manage.goods.controller;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.GoodsDto;
import com.aping.manage.goods.service.GoodsService;
import com.aping.manage.goods.dto.MenuDto;
import com.aping.manage.goods.valid.GoodsAdd;
import com.aping.manage.goods.valid.GoodsDel;
import com.aping.manage.goods.valid.GoodsUpdate;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/goods")
@Api(tags = "商品")
@Slf4j
public class GoodsController {
    @Resource
    private GoodsService service;

    @ApiOperation(value = "商品查询")
    @GetMapping("/query")
    public Result query(GoodsDto dto, Page<Integer> page){
        return service.query(dto,page);
    }

    @ApiOperation(value = "商品查询")
    @GetMapping("/queryDetail")
    public Result queryDetail(@RequestParam(value = "goodsId") String goodsId){
        return service.queryDetail(goodsId);
    }

    @ApiOperation("新增商品")
    @GetMapping("/add")
    public Result add(@Validated(value = GoodsAdd.class) GoodsDto dto){
        return service.add(dto);
    }

    @ApiOperation("修改商品")
    @GetMapping("/update")
    public Result update(@Validated(value = GoodsUpdate.class) GoodsDto dto){
        return service.update(dto);
    }

    @ApiOperation("删除商品")
    @GetMapping("/del")
    public Result del(@Validated(value = GoodsDel.class) GoodsDto dto){
        return service.del(dto);
    }
}
