package com.aping.manage.goods.controller;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.ImageInfoDto;
import com.aping.manage.goods.service.ImageService;
import com.aping.manage.goods.valid.Upload;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/images")
@Api(tags = "图片")
@Slf4j
public class ImageController {

    @Resource
    private ImageService service;

    @ApiOperation(value = "上传图片")
    @PostMapping("/upload")
    public Result upload(HttpServletRequest request,@Validated(value = Upload.class) ImageInfoDto dto){
        return service.upload(request,dto);
    }

    @ApiOperation(value = "查询广告栏位")
    @GetMapping("/queryBanner")
    public Result queryBanner(){
        return service.queryBanner();
    }

    @ApiOperation(value = "查询小程序码底部图片")
    @GetMapping("/queryBottom")
    public Result queryBottom(){
        return service.queryBottom();
    }


    @ApiOperation(value = "查询列表")
    @GetMapping("/queryList")
    public Result queryList(ImageInfoDto dto,Page<Integer> page){
        return service.queryList(dto,page);
    }

    @ApiOperation(value = "删除")
    @GetMapping("/del")
    public Result del(@RequestParam String imgId){
        return service.delImage(imgId);
    }
}
