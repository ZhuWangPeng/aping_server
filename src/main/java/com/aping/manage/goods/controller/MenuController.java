package com.aping.manage.goods.controller;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.MenuDto;
import com.aping.manage.goods.service.MenuService;
import com.aping.manage.goods.valid.MenuAdd;
import com.aping.manage.goods.valid.MenuDel;
import com.aping.manage.goods.valid.MenuUpdate;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/menu")
@Api(tags = "菜单")
@Slf4j
public class MenuController {
    @Resource
    private MenuService service;

    @ApiOperation(value = "菜单查询")
    @GetMapping("/query")
    public Result query(MenuDto dto, Page<Integer> page){
        return service.query(dto,page);
    }

    @ApiOperation(value = "查询菜单列表")
    @GetMapping("/queryList")
    public Result queryList(){
        return service.queryList();
    }


    @ApiOperation("新增菜单")
    @GetMapping("/add")
    public Result add(@Validated(value = MenuAdd.class) MenuDto dto){
        return service.add(dto);
    }

    @ApiOperation("修改菜单")
    @GetMapping("/update")
    public Result update(@Validated(value = MenuUpdate.class) MenuDto dto){
        return service.update(dto);
    }

    @ApiOperation("删除菜单")
    @GetMapping("/del")
    public Result del(@Validated(value = MenuDel.class) MenuDto dto){
        return service.del(dto);
    }
}
