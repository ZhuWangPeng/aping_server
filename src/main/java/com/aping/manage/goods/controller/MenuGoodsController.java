package com.aping.manage.goods.controller;

import com.aping.common.model.Result;
import com.aping.manage.goods.service.MenuGoodsService;
import com.aping.manage.goods.vo.MenuOrderVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/menuOrder")
@Api(tags = "菜单排序")
@Slf4j
public class MenuGoodsController {

    @Resource
    private MenuGoodsService menuGoodsService;

    @ApiOperation(value = "查询")
    @GetMapping("/query")
    public Result query(){
        return menuGoodsService.query();
    }

    @ApiOperation(value = "修改")
    @PostMapping("/update")
    public Result update(@RequestBody List<MenuOrderVo> menuOrderVoList){
        return menuGoodsService.update(menuOrderVoList);
    }

}
