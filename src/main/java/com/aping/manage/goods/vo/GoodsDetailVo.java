package com.aping.manage.goods.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GoodsDetailVo {
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品图片地址，英文逗号隔开")
    @JsonIgnore
    private String imgId;
    private List<String> imgUrlList;
    @ApiModelProperty("商品描述")
    private String goodsMemo;
    @ApiModelProperty("商品详情 富文本")
    private String goodsDetail;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品库存")
    private String goodsStock;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("商品排序")
    private String goodsOrder;
    @ApiModelProperty("商品成本")
    private String goodsCast;
//    @ApiModelProperty("商品数量")
//    private String goodsNum;
    @ApiModelProperty("快递费")
    private String expressFee;
    @ApiModelProperty("标签")
    private String label;
    @JsonIgnore
    @ApiModelProperty("是否删除 0-否 1-是")
    private String isDel;
}