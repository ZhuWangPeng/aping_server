package com.aping.manage.goods.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ImageInfoVo {
    @ApiModelProperty("图片id")
    private String imgId;
    @ApiModelProperty("图片名称")
    private String imgName;
    @ApiModelProperty("图片地址")
    private String imgUrl;
    @ApiModelProperty("图片在服务器中存储的地址")
    private String imgAddress;
    @ApiModelProperty("图片所属")
    private String belongTo;
    @ApiModelProperty("是否删除 0-否 1-是")
    @JsonIgnore
    private String isDel;
    private String insertTime;
}
