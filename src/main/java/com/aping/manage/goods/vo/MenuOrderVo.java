package com.aping.manage.goods.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class MenuOrderVo {
    @ApiModelProperty("菜单id")
    private String menuId;
    @ApiModelProperty("菜单名称")
    private String menuName;
    @ApiModelProperty("菜单名称")
    private String menuOrder;
    @ApiModelProperty("商品排序列表")
    private List<GoodsOrderVo> goodsOrderVoList;
}
