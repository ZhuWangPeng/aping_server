package com.aping.manage.goods.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class GoodsVo {
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("所属菜单id,英文逗号隔开")
    private String menuId;
    @ApiModelProperty("所属菜单名称,英文逗号隔开")
    private String menuName;
    @ApiModelProperty("商品图片id，英文逗号隔开")
    private String imgId;
    private List<String> imgUrl;
    @ApiModelProperty("商品描述")
    private String goodsMemo;
    @ApiModelProperty("商品详情")
    private String goodsDetail;
    @ApiModelProperty("商品标签")
    private String label;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("商品快递费")
    private String expressFee;
    @ApiModelProperty("商品成本")
    private String goodsCost;
    @ApiModelProperty("0-下架 1-上架")
    private String goodsStatus;
    @ApiModelProperty("商品价格")
    private String goodsPrice;
    @ApiModelProperty("商品库存")
    private String goodsStock;
//    @ApiModelProperty("商品排序")
//    private String goodsOrder;
//    @ApiModelProperty("写入日期")
//    private String insertDate;
//    @ApiModelProperty("写入时间")
    private String insertTime;
//    @ApiModelProperty("写入人")
//    private String insertUser;
//    @ApiModelProperty("最后一次更新日期")
//    private String lstUpdateDate;
//    @ApiModelProperty("最后一次更新时间")
//    private String lstUpdateTime;
//    @ApiModelProperty("最后一次更新人")
//    private String lstUpdateUser;
}
