package com.aping.manage.goods.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MenuListVo {
    @ApiModelProperty("菜单id")
    private String menuId;
    @ApiModelProperty("菜单名称")
    private String menuName;
}
