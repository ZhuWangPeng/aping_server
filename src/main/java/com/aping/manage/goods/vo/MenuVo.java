package com.aping.manage.goods.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class MenuVo {
    @ApiModelProperty("菜单id")
    private String menuId;
    @ApiModelProperty("菜单名称")
    private String menuName;
    @ApiModelProperty("菜单说明")
    private String menuMemo;
    @ApiModelProperty("菜单排序")
    private String menuOrder;
    @ApiModelProperty("是否对客展示 0-否 1-是")
    private String menuStatus;
//    private List<GoodsVo> goodsVoList;
//    @ApiModelProperty("写入时间")
    private String insertTime;
//    @ApiModelProperty("写入人")
//    private String insertUser;
//    @ApiModelProperty("最后一次更新时间")
//    private String lstUpdateTime;
//    @ApiModelProperty("最后一次更新人")
//    private String lstUpdateUser;
}
