package com.aping.manage.goods.dto;

import com.aping.manage.goods.valid.MenuAdd;
import com.aping.manage.goods.valid.MenuDel;
import com.aping.manage.goods.valid.MenuUpdate;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MenuGoodsDto {
    @ApiModelProperty("菜单id")
    private String menuId;
    @ApiModelProperty("商品Id")
    private String goodsId;
    @ApiModelProperty("该菜单下商品的序号")
    private int goodsOrder;
}
