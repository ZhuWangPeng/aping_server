package com.aping.manage.goods.dto;

import com.aping.manage.goods.valid.GoodsAdd;
import com.aping.manage.goods.valid.GoodsDel;
import com.aping.manage.goods.valid.GoodsUpdate;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class GoodsDto {
    @ApiModelProperty("商品id")
    @NotBlank(message = "商品id不可为空",groups = {GoodsUpdate.class, GoodsDel.class})
    private String goodsId;
    @ApiModelProperty("商品名称")
    @NotBlank(message = "商品名称不可为空",groups = {GoodsAdd.class,GoodsUpdate.class})
    private String goodsName;
    @ApiModelProperty("所属菜单id,逗号隔开")
    @NotBlank(message = "所属菜单id不可为空",groups = {GoodsAdd.class,GoodsUpdate.class})
    private String menuId;
    @ApiModelProperty("商品图片id，英文逗号隔开")
    private String imgId;
    @ApiModelProperty("商品描述")
    private String goodsMemo;
    @ApiModelProperty("商品详情")
    private String goodsDetail;
    @ApiModelProperty("商品标签")
    private String label;
    @ApiModelProperty("商品销量")
    private String goodsVolume;
    @ApiModelProperty("商品快递费")
    private String expressFee;
    @ApiModelProperty("商品成本")
    private String goodsCost;
    @ApiModelProperty("商品状态 0-下架 1-上架")
    @NotBlank(message = "商品状态不可为空",groups = {GoodsAdd.class,GoodsUpdate.class})
    private String goodsStatus;
    @ApiModelProperty("商品价格")
    @NotBlank(message = "商品价格不可为空",groups = {GoodsAdd.class,GoodsUpdate.class})
    private String goodsPrice;
    @ApiModelProperty("商品库存")
    @NotBlank(message = "商品库存不可为空",groups = {GoodsAdd.class,GoodsUpdate.class})
    private String goodsStock;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("写入人")
    private String insertUser;
    @ApiModelProperty("最后一次更新日期")
    private String lstUpdateDate;
    @ApiModelProperty("最后一次更新时间")
    private String lstUpdateTime;
    @ApiModelProperty("最后一次更新人")
    private String lstUpdateUser;
}
