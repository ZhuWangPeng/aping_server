package com.aping.manage.goods.dto;

import com.aping.manage.goods.valid.Upload;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ImageInfoDto {
    @ApiModelProperty("图片id")
    private String imgId;
    @ApiModelProperty("图片名称")
    private String imgName;
    @ApiModelProperty("图片地址")
    private String imgUrl;
    @ApiModelProperty("图片在服务器中存储的地址")
    private String imgAddress;
    @ApiModelProperty("图片所属 1-banner 2-goods 3-bottom")
    @NotBlank(message = "图片所属不可为空",groups = Upload.class)
    private String belongTo;
    @ApiModelProperty("是否删除 0-否 1-是")
    private String isDel;
    @NotNull(message = "文件不可为空",groups = Upload.class)
    private MultipartFile file;
    private String insertTime;
}
