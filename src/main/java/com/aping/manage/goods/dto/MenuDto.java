package com.aping.manage.goods.dto;

import com.aping.manage.goods.valid.MenuAdd;
import com.aping.manage.goods.valid.MenuDel;
import com.aping.manage.goods.valid.MenuUpdate;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class MenuDto {
    @ApiModelProperty("菜单id")
    @NotBlank(message = "菜单id不可为空",groups = {MenuUpdate.class, MenuDel.class})
    private String menuId;
    @ApiModelProperty("菜单名称")
    @NotBlank(message = "菜单名称不可为空",groups = {MenuAdd.class})
    private String menuName;
    @ApiModelProperty("菜单说明")
    private String menuMemo;
    @ApiModelProperty("菜单排序")
    @NotBlank(message = "菜单排序不可为空",groups = {MenuAdd.class})
    private String menuOrder;
    @ApiModelProperty("是否对客展示 0-否 1-是")
    @NotBlank(message = "是否对客展示不可为空",groups = {MenuAdd.class})
    private String menuStatus;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @ApiModelProperty("写入人")
    private String insertUser;
    @ApiModelProperty("最后一次更新日期")
    private String lstUpdateDate;
    @ApiModelProperty("最后一次更新时间")
    private String lstUpdateTime;
    @ApiModelProperty("最后一次更新人")
    private String lstUpdateUser;
    private String isDel;
}
