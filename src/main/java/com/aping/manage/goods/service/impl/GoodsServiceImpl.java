package com.aping.manage.goods.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.StringUtil;
import com.aping.manage.user.service.LoginService;
import com.aping.manage.goods.dao.GoodsDao;
import com.aping.manage.goods.dao.MenuGoodsDao;
import com.aping.manage.goods.dto.GoodsDto;
import com.aping.manage.goods.dto.MenuGoodsDto;
import com.aping.manage.goods.service.GoodsService;
import com.aping.manage.goods.service.ImageService;
import com.aping.manage.goods.vo.GoodsDetailVo;
import com.aping.manage.goods.vo.GoodsVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class GoodsServiceImpl implements GoodsService {

    @Resource
    private GoodsDao goodsdao;
    @Resource
    private MenuGoodsDao menuGoodsDao;
    @Resource
    private ImageService imageService;
    @Resource
    private LoginService loginService;

    @Override
    public Result query(GoodsDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(),page.getPageSize());
        List<GoodsVo> list = goodsdao.queryList(dto);
        list.forEach(goodsVo -> {
            if(StringUtil.isNotEmpty(goodsVo.getImgId())){
                goodsVo.setImgUrl(imageService.getImgUrlList(goodsVo.getImgId()));
            }
        });
        return Result.resultInstance().success(new PageInfo<>(list));
    }

    @Override
    public Result queryDetail(String goodsId) {
        GoodsDetailVo goodsDetailVo = goodsdao.queryGoodsDetail(goodsId);
        if(ObjectUtil.isEmpty(goodsDetailVo)){
            return Result.resultInstance().fail(SysCodeEnum.GOODS_NO_EXISTS);
        } else if ("1".equals(goodsDetailVo.getIsDel())) {
            return Result.resultInstance().fail(SysCodeEnum.GOODS_DEL);
        }
        if(StringUtil.isNotEmpty(goodsDetailVo.getImgId())){
            goodsDetailVo.setImgUrlList(imageService.getImgUrlList(goodsDetailVo.getImgId()));
        }
        return Result.resultInstance().success(goodsDetailVo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result add(GoodsDto dto) {
        String goodsId = IdUtil.getSpecLengthId(5);
        dto.setGoodsId(goodsId);
        dto.setInsertDate(CommonUtil.getSeverDate());
        dto.setInsertTime(CommonUtil.getSeverTime());
        dto.setInsertUser(loginService.getAdminId());
        dto.setLstUpdateDate(CommonUtil.getSeverDate());
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        dto.setLstUpdateUser(loginService.getAdminId());
        goodsdao.addGoods(dto);
        dealMenuGoods(dto.getMenuId(),goodsId);
        return Result.resultInstance().success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result update(GoodsDto dto) {
        dto.setLstUpdateDate(CommonUtil.getSeverDate());
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        dto.setLstUpdateUser(loginService.getAdminId());
        goodsdao.updateGoods(dto);
        menuGoodsDao.delMenuGoods(dto.getGoodsId());
        dealMenuGoods(dto.getMenuId(),dto.getGoodsId());
        return Result.resultInstance().success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result del(GoodsDto dto) {
        goodsdao.delGoods(dto.getGoodsId());
        menuGoodsDao.delMenuGoods(dto.getGoodsId());
        return Result.resultInstance().success();
    }

    private void dealMenuGoods(String menuIdStr,String goodsId){
        List<MenuGoodsDto> menuGoodsList = new ArrayList<>();
        String[] menuIdList = menuIdStr.split(",");
        for(String menuId : menuIdList){
            MenuGoodsDto menuGoodsDto = new MenuGoodsDto();
            menuGoodsDto.setMenuId(menuId);
            menuGoodsDto.setGoodsId(goodsId);
            menuGoodsDto.setGoodsOrder(0);
            menuGoodsList.add(menuGoodsDto);
        }
        menuGoodsDao.addMenuGoods(menuGoodsList);
    }
}
