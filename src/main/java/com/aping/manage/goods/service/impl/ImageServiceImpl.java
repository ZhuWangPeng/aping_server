package com.aping.manage.goods.service.impl;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.common.utils.StringUtil;
import com.aping.common.utils.UUIDUtils;
import com.aping.manage.goods.dao.ImageDao;
import com.aping.manage.goods.dto.ImageInfoDto;
import com.aping.manage.goods.service.ImageService;
import com.aping.manage.goods.vo.ImageInfoVo;
import com.aping.manage.goods.vo.ImageVo;
import com.aping.manage.sysparam.service.SysParamService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ImageServiceImpl implements ImageService {

    @Resource
    private ImageDao dao;
    @Resource
    private RedisHandle redisHandle;
    @Resource
    private SysParamService sysParamService;

    @Value("${image.localUrl}")
    private String realPath;

    @SneakyThrows
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result upload(HttpServletRequest request, ImageInfoDto dto) {
        MultipartFile file = dto.getFile();
        ImageVo imageVo = null;
        if(!file.isEmpty()){
            String fileName = file.getOriginalFilename();
            dto.setImgName(fileName);
            assert fileName != null;
            String type = fileName.contains(".") ? fileName.substring(fileName.lastIndexOf(".") + 1) : null;
            if(type != null){
                String trueFileName = UUIDUtils.getUUID() + IdUtil.getDateStr() + ".jpg";
                // 设置存放文件的路径
                String path = realPath + trueFileName;
                File dest = new File(path);
                if(!dest.getParentFile().exists()){
                    boolean temp = dest.getParentFile().mkdir();
                }
                String imgId = IdUtil.getImgId();
                dto.setImgId(imgId);
                // 可访问的图片url地址
                String url = sysParamService.getSysParamRedis("IMG_URl");
                String imgUrl =  url + "/images/" + trueFileName;
                dto.setImgUrl(imgUrl);
                dto.setImgAddress(realPath + trueFileName);
                dto.setInsertTime(CommonUtil.getSeverTime());
                int count = dao.addImageInfo(dto);
                if(count > 0){
                    // 保存图片
                    file.transferTo(new File(path));
                    imageVo = new ImageVo();
                    imageVo.setImgId(imgId);
                    imageVo.setImgUrl(imgUrl);
                }
                if("banner".equals(dto.getBelongTo())){
                    // 刷到bannerList
                    initBannerList();
                }else {
                    initImageUrl();
                }
            }else {
                log.info("图片类型:{}",type);
                return Result.resultInstance().fail(SysCodeEnum.IMG_TYPE_ERROR);
            }
        }
        if(imageVo == null){
            return Result.resultInstance().fail(SysCodeEnum.IMG_UPLOAD_ERROR);
        }else {
            return Result.resultInstance().success(imageVo);
        }
    }

    @Override
    public Result queryList(ImageInfoDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(),page.getPageSize());
        List<ImageInfoVo> list = dao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(list));
    }

    @Override
    public Result queryBanner() {
        List<ImageVo> list;
        if(redisHandle.exists(SysConstant.REDIS_KEY_BANNER_IMAGE_URL)){
            list = (List<ImageVo>) redisHandle.get(SysConstant.REDIS_KEY_BANNER_IMAGE_URL);
        }else {
            list = dao.queryBanner();
            initBannerList();
        }
        return Result.resultInstance().success(list);
    }

    @Override
    public Result queryBottom() {
        List<ImageVo> list = dao.queryBottom();
        return Result.resultInstance().success(list);
    }

    @Override
    public Result delImage(String imgId) {
        dao.delImages(imgId);
        initImageUrl();
        initBannerList();
        return Result.resultInstance().success();
    }


    @Override
    public String getImgUrl(String imgId) {
        String imgUrl;
        try {
            imgUrl = redisHandle.getMapField(SysConstant.REDIS_KEY_APING_IMAGE_URL, imgId);
            if(StringUtils.isEmpty(imgUrl)) {
                imgUrl = getImageUrl(imgId);
                initImageUrl();
            }
            return imgUrl;
        }catch (Exception e){
            e.printStackTrace();
            log.info(">>>>>>获取图片url错误:{}<<<<<<",e.getMessage());
            imgUrl = getImageUrl(imgId);
            initImageUrl();
            return imgUrl;
        }
    }

    private String getImageUrl(String imgId){
        return dao.getImageUrl(imgId);
    }

    @Override
    public void initImageUrl(){
        List<ImageInfoVo> list = dao.queryList(null);
        if (CollectionUtils.isNotEmpty(list)) {
            if(redisHandle.exists(SysConstant.REDIS_KEY_APING_IMAGE_URL)){
                redisHandle.remove(SysConstant.REDIS_KEY_APING_IMAGE_URL);
            }
            list.forEach(item -> {
                redisHandle.addMap(SysConstant.REDIS_KEY_APING_IMAGE_URL, item.getImgId(), item.getImgUrl());
            });
        }
    }

    @Override
    public void initBannerList(){
        List<ImageVo> list = dao.queryBanner();
        if (CollectionUtils.isNotEmpty(list)) {
            if(redisHandle.exists(SysConstant.REDIS_KEY_BANNER_IMAGE_URL)){
                redisHandle.remove(SysConstant.REDIS_KEY_BANNER_IMAGE_URL);
            }
            redisHandle.set(SysConstant.REDIS_KEY_BANNER_IMAGE_URL,list);
        }
    }

    @Override
    public List<String> getImgUrlList(String imgIdStr) {
        List<String> imgUrlList = new ArrayList<>();
        String[] imgIdArray = imgIdStr.split(",");
        for (String imgId : imgIdArray){
            imgUrlList.add(getImgUrl(imgId));
        }
        return imgUrlList;
    }
}
