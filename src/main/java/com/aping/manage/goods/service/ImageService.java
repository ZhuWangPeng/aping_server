package com.aping.manage.goods.service;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.ImageInfoDto;
import com.github.pagehelper.Page;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ImageService {
    Result upload(HttpServletRequest request, ImageInfoDto dto);

    Result queryList(ImageInfoDto dto, Page<Integer> page);
    Result queryBanner();
    Result queryBottom();

    Result delImage(String imgId);

    String getImgUrl(String imgId);

    void initImageUrl();

    void initBannerList();

    List<String> getImgUrlList(String imgId);
}
