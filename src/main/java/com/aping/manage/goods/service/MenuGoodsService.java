package com.aping.manage.goods.service;

import com.aping.common.model.Result;
import com.aping.manage.goods.vo.MenuOrderVo;

import java.util.List;

public interface MenuGoodsService {
    Result query();

    Result update(List<MenuOrderVo> menuOrderVoList);
}
