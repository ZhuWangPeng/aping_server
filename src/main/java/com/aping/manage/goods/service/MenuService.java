package com.aping.manage.goods.service;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.MenuDto;
import com.github.pagehelper.Page;

public interface MenuService {
    Result query(MenuDto dto, Page<Integer> page);

    Result queryList();

    Result add(MenuDto dto);

    Result update(MenuDto dto);

    Result del(MenuDto dto);
}
