package com.aping.manage.goods.service.impl;

import com.aping.common.model.Result;
import com.aping.manage.goods.dao.MenuGoodsDao;
import com.aping.manage.goods.dto.MenuGoodsDto;
import com.aping.manage.goods.service.MenuGoodsService;
import com.aping.manage.goods.vo.GoodsOrderVo;
import com.aping.manage.goods.vo.MenuOrderVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class MenuGoodsServiceImpl implements MenuGoodsService {

    @Resource
    private MenuGoodsDao menuGoodsDao;

    @Override
    public Result query() {
        List<MenuOrderVo> menuOrderVoList = menuGoodsDao.queryMenuOrder();
        menuOrderVoList.forEach(menuOrderVo -> {
            menuOrderVo.setGoodsOrderVoList(menuGoodsDao.queryGoodsOrder(menuOrderVo.getMenuId()));
        });
        return Result.resultInstance().success(menuOrderVoList);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result update(List<MenuOrderVo> menuOrderVoList) {
        List<String> menuIdList = new ArrayList<>();
        List<MenuGoodsDto> menuGoodsDtoList = new ArrayList<>();
        menuOrderVoList.forEach(menuOrderVo -> {
            menuIdList.add(menuOrderVo.getMenuId());
            menuOrderVo.getGoodsOrderVoList().forEach(goodsOrderVo -> {
                MenuGoodsDto menuGoodsDto = new MenuGoodsDto();
                menuGoodsDto.setMenuId(menuOrderVo.getMenuId());
                menuGoodsDto.setGoodsId(goodsOrderVo.getGoodsId());
                menuGoodsDto.setGoodsOrder(Integer.parseInt(goodsOrderVo.getGoodsOrder()));
                menuGoodsDtoList.add(menuGoodsDto);
            });
        });
        if(!menuOrderVoList.isEmpty()){
            menuGoodsDao.updateMenuOrder(menuOrderVoList);
        }
        if(!menuIdList.isEmpty()){
            menuGoodsDao.delMenuGoodsByMenuIdList(menuIdList);
        }
        if(!menuGoodsDtoList.isEmpty()){
            menuGoodsDao.addMenuGoods(menuGoodsDtoList);
        }
        return Result.resultInstance().success();
    }
}
