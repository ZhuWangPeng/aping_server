package com.aping.manage.goods.service;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.GoodsDto;
import com.aping.manage.goods.dto.MenuDto;
import com.github.pagehelper.Page;
import org.springframework.web.bind.annotation.RequestParam;

public interface GoodsService {
    Result query(GoodsDto dto, Page<Integer> page);
    Result queryDetail(String goodsId);
    Result add(GoodsDto dto);

    Result update(GoodsDto dto);

    Result del(GoodsDto dto);
}
