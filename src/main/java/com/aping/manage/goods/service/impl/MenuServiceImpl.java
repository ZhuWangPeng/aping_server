package com.aping.manage.goods.service.impl;

import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.manage.user.service.LoginService;
import com.aping.manage.goods.dao.GoodsDao;
import com.aping.manage.goods.dao.MenuDao;
import com.aping.manage.goods.dao.MenuGoodsDao;
import com.aping.manage.goods.dto.MenuDto;
import com.aping.manage.goods.service.MenuService;
import com.aping.manage.goods.vo.MenuListVo;
import com.aping.manage.goods.vo.MenuVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuDao dao;
    @Resource
    private GoodsDao goodsDao;
    @Resource
    private MenuGoodsDao menuGoodsDao;
    @Resource
    private LoginService loginService;



    @Override
    public Result query(MenuDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(),page.getPageSize());
        List<MenuVo> list = dao.queryList(dto);
//        List<GoodsVo> goodsVoList = goodsDao.queryList(null);
//        list.forEach(menuVo -> {
//            menuVo.setGoodsVoList(goodsVoList.stream().filter(goodsVo ->
//                    goodsVo.getMenuId().contains(menuVo.getMenuId())).collect(Collectors.toList()));
//        });
        return Result.resultInstance().success(new PageInfo<>(list));
    }

    @Override
    public Result queryList() {
        List<MenuListVo> list = dao.queryMenuList();
        return Result.resultInstance().success(list);
    }

    @Override
    public Result add(MenuDto dto) {
        dto.setMenuId(IdUtil.getSpecLengthId(5));
        dto.setInsertDate(CommonUtil.getSeverDate());
        dto.setInsertTime(CommonUtil.getSeverTime());
        dto.setInsertUser(loginService.getAdminId());
        dto.setLstUpdateDate(CommonUtil.getSeverDate());
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        dto.setLstUpdateUser(loginService.getAdminId());
        dao.addMenu(dto);
        return Result.resultInstance().success();
    }

    @Override
    public Result update(MenuDto dto) {
        dto.setLstUpdateDate(CommonUtil.getSeverDate());
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        dto.setLstUpdateUser(loginService.getAdminId());
        dao.updateMenu(dto);
        return Result.resultInstance().success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result del(MenuDto dto) {
        dto.setLstUpdateDate(CommonUtil.getSeverDate());
        dto.setLstUpdateTime(CommonUtil.getSeverTime());
        dto.setLstUpdateUser(loginService.getAdminId());
        dto.setIsDel("1");
        dao.updateMenu(dto);
        menuGoodsDao.delMenuGoodsByMenuId(dto.getMenuId());
        return Result.resultInstance().success();
    }
}
