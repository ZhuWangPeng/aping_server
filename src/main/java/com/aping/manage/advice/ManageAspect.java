package com.aping.manage.advice;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.StringUtil;
import com.aping.manage.user.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 管理端访问记录
 */
@Aspect
@Component
@Order(1)
@Slf4j
public class ManageAspect {

    @Resource
    private LoginService loginService;

    @Pointcut("(execution(* com.aping.manage.*.controller.*.*(..)) && " +
            "!execution(* com.aping.manage.user.controller.LoginController.*(..)))")
    public void aopManageAspect(){}

    @Around("aopManageAspect()")
    public Result doManageAround(ProceedingJoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();
        Map<String, String> paramsMap = convertMap(request.getParameterMap());
        String adminToken = request.getHeader("adminToken");
        if(StringUtil.isNotEmpty(adminToken)){
            Result result;
            try {
                result = loginService.checkAdminToken(adminToken);
            }catch (Exception e){
                e.printStackTrace();
                return Result.resultInstance().fail(SysCodeEnum.ADMIN_TOKEN_CHECK_ERROR);
            }
            if(!"200".equals(result.getCode())){
                return result;
            }
            return (Result) joinPoint.proceed();
        }else {
            return Result.resultInstance().fail(SysCodeEnum.ADMIN_TOKEN_NULL);
        }

    }

    public Map<String, String> convertMap(Map<String, String[]> paramMap){
        Map<String,String> rtnMap = new HashMap<>();
        for(String key: paramMap.keySet()){
            rtnMap.put(key, paramMap.get(key)[0]);
        }
        return rtnMap;
    }

}
