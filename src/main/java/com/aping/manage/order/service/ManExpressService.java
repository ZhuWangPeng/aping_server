package com.aping.manage.order.service;

import com.aping.common.model.Result;
import com.aping.manage.order.dto.ExpressDto;
import com.github.pagehelper.Page;

public interface ManExpressService {
    Result queryList();
    Result query(ExpressDto dto, Page<Integer> page);
    Result add(ExpressDto dto);
    Result update(ExpressDto dto);
    Result del(String expressId);
}
