package com.aping.manage.order.service.impl;

import com.aping.common.model.Result;
import com.aping.manage.order.dao.ManMsgDao;
import com.aping.manage.order.dto.MsgDto;
import com.aping.manage.order.service.ManMsgService;
import com.aping.manage.order.vo.MsgVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class ManMsgServiceImpl implements ManMsgService {

    @Resource
    private ManMsgDao manMsgDao;

    @Override
    public Result queryList(MsgDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<MsgVo> msgVoList = manMsgDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(msgVoList));
    }
}
