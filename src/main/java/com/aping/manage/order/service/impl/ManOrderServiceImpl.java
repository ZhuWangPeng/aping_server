package com.aping.manage.order.service.impl;

import cn.hutool.core.date.DateUtil;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisLockHelper;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.MyExcelUtils;
import com.aping.common.utils.StringUtil;
import com.aping.manage.flow.dao.ManAmountDao;
import com.aping.manage.flow.dao.ManWeChatDao;
import com.aping.manage.flow.vo.AmountVo;
import com.aping.manage.flow.vo.WeChatVo;
import com.aping.manage.order.dao.ManOrderDao;
import com.aping.manage.order.dto.ManOrderDto;
import com.aping.manage.order.dto.ManOrderExpressDto;
import com.aping.manage.order.dto.OrderExpressDetailDto;
import com.aping.manage.order.dto.OrderExpressDto;
import com.aping.manage.order.service.ManOrderService;
import com.aping.manage.order.vo.*;
import com.aping.mob.flow.service.AmountService;
import com.aping.mob.order.dao.OrderDao;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.OrderVo;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.wechat.service.WeChatPayService;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ManOrderServiceImpl implements ManOrderService {

    @Resource
    private ManOrderDao manOrderDao;
    @Resource
    private ManAmountDao manAmountDao;
    @Resource
    private ManWeChatDao manWeChatDao;
    @Resource
    private OrderDao orderDao;
    @Resource
    private WeChatPayService weChatPayService;
    @Resource
    private RedisLockHelper redisLockHelper;
    @Resource
    private AmountService amountService;

    @Override
    public Result queryList(ManOrderDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(),page.getPageSize());
        List<ManOrderVo> orderVoList = manOrderDao.queryList(dto);
        // 订单详情列表
        List<ManOrderDetailVo> detailVoList = manOrderDao.queryDetailList();
        // 地址列表
        List<ManAddressVo> addressVoList = manOrderDao.queryAddressList();
        HashMap<String,ManAddressVo> addressVoHashMap = new HashMap<>();
        addressVoList.forEach(addressVo -> {
            addressVoHashMap.put(addressVo.getAddressId(),addressVo);
        });
        orderVoList.forEach(orderVo -> {
            orderVo.setOrderDetailVoList(detailVoList.stream().filter(orderDetailVo ->
                            orderDetailVo.getOrderId().equals(orderVo.getOrderId()))
                    .collect(Collectors.toList()));
            orderVo.setAddressVo(addressVoHashMap.get(orderVo.getAddressId()));
        });
        return Result.resultInstance().success(new PageInfo<>(orderVoList));
    }

    @Override
    public Result updateExpress(List<ManOrderExpressDto> list) {
        List<OrderExpressDto> dtoList = new ArrayList<>();
        String orderId = list.get(0).getOrderId();
        list.forEach(dto -> {
            List<OrderExpressDetailDto> lists = dto.getList();
            lists.forEach(expressDto->{
                OrderExpressDto orderExpressDto = new OrderExpressDto();
                orderExpressDto.setExpressNumber(dto.getExpressNumber());
                orderExpressDto.setOrderId(dto.getOrderId());
                orderExpressDto.setExpressId(dto.getExpressId());
                orderExpressDto.setExpressFee(dto.getExpressFee());
                orderExpressDto.setDeliverDate(dto.getDeliverTime().substring(0,10));
                orderExpressDto.setDeliverTime(dto.getDeliverTime());
                orderExpressDto.setGoodsId(expressDto.getGoodsId());
                orderExpressDto.setGoodsNum(expressDto.getGoodsNum());
                orderExpressDto.setInsertTime(CommonUtil.getSeverTime());
                dtoList.add(orderExpressDto);
            });
        });
        manOrderDao.replaceExpress(dtoList);
        // 判断订单商品中的是否发货完全 // 完全发货后
        List<DeliverVo> deliverVos = manOrderDao.queryDeliver(orderId);
        boolean isPart = false;
        for (DeliverVo deliverVo : deliverVos){
            int buyNum = Integer.parseInt(deliverVo.getBuyNum());
            int deliverNum = Integer.parseInt(deliverVo.getDeliverNum());
            if(buyNum > deliverNum){
                isPart = true;
                break;
            }
        }
        if(isPart){
            // 将订单更新为部分发货
            manOrderDao.updateOrderDeliver(orderId,"2");
        }else {
            ManOrderVo orderVo = manOrderDao.queryByOrderId(orderId);
            if(!"3".equals(orderVo.getOrderStatus())){
                // 将订单更新为全部发货
                String deliverTime = orderDao.queryLstDeliver(orderId);
                manOrderDao.updateOrderAllDeliver(orderId,"3",
                        CommonUtil.getSeverTime(),CommonUtil.getConfirmReceiptTime(deliverTime));
            }
        }
        return Result.resultInstance().success();
    }

    @Override
    public Result queryExpress(String orderId) {
        List<OrderExpressVo> expressVoList = manOrderDao.queryExpress(orderId);
        List<OrderExpressDetailVo> list = manOrderDao.queryExpressDetail(orderId);
        expressVoList.forEach(orderExpressVo -> {
            orderExpressVo.setList(list.stream().filter(orderExpressDetailVo ->
                    orderExpressVo.getExpressNumber().equals(
                            orderExpressDetailVo.getExpressNumber())).collect(Collectors.toList()));
        });
        return Result.resultInstance().success(expressVoList);
    }

    @Override
    @SneakyThrows
    public Result export(ManOrderDto dto, HttpServletResponse response, Page<Integer> page) {
        List<ManOrderExportVo> orderVoList;
        if (!StringUtil.isNotEmpty(dto.getIsAll()) || !"1".equals(dto.getIsAll())) {
            PageHelper.startPage(page.getPageNum(), page.getPageSize());
        }
        orderVoList = manOrderDao.queryExportList(dto);
        List<ExpressVo> expressVoList = manOrderDao.queryExpressList();
        List<GoodsInfoVo> goodsInfoVoList = manOrderDao.queryGoodsInfoList(dto.getExportStatus());
        orderVoList.forEach(order->{
            order.setConsigneeAddress(order.getConsigneeAddress().replace(",",""));
            List<GoodsInfoVo> goodsVoList = goodsInfoVoList.stream()
                    .filter(goodsInfoVo -> goodsInfoVo.getOrderId()
                            .equals(order.getOrderId())).collect(Collectors.toList());
            order.setGoodsInfoVoList(goodsVoList);
            List<String> goodsId = goodsVoList.stream().map(GoodsInfoVo::getGoodsId).collect(Collectors.toList());
            List<ExpressVo> expressList = expressVoList.stream()
                    .filter(expressVo -> expressVo.getOrderId()
                            .equals(order.getOrderId())).collect(Collectors.toList());

            Map<String,ExpressVo> map = expressList.stream().collect(Collectors.toMap(ExpressVo::getGoodsId,Function.identity()));
            List<ExpressVo> finallyList = new ArrayList<>();
            for(String id : goodsId){
                if(ObjectUtils.isNotEmpty(map.get(id))){
                    finallyList.add(map.get(id));
                }
            }
            order.setExpressVoList(finallyList);
        });
        String fileName = "DingDan" + DateUtil.format(new Date(),"yyyyMMddHHmmss") + ".xls";
        MyExcelUtils.exportExcel(orderVoList,null,"订单详情表",ManOrderExportVo.class,fileName,response);
        return Result.resultInstance().success("导出成功");
    }

    @Override
    public Result refund(String orderId, String refundMoney) {
        Long time = System.currentTimeMillis() + 10 * 1000;
        String targetId = "APING_MAN_REFUND_"+orderId;
        try {
            if (!redisLockHelper.lock(targetId, String.valueOf(time))) {
                log.info("【{}】---Redis lock fail 加锁失败", targetId);
                return Result.resultInstance().fail(SysCodeEnum.SYSTEM_BUSY);
            }
            log.info("【{}】----Redis lock suc加锁成功", targetId);
            // 查询订单
            // 订单状态 0-待付款 1-待发货 2-部分发货 3-全部发货
            // 4-已完成 5-取消订单 6-确认收货 （7-部分退款 8-全部退款）针对管理端的状态
            // 1、2、3、6可退款 其余无法无法退款
            OrderVo orderVo = orderDao.queryOrderById(orderId);
            // 订单金额
            BigDecimal orderPrice = new BigDecimal(orderVo.getOrderPrice());
            // 订单已退金额
            BigDecimal refundPrice = new BigDecimal(orderVo.getRefundPrice());
            Result result = Result.resultInstance().success();
            // 退款金额
            BigDecimal refund = new BigDecimal(refundMoney);
            // 退款金额比订单金额大
            if (refund.compareTo(orderPrice) > 0){
                result = Result.resultInstance().fail(SysCodeEnum.REFUND_MONEY_TO_BIG);
                return result;
            }
            // 已退款金额加上要退款金额比订单金额大
            if((refundPrice.add(refund)).compareTo(orderPrice) > 0){
                result = Result.resultInstance().fail(SysCodeEnum.ALREADY_ADD_REFUND);
                return result;
            }
            // 已退款金额等于订单金额
            if(orderPrice.compareTo(refundPrice) == 0){
                result = Result.resultInstance().fail(SysCodeEnum.REFUND_EQUAL_ORDER);
                return result;
            }
            // todo 状态 1、2、3、6、7 可退 0、4、5、8不可退款
            String orderStatus = orderVo.getOrderStatus();
            if ("0".equals(orderStatus) || "4".equals(orderStatus)
                    || "5".equals(orderStatus) || "8".equals(orderStatus)) {
                // 满足上列状态下无法退款
                result = Result.resultInstance().fail(SysCodeEnum.ORDER_STATUS_NO_REFUND);
                return result;
            }
            // 查询订单是否有余额支付流水
            AmountVo amountVo = manAmountDao.queryByParam(orderId, "2", "0");
            // 查询订单是否有成功支付的微信支付流水
            WeChatVo weChatVo = manWeChatDao.queryByParam(orderId, "2", "1");
            // 有微信支付流水和余额支付流水
            if(ObjectUtils.isNotEmpty(weChatVo) && ObjectUtils.isNotEmpty(amountVo)){
                // 有微信支付流水和余额支付流水 优先退微信支付
                BigDecimal wxPayPrice = new BigDecimal(weChatVo.getWechatAmount());
                BigDecimal amPayPrice = new BigDecimal(amountVo.getSpentAmount());
                BigDecimal alreadyWxRefundMoney = new BigDecimal(
                        manWeChatDao.queryRefundByParam(orderId));
                BigDecimal alreadyAmRefundMoney = new BigDecimal(
                        manAmountDao.queryRefundByParam(orderId));
                BigDecimal restWxMoney = wxPayPrice.subtract(alreadyWxRefundMoney);
                BigDecimal restAmMoney = amPayPrice.subtract(alreadyAmRefundMoney);
                // 剩余总的能够退款金额
                BigDecimal restTotalMoney = restWxMoney.add(restAmMoney);
                if(refund.compareTo(restTotalMoney) > 0){
                    // 退款金额比剩余能够退款的金额要大
                    result = Result.resultInstance().fail(SysCodeEnum.CAN_REFUND_NO_ENOUGH);
                    return result;
                }
                if(restWxMoney.compareTo(refund) >= 0){
                    // 退款金额小于微信剩余退款金额
                    result = weChatPayService.wxPayRefund(orderId,
                            refund, "1", refundMoney, "2");
                }
                if (refund.compareTo(restWxMoney) > 0 && restTotalMoney.compareTo(refund) >= 0) {
                    // 退款金额比微信剩余退款金额要大比总的剩余退款金额要小
                    if(restWxMoney.compareTo(new BigDecimal("0.00")) == 0){
                        // 退余额
                        amountService.amRefund(orderVo,orderPrice,refundPrice,refund);
                    }else {
                        // 退微信和余额 处理实际退款金额
                        result = weChatPayService.wxPayRefund(orderId,
                                restWxMoney, "1", refundMoney, "2");
                    }
                }
            } else if (ObjectUtils.isNotEmpty(weChatVo) && ObjectUtils.isEmpty(amountVo)) {
                // 只有微信支付流水
                BigDecimal wxPayPrice = new BigDecimal(weChatVo.getWechatAmount());
                BigDecimal alreadyWxRefundMoney = new BigDecimal(
                        manWeChatDao.queryRefundByParam(orderId));
                BigDecimal restWxMoney = wxPayPrice.subtract(alreadyWxRefundMoney);
                if(restWxMoney.compareTo(refund) >= 0){
                    // 退微信 实际金额与退款金额相等
                    result = weChatPayService.wxPayRefund(orderId,
                            refund, "1", refundMoney, "2");
                }else {
                    // 无法退
                    result = Result.resultInstance().fail(SysCodeEnum.CAN_REFUND_NO_ENOUGH);
                    return result;
                }
            } else if (ObjectUtils.isEmpty(weChatVo) && ObjectUtils.isNotEmpty(amountVo)) {
                BigDecimal amPayPrice = new BigDecimal(amountVo.getSpentAmount());
                BigDecimal alreadyAmRefundMoney = new BigDecimal(
                        manAmountDao.queryRefundByParam(orderId));
                BigDecimal restAmMoney = amPayPrice.subtract(alreadyAmRefundMoney);
                if(restAmMoney.compareTo(refund) >= 0){
                    // 余额能退
                    amountService.amRefund(orderVo,orderPrice,refundPrice,refund);
                }else {
                    // 无法退
                    result = Result.resultInstance().fail(SysCodeEnum.CAN_REFUND_NO_ENOUGH);
                    return result;
                }
            }else {
                // 没有任何支付流水
                result = Result.resultInstance().fail(SysCodeEnum.CAN_REFUND_NO_ENOUGH);
                return result;
            }
            return result;
        }catch (Exception e){
            e.printStackTrace();
            log.error(">>>>>>管理端退款失败<<<<<<");
            return Result.resultInstance().fail(SysCodeEnum.MAN_REFUND_FAIL);
        }finally {
            redisLockHelper.unlock(targetId, String.valueOf(time));
            log.info("【{}】----Redis lock suc解锁成功", targetId);
        }
    }

}
