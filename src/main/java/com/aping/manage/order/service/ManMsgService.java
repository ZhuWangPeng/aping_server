package com.aping.manage.order.service;

import com.aping.common.model.Result;
import com.aping.manage.order.dto.MsgDto;
import com.github.pagehelper.Page;

public interface ManMsgService {
    Result queryList(MsgDto dto, Page<Integer> page);
}
