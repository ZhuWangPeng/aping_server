package com.aping.manage.order.service;

import com.aping.common.model.Result;
import com.aping.manage.order.dto.ExpressDto;
import com.aping.manage.order.dto.ManOrderDto;
import com.aping.manage.order.dto.ManOrderExpressDto;
import com.aping.manage.order.vo.ExpressVo;
import com.github.pagehelper.Page;
import org.springframework.validation.annotation.Validated;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface ManOrderService {
    Result queryList(ManOrderDto dto, Page<Integer> page);

    Result updateExpress(List<ManOrderExpressDto> list);

    Result queryExpress(String orderId);

    Result export(ManOrderDto dto, HttpServletResponse response, Page<Integer> page);

    Result refund(String orderId,String refundMoney);

}
