package com.aping.manage.order.service.impl;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.IdUtil;
import com.aping.manage.order.dao.ManExpressDao;
import com.aping.manage.order.dto.ExpressDto;
import com.aping.manage.order.service.ManExpressService;
import com.aping.manage.order.vo.ManExpressVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class ManExpressServiceImpl implements ManExpressService {

    @Resource
    private ManExpressDao manExpressDao;

    @Override
    public Result queryList() {
        List<ManExpressVo> list = manExpressDao.queryList(null);
        return Result.resultInstance().success(list);
    }

    @Override
    public Result query(ExpressDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<ManExpressVo> list = manExpressDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(list));
    }

    @Override
    public Result add(ExpressDto dto) {
        dto.setExpressId("EP_"+ IdUtil.getSpecLengthId(5));
        manExpressDao.add(dto);
        return Result.resultInstance().success();
    }

    @Override
    public Result update(ExpressDto dto) {
        manExpressDao.update(dto);
        return Result.resultInstance().success();
    }

    @Override
    public Result del(String expressId) {
        manExpressDao.del(expressId);
        return Result.resultInstance().success();
    }

}
