package com.aping.manage.order.controller;

import com.aping.common.model.Result;
import com.aping.manage.order.dto.MsgDto;
import com.aping.manage.order.service.ManMsgService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/msg")
@Api(tags = "消息通知")
@Slf4j
public class MessageController {

    @Resource
    private ManMsgService manMsgService;

    @ApiOperation(value = "查询订单")
    @GetMapping("/queryList")
    public Result queryList(MsgDto dto, Page<Integer> page){
        return manMsgService.queryList(dto,page);
    }


}
