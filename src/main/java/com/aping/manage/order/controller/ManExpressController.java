package com.aping.manage.order.controller;

import com.aping.common.model.Result;
import com.aping.manage.order.dto.ExpressDto;
import com.aping.manage.order.dto.ManOrderDto;
import com.aping.manage.order.service.ManExpressService;
import com.aping.manage.order.service.ManOrderService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/express")
@Api(tags = "快递")
@Slf4j
public class ManExpressController {

    @Resource
    private ManExpressService manExpressService;

    @ApiOperation(value = "查询下拉列表")
    @GetMapping("/queryList")
    public Result queryList(){
        return manExpressService.queryList();
    }

    @ApiOperation(value = "查询列表")
    @GetMapping("/query")
    public Result query(ExpressDto dto, Page<Integer> page){
        return manExpressService.query(dto,page);
    }

    @ApiOperation(value = "新增")
    @GetMapping("/add")
    public Result add(ExpressDto dto){
        return manExpressService.add(dto);
    }

    @ApiOperation(value = "修改")
    @GetMapping("/update")
    public Result update(ExpressDto dto){
        return manExpressService.update(dto);
    }

    @ApiOperation(value = "删除")
    @GetMapping("/del")
    public Result del(@RequestParam("expressId") String expressId){
        return manExpressService.del(expressId);
    }

}
