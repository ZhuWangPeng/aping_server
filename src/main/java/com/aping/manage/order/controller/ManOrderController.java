package com.aping.manage.order.controller;

import com.aping.common.model.Result;
import com.aping.manage.goods.dto.GoodsDto;
import com.aping.manage.goods.valid.GoodsUpdate;
import com.aping.manage.order.dto.ExpressDto;
import com.aping.manage.order.dto.ManOrderDto;
import com.aping.manage.order.dto.ManOrderExpressDto;
import com.aping.manage.order.service.ManOrderService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/order")
@Api(tags = "订单")
@Slf4j
public class ManOrderController {

    @Resource
    private ManOrderService manOrderService;

    @ApiOperation(value = "查询订单")
    @GetMapping("/queryList")
    public Result queryList(ManOrderDto dto, Page<Integer> page){
        return manOrderService.queryList(dto,page);
    }

    @ApiOperation("查询快递列表")
    @GetMapping("/queryExpress")
    public Result queryExpress(@RequestParam(value = "orderId") String orderId){
        return manOrderService.queryExpress(orderId);
    }

    @ApiOperation("编辑快递信息")
    @PostMapping("/updateExpress")
    public Result updateExpress(@RequestBody List<ManOrderExpressDto> list){
        return manOrderService.updateExpress(list);
    }

    @ApiOperation(value = "导出订单")
    @GetMapping("/export")
    public Result export(@Validated ManOrderDto dto, HttpServletResponse response, Page<Integer> page){
        return manOrderService.export(dto,response,page);
    }

    @ApiOperation(value = "退款")
    @GetMapping("/refund")
    public Result refund(@RequestParam("orderId") String orderId,@RequestParam("refundMoney") String refundMoney){
        return manOrderService.refund(orderId,refundMoney);
    }
}
