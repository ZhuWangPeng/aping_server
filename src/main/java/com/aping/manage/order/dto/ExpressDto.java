package com.aping.manage.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ExpressDto {
    @ApiModelProperty("快递公司id")
    private String expressId;
    @ApiModelProperty("快递公司编码")
    private String expressCode;
    @ApiModelProperty("快递公司名称")
    private String expressName;
}
