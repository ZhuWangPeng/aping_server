package com.aping.manage.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
public class ManOrderExpressDto {
    @ApiModelProperty("快递单号")
    @NotBlank(message = "快递单号不可为空")
    private String expressNumber;
    @ApiModelProperty("订单编号")
    @NotBlank(message = "订单编号不可为空")
    private String orderId;
    @ApiModelProperty("快递公司id")
    @NotBlank(message = "快递公司id不可为空")
    private String expressId;
    @ApiModelProperty("快递费用")
    @NotBlank(message = "快递费用不可为空")
    private String expressFee;
    @ApiModelProperty("发货日期")
    private String deliverDate;
    @NotBlank(message = "发货时间不可为空")
    @ApiModelProperty("发货时间")
    private String deliverTime;
    @ApiModelProperty("写入时间")
    private String insertTime;
    @NotEmpty(message = "商品详情列表不可为空")
    List<OrderExpressDetailDto> list;
}
