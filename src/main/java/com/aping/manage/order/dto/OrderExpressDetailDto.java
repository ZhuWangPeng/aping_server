package com.aping.manage.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderExpressDetailDto {
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品数量")
    private int goodsNum;
}
