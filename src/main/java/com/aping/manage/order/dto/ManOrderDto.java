package com.aping.manage.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ManOrderDto {
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("用户标识")
    private String openId;
    @ApiModelProperty("订单价格")
    private String orderPrice;
    @ApiModelProperty("订单状态 0-待付款 1-待发货 2-部分发货 3-全部发货 4-已完成 5-取消订单")
    private String orderStatus;
    @ApiModelProperty("地址id")
    private String addressId;
    @ApiModelProperty("写入日期")
    private String insertDateStart;
    @ApiModelProperty("写入日期")
    private String insertDateEnd;
    @ApiModelProperty("是否查询全部 1-是")
    private String isAll;
    @ApiModelProperty("订单状态 1-已发货 2-未发货")
    private String exportStatus;

}
