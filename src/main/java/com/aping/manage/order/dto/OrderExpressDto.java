package com.aping.manage.order.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderExpressDto {
    @ApiModelProperty("快递单号")
    private String expressNumber;
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("快递公司id")
    private String expressId;
    @ApiModelProperty("快递费用")
    private String expressFee;
    @ApiModelProperty("发货日期")
    private String deliverDate;
    @ApiModelProperty("发货时间")
    private String deliverTime;
    @ApiModelProperty("商品id")
    private String goodsId;
    @ApiModelProperty("商品数量")
    private int goodsNum;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
