package com.aping.manage.order.dao;

import com.aping.manage.order.dto.ExpressDto;
import com.aping.manage.order.dto.ManOrderDto;
import com.aping.manage.order.dto.ManOrderExpressDto;
import com.aping.manage.order.dto.OrderExpressDto;
import com.aping.manage.order.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ManOrderDao {
    List<ManOrderVo> queryList(ManOrderDto dto);

    List<ManOrderDetailVo> queryDetailList();

    List<ManAddressVo> queryAddressList();

    void replaceExpress(@Param("list") List<OrderExpressDto> dtoList);

    List<ManOrderExportVo> queryExportByPageNumList(ManOrderDto dto);

    // 查询导出的订单数据
    List<ManOrderExportVo> queryExportList(ManOrderDto dto);

    //查询快递信息
    List<ExpressVo> queryExpressList();
    // 查询商品信息
    List<GoodsInfoVo> queryGoodsInfoList(@Param("exportStatus") String exportStatus);

    List<OrderExpressVo> queryExpress(String orderId);
    List<OrderExpressDetailVo> queryExpressDetail(String orderId);

    List<OrderGoodsVo> queryOrderGoodsList(String orderId);

    List<DeliverVo> queryDeliver(@Param("orderId") String orderId);

    void updateOrderDeliver(@Param("orderId") String orderId,
                            @Param("orderStatus") String orderStatus);

    void updateOrderAllDeliver(@Param("orderId") String orderId,
                               @Param("orderStatus") String orderStatus,
                               @Param("allDeliverTime") String allDeliverTime,
                               @Param("confirmReceiptTime") String confirmReceiptTime);

    ManOrderVo queryByOrderId(@Param("orderId") String orderId);

}
