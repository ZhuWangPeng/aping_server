package com.aping.manage.order.dao;

import com.aping.manage.order.dto.ExpressDto;
import com.aping.manage.order.vo.ManExpressVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ManExpressDao {
    List<ManExpressVo> queryList(ExpressDto dto);
    void add(ExpressDto dto);
    void update(ExpressDto dto);
    void del(@Param("expressId") String expressId);
}
