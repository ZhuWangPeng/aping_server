package com.aping.manage.order.dao;

import com.aping.manage.order.dto.MsgDto;
import com.aping.manage.order.vo.MsgVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ManMsgDao {

    List<MsgVo> queryList(MsgDto dto);
}
