package com.aping.manage.order.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderExpressDetailVo {
    @JsonIgnore
    private String expressNumber;
    @ApiModelProperty("商品id")
    private String goodsId;
    private String goodsName;
    @ApiModelProperty("商品数量")
    private int goodsNum;
}
