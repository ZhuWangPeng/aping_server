package com.aping.manage.order.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ManAddressVo {
    @ApiModelProperty("地址id")
    private String addressId;
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("收货人姓名")
    private String consigneeName;
    @ApiModelProperty("收货人联系方式")
    private String consigneeTel;
    @ApiModelProperty("地区id 逗号隔开")
    private String areaId;
    @ApiModelProperty("地区 逗号隔开")
    private String consigneeArea;
    @ApiModelProperty("收货地址")
    private String consigneeAddress;
    @JsonIgnore
    @ApiModelProperty("是否为默认地址")
    private String isDefault;
}
