package com.aping.manage.order.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class GoodsInfoVo {
    @ApiModelProperty("订单编号")
    @ExcelIgnore
    private String orderId;
    @ApiModelProperty("商品编号")
    @Excel(name = "商品编号", width = 20)
    private String goodsId;
    @ApiModelProperty("商品名称")
    @Excel(name = "商品名称", width = 20)
    private String goodsName;
    @ApiModelProperty("进货价")
    @Excel(name = "进货价", width = 20)
    private String goodsCost;
    @ApiModelProperty("商品数量")
    @Excel(name = "商品数量", width = 20)
    private String goodsNum;
}
