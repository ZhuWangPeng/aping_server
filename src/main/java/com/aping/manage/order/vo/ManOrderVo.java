package com.aping.manage.order.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ManOrderVo {
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("用户标识")
    @ExcelIgnore
    private String openId;
    @ApiModelProperty("用户微信名")
    private String wechatName;
    @ApiModelProperty("订单价格")
    private String orderPrice;
    @ApiModelProperty("退款金额")
    private String refundPrice;
    @ApiModelProperty("订单状态 0-待付款 1-待发货 2-部分发货 3-待发货 4-已完成")
    private String orderStatus;
    @ApiModelProperty("订单备注")
    private String orderMemo;
    @ApiModelProperty("地址id")
    private String addressId;
//    @ApiModelProperty("写入日期")
//    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
//    @ApiModelProperty("订单过期时间")
//    private String expireTime;
    @ApiModelProperty("支付时间")
    private String wxPayTime;
    @ApiModelProperty("支付金额")
    private String wxPayPrice;
    @ApiModelProperty("支付时间")
    private String amPayTime;
    @ApiModelProperty("支付金额")
    private String amPayPrice;
    @ApiModelProperty("订单详情列表")
    private List<ManOrderDetailVo> OrderDetailVoList;
    private ManAddressVo addressVo;
}
