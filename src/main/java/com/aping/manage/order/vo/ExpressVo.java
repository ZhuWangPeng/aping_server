package com.aping.manage.order.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import com.aping.mob.order.vo.OrderDetailVo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ExpressVo {
    @ApiModelProperty("订单编号")
    @ExcelIgnore
    @JsonIgnore
    private String orderId;
    @ApiModelProperty("商品编号")
    @ExcelIgnore
    @JsonIgnore
    private String goodsId;
    @ApiModelProperty("快递名称")
    @Excel(name = "快递名称", width = 20)
    private String expressName;
    @ApiModelProperty("快递单号")
    @Excel(name = "快递单号", width = 20)
    private String expressNumber;
    @ApiModelProperty("发货日期")
    @ExcelIgnore
    private String deliverDate;
    @ApiModelProperty("发货时间")
    @Excel(name = "发货时间", width = 20)
    private String deliverTime;
    @ApiModelProperty("快递运费")
    @Excel(name = "快递运费", width = 20)
    private String expressFee;
}
