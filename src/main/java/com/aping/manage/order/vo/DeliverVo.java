package com.aping.manage.order.vo;

import lombok.Data;

@Data
public class DeliverVo {
    private String orderId;
    private String goodsId;
    private String deliverNum;
    private String buyNum;
}
