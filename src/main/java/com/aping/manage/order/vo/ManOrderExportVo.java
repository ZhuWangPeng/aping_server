package com.aping.manage.order.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class ManOrderExportVo {
    @ApiModelProperty("订单编号")
    @Excel(name = "订单编号", needMerge = true, width = 20)
    private String orderId;
    @ApiModelProperty("订单创建时间")
    @Excel(name = "订单创建时间", needMerge = true, width = 20)
    private String insertTime;
    @ExcelCollection(name = "商品信息")
    List<GoodsInfoVo> goodsInfoVoList;
    @ApiModelProperty("收货地址")
    @Excel(name = "收货地址", needMerge = true, width = 20)
    private String consigneeAddress;
    @ApiModelProperty("收货人")
    @Excel(name = "收货人", needMerge = true, width = 20)
    private String consigneeName;
    @ApiModelProperty("手机号码")
    @Excel(name = "手机号码", needMerge = true, width = 20)
    private String consigneeTel;
    @ExcelCollection(name = "快递信息")
    List<ExpressVo> expressVoList;
}
