package com.aping.manage.user.controller;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.AdminDto;
import com.aping.manage.user.service.AdminService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/admin")
@Api(tags = "用户管理")
@Slf4j
public class AdminController {

    @Resource
    private AdminService adminService;

    @ApiOperation(value = "查询用户列表")
    @GetMapping("/queryList")
    public Result queryList(AdminDto dto, Page<Integer> page){
        return adminService.queryList(dto,page);
    }


    @ApiOperation(value = "新增管理员")
    @PostMapping("/addAdmin")
    public Result addAdmin(AdminDto dto){
        return null;
    }
}
