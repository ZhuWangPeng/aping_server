package com.aping.manage.user.controller;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.LoginDto;
import com.aping.manage.user.service.LoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/login")
@Api(tags = "登录")
@Slf4j
public class LoginController {

    @Resource
    private LoginService loginService;

    @ApiOperation(value = "获取验证码")
    @GetMapping("/getValidCode")
    public Result getValidCode(String mobile){
        return loginService.getValidCode(mobile);
    }

    @ApiOperation(value = "获取adminToken")
    @GetMapping("/getAdminToken")
    public Result login(LoginDto loginDto){
        return loginService.login(loginDto);
    }
}

