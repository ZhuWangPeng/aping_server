package com.aping.manage.user.controller;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.RecordDto;
import com.aping.manage.user.service.RecordService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/record")
@Api(tags = "操作记录")
@Slf4j
public class RecordController {

    @Resource
    private RecordService recordService;

    @ApiOperation(value = "查询操作记录")
    @GetMapping("/queryList")
    public Result queryList(RecordDto dto, Page<Integer> page){
        return recordService.queryList(dto,page);
    }

}
