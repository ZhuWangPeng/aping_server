package com.aping.manage.user.controller;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.UserDto;
import com.aping.manage.user.service.UserService;
import com.aping.manage.user.valid.Recharge;
import com.aping.manage.user.valid.Withdrawal;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/user")
@Api(tags = "管理端用户查询")
@Slf4j
public class UserController {

    @Resource
    private UserService userService;

    @ApiOperation(value = "查询移动端用户")
    @GetMapping("/queryList")
    public Result queryList(UserDto dto, Page<Integer> page){
        return userService.queryList(dto,page);
    }

//    @ApiOperation(value = "修改余额")
//    @GetMapping("/editMoney")
//    public Result editMoney(UserDto dto){
//        return userService.editMoney(dto);
//    }

    @ApiOperation(value = "拉入黑名单")
    @GetMapping("/inBack")
    public Result inBack(UserDto dto){
        return userService.inBack(dto);
    }

    @ApiOperation(value = "解除黑名单")
    @GetMapping("/outBack")
    public Result outBack(UserDto dto){
        return userService.outBack(dto);
    }

    @ApiOperation(value = "账户充值")
    @GetMapping("/recharge")
    public Result recharge(@Validated(value = Recharge.class) UserDto dto){
        return userService.recharge(dto);
    }

    @ApiOperation(value = "账户提现")
    @GetMapping("/withdrawal")
    public Result withdrawal(@Validated(value = Withdrawal.class) UserDto dto){
        return userService.withdrawal(dto);
    }

}
