package com.aping.manage.user.service;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.AdminDto;
import com.github.pagehelper.Page;

public interface AdminService {

    Result queryList(AdminDto dto, Page<Integer> page);

}
