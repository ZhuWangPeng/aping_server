package com.aping.manage.user.service.impl;

import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.manage.user.dao.RecordDao;
import com.aping.manage.user.dto.RecordDto;
import com.aping.manage.user.service.RecordService;
import com.aping.manage.user.vo.RecordVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class RecordServiceImpl implements RecordService {

    @Resource
    private RecordDao recordDao;

    @Override
    public Result queryList(RecordDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<RecordVo> recordVoList = recordDao.queryList(dto);
        recordVoList.forEach(recordVo -> {
            String content;
            switch (recordVo.getOperateMenu()){
                case "/user/recharge":
                    content = "管理员【"+recordVo.getAdminName()+"】("+recordVo.getAdminId()+")给";
                    content = content + "【"+ recordVo.getWechatName()+"】("+recordVo.getOpenId()+")的账户充值了【";
                    content = content + recordVo.getOperateContent() +"】元";
                    recordVo.setOperateContent(content);
                    break;
                case "/user/withdrawal":
                    content = "管理员【"+recordVo.getAdminName()+"】("+recordVo.getAdminId()+")将";
                    content = content + "【"+ recordVo.getWechatName()+"】("+recordVo.getOpenId()+")的账户提现了【";
                    content = content + recordVo.getOperateContent() +"】元";
                    recordVo.setOperateContent(content);
                    break;
                case "/user/inBack":
                    content = "管理员【"+recordVo.getAdminName()+"】("+recordVo.getAdminId()+")将";
                    content = content + "【"+ recordVo.getWechatName()+"】("+recordVo.getOpenId()+")拉入黑名单";
                    recordVo.setOperateContent(content);
                    break;
                case "/user/outBack":
                    content = "管理员【"+recordVo.getAdminName()+"】("+recordVo.getAdminId()+")将";
                    content = content + "【"+ recordVo.getWechatName()+"】("+recordVo.getOpenId()+")解除黑名单";
                    recordVo.setOperateContent(content);
                    break;
            }
            recordVo.setOperateMenu(SysConstant.operateMenuMap.get(recordVo.getOperateMenu()));
        });
        return Result.resultInstance().success(new PageInfo<>(recordVoList));
    }
}
