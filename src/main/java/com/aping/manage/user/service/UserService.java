package com.aping.manage.user.service;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.UserDto;
import com.github.pagehelper.Page;

public interface UserService {

    Result queryList(UserDto dto, Page<Integer> page);

    Result editMoney(UserDto dto);

    Result inBack(UserDto dto);

    Result outBack(UserDto dto);
    Result recharge(UserDto dto);
    Result withdrawal(UserDto dto);

}
