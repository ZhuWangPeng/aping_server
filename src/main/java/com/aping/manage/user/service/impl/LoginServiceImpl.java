package com.aping.manage.user.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.*;
import com.aping.manage.user.dao.LoginDao;
import com.aping.manage.user.dto.LoginDto;
import com.aping.manage.user.service.LoginService;
import com.aping.manage.user.vo.AdminFuncVo;
import com.aping.manage.user.vo.AdminInfoVo;
import com.aping.sms.SendSms;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Resource
    private LoginDao loginDao;
    @Resource
    private RedisHandle redisHandle;
    @Resource
    private SendSms sendSms;

    @Value("${aping.secret.man-private-key}")
    private String privateKey;
    @Value("${aping.secret.man-public-key}")
    private String publicKey;
    @Value("${aping.secret.des-admin-token}")
    private String adminTokenSecret;

    private static final Long CODE_EXPIRE_TIME = 5 * 60L; // 1分钟有效期
    private static final Long ADMIN_TOKEN_EXPIRE_TIME = 30 * 60L; // 30分钟
    // 缓存验证码key的前缀
    private static final String CODE_KEY_PREFIX = "APING_ADMIN_MOBILE_";
    // adminTokenKey 前缀
    private static final String ADMIN_TOKEN_KEY_PREFIX = "APING_ADMIN_TOKEN_";

    @Override
    public Result getValidCode(String mobile) {
        try {
            mobile = mobile.replace(" ","+");
            mobile = RSAUtil.decryptData(mobile, privateKey);
            if(StringUtils.isEmpty(loginDao.validAdmin(mobile))){
                return Result.resultInstance().fail(SysCodeEnum.NO_ADMIN_USER);
            }
            // 生成随机验证码
            String key = CODE_KEY_PREFIX + mobile;
            String code = IdUtil.getRandomNum(6);
            // 发送短信
            sendSms.sendSms(mobile,code);
            log.info("mobile:{} - code:{}",mobile,code);
            redisHandle.set(key,code,CODE_EXPIRE_TIME);
//            return Result.resultInstance().success("200","发送成功",RSAUtil.encryptedData(code,publicKey));
            return Result.resultInstance().success("200","发送成功");
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().fail(SysCodeEnum.GET_CODE_ERROR);
        }
    }

    @Override
    public Result login(LoginDto loginDto) {
        try {
            String mobile = loginDto.getMobile().replace(" ","+");
            String code = loginDto.getCode().replace(" ","+");
            mobile = RSAUtil.decryptData(mobile, privateKey);
            code = RSAUtil.decryptData(code, privateKey);
            String key = "APING_ADMIN_MOBILE_" + mobile;
            if(!redisHandle.exists(key)){
                return Result.resultInstance().fail(SysCodeEnum.CODE_DEAD);
            }
            if(!code.equals(redisHandle.get(key))){
                return Result.resultInstance().fail(SysCodeEnum.CODE_ERROR);
            }
            // 菜单列表
            List<AdminFuncVo> funcVoList = loginDao.queryFunction(mobile);
            // 获取adminToken
            AdminInfoVo adminInfoVo = loginDao.queryAdminInfo(mobile);
            String adminId = adminInfoVo.getAdminId();
            Map<String,String> adminTokenMap = new HashMap<>();
            adminTokenMap.put("mobile",mobile);
            adminTokenMap.put("adminId",adminId);
            String adminTokenKey = ADMIN_TOKEN_KEY_PREFIX + mobile;
            Object adminToken =  redisHandle.get(adminTokenKey);
            if(ObjectUtil.isEmpty(adminToken)){
                adminToken = DESUtil.encrypt3DESToHex(JSONObject.toJSONString(adminTokenMap), adminTokenSecret);
                redisHandle.set(adminTokenKey,adminToken, ADMIN_TOKEN_EXPIRE_TIME);
            }else {
                redisHandle.set(adminTokenKey,adminToken, ADMIN_TOKEN_EXPIRE_TIME);
            }
            if(StringUtil.isEmpty(adminInfoVo.getFirstLoginTime())){
                adminInfoVo.setFirstLoginTime(CommonUtil.getSeverTime());
            }
            loginDao.updateAdminInfo(adminInfoVo);
            Map<String,Object> map = new HashMap<>();
            map.put("funcList",funcVoList);
            map.put("adminToken",adminToken);
            return Result.resultInstance().success(map);
        }catch (Exception e){
            log.error("登录异常:{}",e.getMessage());
            return Result.resultInstance().fail(SysCodeEnum.LOGIN_FAILED);
        }
    }

    @Override
    public Result checkAdminToken(String adminToken) {
        String result = DESUtil.decrypt3DESToHex(adminToken, adminTokenSecret);
        Map map = JSONObject.parseObject(result, HashMap.class);
        String mobile = (String) map.get("mobile");
        String adminTokenKey = ADMIN_TOKEN_KEY_PREFIX + mobile;
        String checkToken = (String) redisHandle.get(adminTokenKey);
        if(StringUtil.isNotEmpty(checkToken)){
            redisHandle.set(adminTokenKey,adminToken,ADMIN_TOKEN_EXPIRE_TIME);
            log.info(">>>>>adminToken验证成功<<<<<");
            return Result.resultInstance().success();
        }else {
            return Result.resultInstance().fail(SysCodeEnum.ADMIN_TOKEN_EXPIRE);
        }
    }

    public String getAdminId() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            return null;
        }
        String adminToken =  attributes.getRequest().getHeader("adminToken") == null ?
                attributes.getRequest().getParameter("adminToken"):attributes.getRequest().getHeader("adminToken");
        if(StringUtil.isEmpty(adminToken)){
            return "adminToken为空";
        }
        Map map =  JSONObject.parseObject(DESUtil.decrypt3DESToHex(adminToken,adminTokenSecret), HashMap.class);
        return map.get("adminId").toString();
    }
}
