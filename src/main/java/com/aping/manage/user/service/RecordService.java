package com.aping.manage.user.service;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.RecordDto;
import com.github.pagehelper.Page;

public interface RecordService {
    Result queryList(RecordDto dto, Page<Integer> page);
}
