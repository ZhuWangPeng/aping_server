package com.aping.manage.user.service.impl;

import com.aping.common.model.Result;
import com.aping.manage.user.dao.AdminDao;
import com.aping.manage.user.dto.AdminDto;
import com.aping.manage.user.service.AdminService;
import com.aping.manage.user.vo.AdminInfoVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class AdminServiceImpl implements AdminService {

    @Resource
    private AdminDao adminDao;

    @Override
    public Result queryList(AdminDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<AdminInfoVo> adminInfoVoList = adminDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(adminInfoVoList));
    }
}
