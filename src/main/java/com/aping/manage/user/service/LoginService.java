package com.aping.manage.user.service;

import com.aping.common.model.Result;
import com.aping.manage.user.dto.LoginDto;

public interface LoginService {
    Result getValidCode(String mobile);

    Result login(LoginDto loginDto);

    Result checkAdminToken(String adminToken);

    String getAdminId();
}
