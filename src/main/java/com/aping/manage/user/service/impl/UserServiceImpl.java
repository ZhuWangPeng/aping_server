package com.aping.manage.user.service.impl;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.manage.flow.dao.ManAmountDao;
import com.aping.manage.flow.dao.ManRechargeDao;
import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.dto.RechargeDto;
import com.aping.manage.flow.service.ManAmountService;
import com.aping.manage.user.dao.RecordDao;
import com.aping.manage.user.dao.UserDao;
import com.aping.manage.user.dto.RecordDto;
import com.aping.manage.user.dto.UserDto;
import com.aping.manage.user.service.LoginService;
import com.aping.manage.user.service.UserService;
import com.aping.manage.user.vo.UserVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;
    @Resource
    private RecordDao recordDao;
    @Resource
    private LoginService loginService;
    @Resource
    private ManRechargeDao manRechargeDao;
    @Resource
    private ManAmountService manAmountService;

    @Override
    public Result queryList(UserDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<UserVo> userVoList = userDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(userVoList));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result editMoney(UserDto dto) {
        userDao.updateUserInfo(dto);
        dealOperateRecord(dto,"1");
        return Result.resultInstance().success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result inBack(UserDto dto) {
        dto.setIsBack("1");
        userDao.updateUserInfo(dto);
        dealOperateRecord(dto,"3");
        return Result.resultInstance().success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result outBack(UserDto dto) {
        dto.setIsBack("0");
        userDao.updateUserInfo(dto);
        dealOperateRecord(dto,"4");
        return Result.resultInstance().success();
    }

    @Override
    public Result recharge(UserDto dto) {
        Double accountBalances = Double.valueOf(dto.getAccountBalances());
        Double rechargeAmount = Double.valueOf(dto.getRechargeAmount());
        RechargeDto rechargeDto = new RechargeDto();
        rechargeDto.setFlowId(IdUtil.getUUID());
        rechargeDto.setOpenId(dto.getOpenId());
        rechargeDto.setRechargeAmount(dto.getRechargeAmount());
        rechargeDto.setRechargeStatus("2");
        rechargeDto.setInsertDate(CommonUtil.getSeverDate());
        rechargeDto.setInsertTime(CommonUtil.getSeverTime());
        manRechargeDao.addRecharge(rechargeDto);
        dto.setAccountBalances(String.valueOf(accountBalances + rechargeAmount));
        userDao.editBalances(dto);
        dto.setAccountBalances(dto.getRechargeAmount());
        dealOperateRecord(dto,"1");
        return Result.resultInstance().success();
    }

    @Override
    public Result withdrawal(UserDto dto) {
        Double accountBalances = Double.valueOf(dto.getAccountBalances());
        Double spentAmount = Double.valueOf(dto.getSpentAmount());
        if(spentAmount > accountBalances){
            return Result.resultInstance().success(SysCodeEnum.BALANCES_NO_ENOUGH);
        }
        manAmountService.createAmountFlow(dto.getOpenId(),
                "提现",dto.getSpentAmount(),"2","1");
        dto.setAccountBalances(String.valueOf(accountBalances - spentAmount));
        userDao.editBalances(dto);
        dto.setAccountBalances(dto.getSpentAmount());
        dealOperateRecord(dto,"2");
        return Result.resultInstance().success();
    }


    private void dealOperateRecord(UserDto userDto,String key){
        // 记录修改金额 拉黑 解除黑名单
        RecordDto dto = new RecordDto();
        dto.setFlowId(IdUtil.getSpecLengthId(10));
        dto.setAdminId(loginService.getAdminId());
        dto.setOpenId(userDto.getOpenId());
        if("1".equals(key)){
            dto.setOperateContent(userDto.getAccountBalances());
        }
        dto.setOperateMenu(SysConstant.operateMenuKeyMap.get(key));
        dto.setOperateDate(CommonUtil.getSeverDate());
        dto.setOperateTime(CommonUtil.getSeverTime());
        recordDao.addRecord(dto);
    }
}
