package com.aping.manage.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UserVo {
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("用户微信名")
    private String wechatName;
//    @ApiModelProperty("用户微信id")
//    private String wechatId;
    @ApiModelProperty("邀请人唯一标识")
    private String inviterOpenId;
    @ApiModelProperty("邀请人微信名")
    private String inviterWechatName;
    @ApiModelProperty("账户余额")
    private String accountBalances;
    @ApiModelProperty("是否已加入黑名单 0-否 1-是")
    private String isBack;
    @ApiModelProperty("迄今花费")
    private String costDate;
    @ApiModelProperty("订单数量")
    private String orderNum;
    @ApiModelProperty("邀请人数")
    private String inviterNum;
    @ApiModelProperty("返利")
    private String rebateMoney;
    @ApiModelProperty("用户首次登录时间")
    private String firstLoginTime;
    @ApiModelProperty("用户最近一次登录时间")
    private String lstLoginTime;
}
