package com.aping.manage.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdminInfoVo {
    @ApiModelProperty("管理员id")
    private String adminId;
    @ApiModelProperty("管理员姓名")
    private String adminName;
    @ApiModelProperty("管理员手机号")
    private String adminTel;
    @ApiModelProperty("管理员首次登录时间")
    private String firstLoginTime;
    @ApiModelProperty("管理员最近一次登录时间")
    private String lstLoginTime;
}
