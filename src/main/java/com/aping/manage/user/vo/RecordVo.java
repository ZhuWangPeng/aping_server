package com.aping.manage.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RecordVo {
    @ApiModelProperty("操作记录流水号")
    private String flowId;
    @ApiModelProperty("管理员id")
    private String adminId;
    @ApiModelProperty("管理员姓名")
    private String adminName;
    @ApiModelProperty("被操作用户的openId")
    private String openId;
    @ApiModelProperty("被操作用户的微信名")
    private String wechatName;
    @ApiModelProperty("操作内容")
    private String operateContent;
    @ApiModelProperty("操作菜单")
    private String operateMenu;
//    @ApiModelProperty("操作日期")
//    private String operateDate;
    @ApiModelProperty("操作时间")
    private String operateTime;
}
