package com.aping.manage.user.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class AdminFuncVo {
    @ApiModelProperty("菜单id")
    private String funcId;
    @ApiModelProperty("父菜单id")
    private String parentFuncId;
    @ApiModelProperty("菜单名称")
    private String funcName;
    @ApiModelProperty("菜单路径")
    private String url;
    @ApiModelProperty("是否菜单 0-否 1-是")
    private String isMenu;
}
