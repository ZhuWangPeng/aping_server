package com.aping.manage.user.dao;

import com.aping.manage.user.dto.AdminDto;
import com.aping.manage.user.vo.AdminInfoVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AdminDao {

    List<AdminInfoVo>  queryList(AdminDto dto);

}
