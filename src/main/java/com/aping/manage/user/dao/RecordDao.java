package com.aping.manage.user.dao;

import com.aping.manage.user.dto.RecordDto;
import com.aping.manage.user.vo.RecordVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RecordDao {

    void addRecord(RecordDto dto);

    List<RecordVo> queryList(RecordDto dto);
}
