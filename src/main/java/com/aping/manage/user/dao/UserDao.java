package com.aping.manage.user.dao;

import com.aping.manage.user.dto.UserDto;
import com.aping.manage.user.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDao {

    List<UserVo> queryList(UserDto dto);

    void updateUserInfo(UserDto dto);

    void editBalances(UserDto dto);

    String queryBalance();
}
