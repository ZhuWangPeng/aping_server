package com.aping.manage.user.dao;

import com.aping.manage.user.vo.AdminFuncVo;
import com.aping.manage.user.vo.AdminInfoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LoginDao {
    String validAdmin(@Param("mobile") String mobile);

    List<AdminFuncVo> queryFunction(@Param("mobile") String mobile);

    AdminInfoVo queryAdminInfo(@Param("mobile") String mobile);

    void updateAdminInfo(AdminInfoVo adminInfoVo);
}
