package com.aping.manage.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginDto {
//    @ApiModelProperty("秘钥")
//    @NotBlank(message = "秘钥不能为空")
//    private String secret;
    @ApiModelProperty("验证码")
    @NotBlank(message = "验证码不能为空")
    private String code;
    @NotBlank(message = "手机号不能为空")
    private String mobile;
}
