package com.aping.manage.user.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RecordDto {
    @ApiModelProperty("操作记录流水号")
    private String flowId;
    @ApiModelProperty("管理员id")
    private String adminId;
    @ApiModelProperty("被操作的openId")
    private String openId;
    @ApiModelProperty("操作内容")
    private String operateContent;
    @ApiModelProperty("操作菜单")
    private String operateMenu;
    @ApiModelProperty("操作日期")
    private String operateDate;
    private String operateDateStart;
    private String operateDateEnd;
    @ApiModelProperty("操作时间")
    private String operateTime;
}
