package com.aping.manage.user.dto;

import com.aping.manage.user.valid.Recharge;
import com.aping.manage.user.valid.Withdrawal;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserDto {
    @ApiModelProperty("用户唯一标识")
    @NotBlank(message = "用户唯一标识不能为空",groups = {Recharge.class,Withdrawal.class})
    private String openId;
    @ApiModelProperty("充值金额")
    @NotBlank(message = "充值金额不能为空",groups = {Recharge.class})
    private String rechargeAmount;
    @NotBlank(message = "提现金额不能为空",groups = {Withdrawal.class})
    private String spentAmount;
    private String channelId;
    @ApiModelProperty("用户微信名")
    private String wechatName;
    @ApiModelProperty("用户微信id")
    private String wechatId;
    @ApiModelProperty("邀请人唯一标识")
    private String inviterOpenId;
    @ApiModelProperty("账户余额")
    private String accountBalances;
    @ApiModelProperty("是否已加入黑名单 0-否 1-是")
    private String isBack;
    @ApiModelProperty("用户首次登录日期")
    private String firstLoginDate;
    @ApiModelProperty("用户首次登录时间")
    private String firstLoginTime;
    @ApiModelProperty("用户最后一次登录日期")
    private String lstLoginDate;
    @ApiModelProperty("用户最后一次登录时间")
    private String lstLoginTime;
}
