package com.aping.manage.flow.controller;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.RechargeDto;
import com.aping.manage.flow.service.ManRechargeService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/recharge")
@Api(tags = "充值流水")
@Slf4j
public class ManRechargeController {
    @Resource
    private ManRechargeService rechargeService;

    @ApiOperation(value = "查询储值流水列表")
    @GetMapping("/queryList")
    public Result queryList(RechargeDto dto, Page<Integer> page){
        return rechargeService.queryList(dto,page);
    }
}
