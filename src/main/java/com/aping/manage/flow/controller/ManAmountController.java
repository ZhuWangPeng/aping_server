package com.aping.manage.flow.controller;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.service.ManAmountService;
import com.aping.platform.requestJson.annotation.RequestJson;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/amount")
@Api(tags = "余额支付流水")
@Slf4j
public class ManAmountController {

    @Resource
    private ManAmountService amountService;

    @ApiOperation(value = "查询储值流水列表")
    @GetMapping("/queryList")
    public Result queryList(AmountDto dto, Page<Integer> page){
        return amountService.queryList(dto,page);
    }

}
