package com.aping.manage.flow.controller;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.dto.RebateDto;
import com.aping.manage.flow.service.ManAmountService;
import com.aping.manage.flow.service.ManRebateService;
import com.github.pagehelper.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/rebate")
@Api(tags = "返利流水")
@Slf4j
public class ManRebateController {

    @Resource
    private ManRebateService manRebateService;

    @ApiOperation(value = "查询储值流水列表")
    @GetMapping("/queryList")
    public Result queryList(RebateDto dto, Page<Integer> page){
        return manRebateService.queryList(dto,page);
    }
}
