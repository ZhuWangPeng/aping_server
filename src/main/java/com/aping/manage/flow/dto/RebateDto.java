package com.aping.manage.flow.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
@Data
public class RebateDto {
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("用户标识")
    @NotBlank(message = "openId不可为空")
    private String openId;
    @ApiModelProperty("微信名")
    private String wechatName;
    @ApiModelProperty("微信名")
    private String orderWechatName;
    @ApiModelProperty("下订单的用户标识")
    private String orderOpenId;
    @ApiModelProperty("返利金额")
    private String rebateMoney;
    @ApiModelProperty("返利状态")
    @NotBlank(message = "返利状态不可为空")
    private String rebateStatus;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入日期")
    private String insertDateStart;
    @ApiModelProperty("写入日期")
    private String insertDateEnd;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
