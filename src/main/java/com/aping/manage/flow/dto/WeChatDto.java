package com.aping.manage.flow.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WeChatDto {
    @ApiModelProperty("用户唯一标识")
    private String openId;
    @ApiModelProperty("微信名")
    private String wechatName;
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("微信支付的订单编号")
    private String wechatOrderId;
    @ApiModelProperty("微信支付金额")
    private String wechatAmount;
    @ApiModelProperty("支付状态 1-支付中 2-支付成功")
    private String payStatus;
    @ApiModelProperty("支付种类 1-商品购买订单 2-充值")
    private String payKind;
    @ApiModelProperty("写入日期")
    private String insertDate;
    @ApiModelProperty("写入日期")
    private String insertDateStart;
    @ApiModelProperty("写入日期")
    private String insertDateEnd;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
