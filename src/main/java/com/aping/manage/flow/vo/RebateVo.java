package com.aping.manage.flow.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class RebateVo {
    @ApiModelProperty("订单编号")
    private String orderId;
    @ApiModelProperty("用户标识")
    private String openId;
    @ApiModelProperty("下订单的用户标识")
    private String orderOpenId;
    @ApiModelProperty("下订单的用户微信名")
    private String orderWechatName;
    @ApiModelProperty("返利金额")
    private String rebateMoney;
    @ApiModelProperty("返利状态")
    private String rebateStatus;
    //    @ApiModelProperty("写入日期")
//    private String insertDate;
    @ApiModelProperty("写入时间")
    private String insertTime;
}
