package com.aping.manage.flow.dao;

import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.vo.AmountVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ManAmountDao {

    List<AmountVo> queryList(AmountDto dto);

    void addAmount(AmountDto dto);

    AmountVo queryByParam(@Param("orderId") String orderId,
                          @Param("spentStatus") String spentStatus,
                          @Param("kind") String kind);
    String queryRefundByParam(@Param("orderId") String orderId);
}
