package com.aping.manage.flow.dao;

import com.aping.manage.flow.dto.RechargeDto;
import com.aping.manage.flow.dto.WeChatDto;
import com.aping.manage.flow.vo.RechargeVo;
import com.aping.manage.flow.vo.WeChatVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ManWeChatDao {

    List<WeChatVo> queryList(WeChatDto dto);

    WeChatVo queryByParam(@Param("orderId") String orderId,
                                                @Param("payStatus")  String payStatus,
                                                @Param("payKind")  String payKind);
    String queryRefundByParam(@Param("orderId") String orderId);
}
