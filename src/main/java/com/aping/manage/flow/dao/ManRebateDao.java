package com.aping.manage.flow.dao;

import com.aping.manage.flow.dto.RebateDto;
import com.aping.manage.flow.vo.RebateVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ManRebateDao {
    List<RebateVo> queryList(RebateDto dto);
}
