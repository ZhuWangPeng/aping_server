package com.aping.manage.flow.dao;

import com.aping.manage.flow.dto.RechargeDto;
import com.aping.manage.flow.vo.RechargeVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ManRechargeDao {

    List<RechargeVo> queryList(RechargeDto dto);

    void addRecharge(RechargeDto dto);
}
