package com.aping.manage.flow.service;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.RechargeDto;
import com.github.pagehelper.Page;

public interface ManRechargeService {

    Result queryList(RechargeDto dto, Page<Integer> page);

}
