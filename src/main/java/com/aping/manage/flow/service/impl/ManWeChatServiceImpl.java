package com.aping.manage.flow.service.impl;

import com.aping.common.model.Result;
import com.aping.manage.flow.dao.ManWeChatDao;
import com.aping.manage.flow.dto.WeChatDto;
import com.aping.manage.flow.service.ManWeChatService;
import com.aping.manage.flow.vo.RechargeVo;
import com.aping.manage.flow.vo.WeChatVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class ManWeChatServiceImpl implements ManWeChatService {

    @Resource
    private ManWeChatDao manWeChatDao;

    @Override
    public Result queryList(WeChatDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<WeChatVo> weChatVoList = manWeChatDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(weChatVoList));
    }
}
