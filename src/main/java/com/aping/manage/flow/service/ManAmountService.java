package com.aping.manage.flow.service;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.AmountDto;
import com.github.pagehelper.Page;

public interface ManAmountService {
    Result queryList(AmountDto dto, Page<Integer> page);

    void createAmountFlow(String openId,
                          String orderId,
                          String money,
                          String status,
                          String kind);
}
