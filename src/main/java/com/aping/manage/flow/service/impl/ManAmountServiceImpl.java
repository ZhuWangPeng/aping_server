package com.aping.manage.flow.service.impl;

import com.aping.common.model.Result;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.IdUtil;
import com.aping.manage.flow.dao.ManAmountDao;
import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.service.ManAmountService;
import com.aping.manage.flow.vo.AmountVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

@Service
@Slf4j
public class ManAmountServiceImpl implements ManAmountService {

    @Resource
    private ManAmountDao manAmountDao;


    @Override
    public Result queryList(AmountDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<AmountVo> amountVoList = manAmountDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(amountVoList));
    }

    @Override
    public void createAmountFlow(String openId,
                                 String orderId,
                                 String money,
                                 String status,
                                 String kind){
        AmountDto amountDto = new AmountDto();
        amountDto.setFlowId(IdUtil.getUUID());
        amountDto.setOpenId(openId);
        amountDto.setOrderId(orderId);
        amountDto.setSpentAmount(money);
        amountDto.setSpentStatus(status);
        amountDto.setKind(kind);
        amountDto.setInsertDate(CommonUtil.getSeverDate());
        amountDto.setInsertTime(CommonUtil.getSeverTime());
        manAmountDao.addAmount(amountDto);
    }
}
