package com.aping.manage.flow.service;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.WeChatDto;
import com.github.pagehelper.Page;

public interface ManWeChatService {
    Result queryList(WeChatDto dto, Page<Integer> page);
}
