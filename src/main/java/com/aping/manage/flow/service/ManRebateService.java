package com.aping.manage.flow.service;

import com.aping.common.model.Result;
import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.dto.RebateDto;
import com.github.pagehelper.Page;

public interface ManRebateService {
    Result queryList(RebateDto dto, Page<Integer> page);
}
