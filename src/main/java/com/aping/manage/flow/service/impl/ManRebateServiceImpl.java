package com.aping.manage.flow.service.impl;

import com.aping.common.model.Result;
import com.aping.manage.flow.dao.ManAmountDao;
import com.aping.manage.flow.dao.ManRebateDao;
import com.aping.manage.flow.dto.AmountDto;
import com.aping.manage.flow.dto.RebateDto;
import com.aping.manage.flow.service.ManRebateService;
import com.aping.manage.flow.vo.AmountVo;
import com.aping.manage.flow.vo.RebateVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class ManRebateServiceImpl implements ManRebateService {

    @Resource
    private ManRebateDao manRebateDao;


    @Override
    public Result queryList(RebateDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<RebateVo> RebateVoList = manRebateDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(RebateVoList));
    }
}
