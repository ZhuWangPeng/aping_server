package com.aping.manage.flow.service.impl;

import com.aping.common.model.Result;
import com.aping.manage.flow.dao.ManRechargeDao;
import com.aping.manage.flow.dto.RechargeDto;
import com.aping.manage.flow.service.ManRechargeService;
import com.aping.manage.flow.vo.AmountVo;
import com.aping.manage.flow.vo.RechargeVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
@Slf4j
public class ManRechargeServiceImpl implements ManRechargeService {

    @Resource
    private ManRechargeDao manRechargeDao;

    @Override
    public Result queryList(RechargeDto dto, Page<Integer> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<RechargeVo> rechargeVoList = manRechargeDao.queryList(dto);
        return Result.resultInstance().success(new PageInfo<>(rechargeVoList));
    }
}
