package com.aping.token.aspect;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.model.Result;
import com.aping.common.utils.StringUtil;
import com.aping.token.service.TokenService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
@Slf4j
@Order(1)
public class CheckToken {

    @Resource
    private TokenService tokenService;

    /**
     * 对controller进行拦截
     * 需要进入到controller中
     */
    @Pointcut(value="execution(* com.aping.mob.*.controller.*.*(..))" +
            "&& !execution(* com.aping.mob.mall.controller.MallController.*(..))")
    public void checkUserTokenAspect(){}

    @Around("checkUserTokenAspect()")
    public Result doCheckUserToken(ProceedingJoinPoint joinPoint) throws Throwable {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        assert request != null;
        Map<String, String> paramsMap = convertMap(request.getParameterMap());
        String userToken = StringUtil.isNotEmpty(request.getHeader("userToken")) ? request.getHeader("userToken") : paramsMap.get("userToken");
        if(StringUtil.isNotEmpty(userToken)){
            Result result = tokenService.checkUserToken(userToken);
            if(result.getCode().equals("200")){
                return (Result) joinPoint.proceed();
            }else {
                return result;
            }
        } else {
            return Result.resultInstance().fail(SysCodeEnum.USER_TOKEN_EXPIRE);
        }
    }

    public Map<String, String> convertMap(Map<String, String[]> paramMap){
        Map<String,String> rtnMap = new HashMap<>();
        for(String key: paramMap.keySet()){
            rtnMap.put(key, paramMap.get(key)[0]);
        }
        return rtnMap;
    }

}
