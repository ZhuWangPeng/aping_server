package com.aping.token.controller;

import cn.hutool.json.JSONObject;
import com.aping.common.model.Result;
import com.aping.mob.order.service.OrderService;
import com.aping.token.service.WeChatNotifyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@RestController
@RequestMapping("/wechat")
@Api(tags = "")
@Slf4j
public class WeChatNotifyController {

    @Resource
    private WeChatNotifyService weChatNotifyService;
    @Resource
    private OrderService orderService;


    @ApiOperation(value = "异步接收微信支付通知")
    @PostMapping("/wxNotifyUrl")
    public JSONObject wxNotify(@RequestBody String jsonData, HttpServletRequest request, HttpServletResponse response) {
        long time = System.currentTimeMillis();
        JSONObject jsonObject = weChatNotifyService.wxNotify(jsonData, request, response);
        log.info(">>>>>>异步接收微信支付通知耗时:{}ms<<<<<<",System.currentTimeMillis() - time);
        return jsonObject;
    }

    @ApiOperation(value = "异步接收微信支付通知")
    @GetMapping("/dealVolumeAndStock")
    public Result dealVolumeAndStock(String orderId) {
        orderService.autoFinishOrder();
        return Result.resultInstance().success();
    }

    @ApiOperation(value = "异步接收微信退款通知")
    @PostMapping("/wxRefundNotifyUrl")
    public JSONObject wxRefundNotify(@RequestBody String jsonData, HttpServletRequest request, HttpServletResponse response) {
        long time = System.currentTimeMillis();
        JSONObject jsonObject = weChatNotifyService.wxRefundNotify(jsonData, request, response);
        log.info(">>>>>>异步接收微信退款通知耗时:{}ms<<<<<<",System.currentTimeMillis() - time);
        return jsonObject;
    }

}
