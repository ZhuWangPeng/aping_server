package com.aping.token.controller;

import com.aping.common.model.Result;
import com.aping.mob.user.dto.UserInfoDto;
import com.aping.token.service.TokenService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/token")
@Api(tags = "token")
@Slf4j
public class TokenController {

    @Resource
    private TokenService tokenService;

    @ApiOperation(value = "获取UserToken")
    @PostMapping("/getUserToken")
    public Result getUserToken(@RequestBody UserInfoDto dto){
        return tokenService.getUserToken(dto);
    }

    @ApiOperation(value = "校验UserToken")
    @GetMapping("/checkUserToken")
    public Result checkUserToken(String userToken){
        return tokenService.checkUserToken(userToken);
    }
}
