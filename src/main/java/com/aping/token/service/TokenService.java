package com.aping.token.service;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysCodeEnum;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.common.properties.GetOpenIdProperties;
import com.aping.common.redis.RedisHandle;
import com.aping.common.utils.CommonUtil;
import com.aping.common.utils.DESUtil;
import com.aping.common.utils.StringUtil;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.user.dto.UserInfoDto;
import com.aping.mob.user.vo.UserInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Service
@Slf4j
public class TokenService {

    @Resource
    private RedisHandle redisHandle;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SysParamService sysParamService;
    @Resource
    private RestTemplate restTemplate;
    @Resource
    private GetOpenIdProperties properties;
    @Value("${aping.secret.des-user-token}")
    private String userTokenSecret;

    private static final String USER_TOKEN_KEY_PREFIX = "APING_USER_TOKEN_";


    public Result getUserToken(UserInfoDto dto){
        Map<String,String> paramMap = new HashMap<>();
        paramMap.put("appId",properties.getAppId());
        paramMap.put("secret",properties.getAppSecret());
        paramMap.put("code",dto.getCode());
        paramMap.put("grantType",properties.getGrantType());
        String url = sysParamService.getSysParamRedis("WX_GET_OPENID_URL");
        url = url + "?appid={appId}&secret={secret}&js_code={code}&grant_type={grantType}";
        try{
            String result = restTemplate.getForObject(url,String.class,paramMap);
            Map<String,String> strMap = JSONObject.parseObject(result,Map.class);
            assert strMap != null;
            dto.setOpenId(strMap.get("openid"));
        }catch (Exception e){
            e.printStackTrace();
            return Result.resultInstance().fail(SysCodeEnum.GET_OPENID_ERROR);
        }
        String openId = dto.getOpenId();
//        String openId = "oMMx05BlyurLeCfabTud-wwyZXWo";
        String headUrl = sysParamService.getSysParamRedis("WX_HEAD_IMG");
        String rebatePercentage = sysParamService.getSysParamRedis("REBATE_PERCENTAGE");
        UserInfoVo userInfoVo = userInfoDao.queryByOpenId(openId);
        String isNew = "0";
        if(ObjectUtil.isNotEmpty(userInfoVo)){
            dto.setLstLoginDate(CommonUtil.getSeverDate());
            dto.setLstLoginTime(CommonUtil.getSeverTime());
            userInfoDao.updateUserInfo(dto);
            dto.setWechatName(userInfoVo.getWechatName());
        }else {
            isNew = "1";
            dto.setAccountBalances("5.00");
            dto.setFirstLoginDate(CommonUtil.getSeverDate());
            dto.setFirstLoginTime(CommonUtil.getSeverTime());
            dto.setLstLoginDate(CommonUtil.getSeverDate());
            dto.setLstLoginTime(CommonUtil.getSeverTime());
            dto.setWechatName(getWechatName());
            userInfoDao.insertUserInfo(dto);
        }
        Map<String,String> map = new HashMap<>();
        map.put("openId",openId);
        String userTokenKey = USER_TOKEN_KEY_PREFIX + openId;
        Object userToken =  redisHandle.get(userTokenKey);
        if(ObjectUtil.isEmpty(userToken)){
            userToken = DESUtil.encrypt3DESToHex(JSONObject.toJSONString(map), userTokenSecret);
            redisHandle.set(userTokenKey,userToken,SysConstant.USER_TOKEN_EXPIRE_TIME);
        }
        // 处理访问量
        String currDate = CommonUtil.getSeverDate();
        int todayVisitNum = 0;
        String redisKey = SysConstant.APING_TODAY_VISIT_NUM;
        if(redisHandle.hasMapKey(redisKey, currDate)){
            todayVisitNum = Integer.parseInt(redisHandle.getMapField(redisKey, currDate));
            todayVisitNum = todayVisitNum + 1;
            redisHandle.removeMapField(redisKey, currDate);
        }else {
            todayVisitNum = todayVisitNum + 1;
        }
        redisHandle.addMap(redisKey, currDate,String.valueOf(todayVisitNum));
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("userToken",userToken);
        resultMap.put("rebatePercentage",rebatePercentage);
        resultMap.put("openId",openId);
        resultMap.put("wechatName",dto.getWechatName());
        resultMap.put("headImg",headUrl);
        resultMap.put("isNew",isNew);
        return Result.resultInstance().success(resultMap);
    }

    private String getWechatName(){
        String str = UUID.randomUUID().toString().replace("-", "").substring(0,10);
        return "平锋用户" + str;
    }

    public Result checkUserToken(String userToken){
        try {
            String result = DESUtil.decrypt3DESToHex(userToken, userTokenSecret);
            Map map = JSONObject.parseObject(result, HashMap.class);
            String openId = (String) map.get("openId");
            String userTokenKey = USER_TOKEN_KEY_PREFIX + openId;
            String checkToken = (String) redisHandle.get(userTokenKey);
            if(StringUtil.isNotEmpty(checkToken)){
//                redisHandle.set(openId,userToken,SysConstant.USER_TOKEN_EXPIRE_TIME);
                log.info(">>>>>userToken验证成功<<<<<");
                return Result.resultInstance().success();
            }else {
                return Result.resultInstance().fail(SysCodeEnum.USER_TOKEN_EXPIRE);
            }
        }catch (Exception e){
            log.error(">>>>>userToken校验失败: {}<<<<<",e.getMessage());
            return Result.resultInstance().fail(SysCodeEnum.USER_TOKEN_CHECK_FAIL);
        }
    }
}
