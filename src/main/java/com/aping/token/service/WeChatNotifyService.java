package com.aping.token.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.aping.common.utils.CommonUtil;
import com.aping.manage.sysparam.service.SysParamService;
import com.aping.mob.flow.dao.AmountDao;
import com.aping.mob.flow.dao.RechargeDao;
import com.aping.mob.flow.dao.WeChatDao;
import com.aping.mob.flow.dto.RechargeDto;
import com.aping.mob.flow.dto.WeChatDto;
import com.aping.mob.flow.service.AmountService;
import com.aping.mob.flow.vo.AmountVo;
import com.aping.mob.flow.vo.RechargeVo;
import com.aping.mob.order.dao.OrderDao;
import com.aping.mob.order.service.OrderService;
import com.aping.mob.order.service.RebateService;
import com.aping.mob.order.vo.OrderVo;
import com.aping.mob.user.dao.UserInfoDao;
import com.aping.mob.wechat.dto.WxRefundDto;
import com.aping.mob.wechat.service.WeChatPayService;
import com.aping.mob.wechat.vo.WxRefundVo;
import com.github.binarywang.wxpay.bean.notify.OriginNotifyResponse;
import com.github.binarywang.wxpay.bean.notify.SignatureHeader;
import com.github.binarywang.wxpay.bean.notify.WxPayOrderNotifyV3Result;
import com.github.binarywang.wxpay.bean.notify.WxPayRefundNotifyV3Result;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.github.binarywang.wxpay.service.WxPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Slf4j
public class WeChatNotifyService {

    @Resource
    private WxPayService wxPayService;
    @Resource
    private OrderDao orderDao;
    @Resource
    private WeChatDao weChatDao;
    @Resource
    private RechargeDao rechargeDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SysParamService sysParamService;
    @Resource
    private RebateService rebateService;
    @Resource
    private AmountService amountService;
    @Resource
    private OrderService orderService;
    @Resource
    private WeChatPayService weChatPayService;
    @Resource
    private AmountDao amountDao;

    @Transactional(rollbackFor = Exception.class)
    public JSONObject wxNotify(String jsonData, HttpServletRequest request, HttpServletResponse response){
        JSONObject wxPayResult = new JSONObject();
        Lock lock = new ReentrantLock();
        if (lock.tryLock()) {
            OriginNotifyResponse notifyResponse = JSONUtil.toBean(jsonData, OriginNotifyResponse.class);
            WxPayOrderNotifyV3Result v3Result;
            try {
                v3Result = wxPayService.parseOrderNotifyV3Result(jsonData,getRequestHeader(request));
                //解密后的数据
                WxPayOrderNotifyV3Result.DecryptNotifyResult result = v3Result.getResult();
                // 注意：微信会通知多次，因此需判断此订单
                log.info("异步接收微信支付通知:{}",result.toString());
                String orderId =  result.getOutTradeNo();
                WxPayOrderNotifyV3Result.Amount amount = result.getAmount();
                // 金额
                log.info(">>>>>>微信通知中的金额:{}分<<<<<<",amount.getPayerTotal());
                BigDecimal payPrice = new BigDecimal(amount.getPayerTotal())
                        .divide(new BigDecimal("100"),2, RoundingMode.DOWN);
                log.info(">>>>>>转换后的金额:{}元<<<<<<",payPrice);
                String payTime = CommonUtil.getSuccessTime(result.getSuccessTime());
                String kind = orderId.substring(0,1);
                String transactionId = result.getTransactionId();
                WxPayOrderNotifyV3Result.Payer payer = result.getPayer();
                if("O".equals(kind)) {
                    // 购买商品订单
                    OrderVo orderVo = orderDao.queryOrderById(result.getOutTradeNo());
                    // 0:未支付,1:已支付
                    if ("0".equals(orderVo.getOrderStatus())) {
                        BigDecimal orderPrice = new BigDecimal(orderVo.getOrderPrice());
                        if(payPrice.compareTo(orderPrice) != 0){
                            log.info(">>>>>>[{}]微信支付:{}元<<<<<<",orderId,payPrice);
                            BigDecimal balance = new BigDecimal(orderVo.getOrderPrice()).subtract(payPrice);
                            dealBalances(balance,orderId,payer.getOpenid());
                        }
                        // 更新商户平台订单状态
                        WeChatDto dto = new WeChatDto();
                        dto.setOrderId(orderId);
                        dto.setPayStatus("2");
                        dto.setWechatOrderId(transactionId);
                        // 更新商户平台订单状态
                        orderDao.updateOrderWxPayInfo(orderId, payTime, String.valueOf(payPrice));
                        // 更新微信支付流水
                        weChatDao.updateWxPayFlowByOrderId(dto);
                        // 生成返利流水
                        rebateService.createRebate(payer.getOpenid(),orderId, orderVo.getOrderPrice());
                        log.info(">>>>>>[{}]微信支付购买商品订单处理成功<<<<<<",orderId);
                        // 处理库存与销量
                        orderService.dealSuccessVolumeAndStock(orderId);
                    }
                }else if ("R".equals(kind)){
                    // 充值订单
                    RechargeVo rechargeVo = rechargeDao.queryByFlowId(orderId);
                    // 1:充值中,2:充值成功
                    if("1".equals(rechargeVo.getRechargeStatus())){
                        RechargeDto rechargeDto = new RechargeDto();
                        rechargeDto.setFlowId(orderId);
                        rechargeDto.setRechargeStatus("2");
                        // 更新充值信息
                        rechargeDao.updateRecharge(rechargeDto);
                        // 更新余额
                        BigDecimal balance = new BigDecimal(userInfoDao.getAmount(payer.getOpenid()));
                        BigDecimal price = balance.add(payPrice);
                        userInfoDao.updateUserAmount(payer.getOpenid(), String.valueOf(price));
                        // 更新商户平台订单状态
                        WeChatDto dto = new WeChatDto();
                        dto.setOrderId(orderId);
                        dto.setPayStatus("2");
                        dto.setWechatOrderId(transactionId);
                        // 更新微信支付流水
                        weChatDao.updateWxPayFlowByOrderId(dto);
                        log.info(">>>>>>[{}]微信支付充值订单处理成功<<<<<<",orderId);
                    }
                }
                //通知应答：接收成功：HTTP应答状态码需返回200或204，无需返回应答报文。
                log.info(">>>>>>订单[{}]接收异步支付通知成功<<<<<<",orderId);
                response.setStatus(HttpServletResponse.SC_OK);
                return null;
            } catch (WxPayException e) {
                e.printStackTrace();
                log.error("支付回调失败：{}", e.getLocalizedMessage());
                // 通知应答：HTTP应答状态码需返回5XX或4XX，同时需返回应答报文
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                wxPayResult.putOpt("code", "FAIL");
                wxPayResult.putOpt("message", "失败");
                return wxPayResult;
            } finally {
                lock.unlock();
            }
        }
        // 通知应答码：HTTP应答状态码需返回5XX或4XX，同时需返回应答报文
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        wxPayResult.putOpt("code", "FAIL");
        wxPayResult.putOpt("message", "失败");
        return wxPayResult;
    }

    @Transactional(rollbackFor = Exception.class)
    public JSONObject wxRefundNotify(String jsonData, HttpServletRequest request, HttpServletResponse response) {
        JSONObject wxPayResult = new JSONObject();
        Lock lock = new ReentrantLock();
        if (lock.tryLock()) {
            WxPayRefundNotifyV3Result v3Result;
            try {
                v3Result = wxPayService.parseRefundNotifyV3Result(jsonData, getRequestHeader(request));
                //解密后的数据
                WxPayRefundNotifyV3Result.DecryptNotifyResult refundV3Result = v3Result.getResult();
                String refundStatus = refundV3Result.getRefundStatus();
                String orderId = refundV3Result.getOutTradeNo();
                log.info(">>>>>>[{}]-退款状态为：{}<<<<<<",orderId,refundStatus);
                switch (refundStatus){
                    // 退款关闭
                    case "CLOSED":
                        log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款关闭");
                        // 更新状态
                        break;
                    // 退款成功
                    case "SUCCESS":
                        // 更新退款流水 判断是否有余额流水
                        log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款成功");
                        successRefund(refundV3Result);
                        break;
                    // 退款异常
                    case "ABNORMAL":
                        log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款异常");
                        break;
                    default:
                        log.info(">>>>>>[{}]-退款状态为:{}<<<<<<",orderId,"退款中");
                        break;
                }
                //通知应答：接收成功：HTTP应答状态码需返回200或204，无需返回应答报文。
                log.info(">>>>>>订单[{}]接收异步退款通知成功<<<<<<",orderId);
                response.setStatus(HttpServletResponse.SC_OK);
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                log.error("支付回调失败：{}", e.getLocalizedMessage());
                // 通知应答：HTTP应答状态码需返回5XX或4XX，同时需返回应答报文
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                wxPayResult.putOpt("code", "FAIL");
                wxPayResult.putOpt("message", "失败");
                return wxPayResult;
            } finally {
                lock.unlock();
            }

        }
        // 通知应答码：HTTP应答状态码需返回5XX或4XX，同时需返回应答报文
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        wxPayResult.putOpt("code", "FAIL");
        wxPayResult.putOpt("message", "失败");
        return wxPayResult;
    }

    public void successRefund(WxPayRefundNotifyV3Result.DecryptNotifyResult refundV3Result){
        String outRefundNo = refundV3Result.getOutRefundNo();
        String orderId = refundV3Result.getOutTradeNo();
        WxRefundVo wxRefundVo = weChatDao.queryRefundByRefundNo(outRefundNo);
        if(!"4".equals(wxRefundVo.getRefundStatus())) {
            String refundM = wxRefundVo.getRefundMoney();
            String refundActualM = wxRefundVo.getActualMoney();
            BigDecimal refund = new BigDecimal(refundActualM).
                    subtract(new BigDecimal(refundM));
            OrderVo orderVo = orderDao.queryOrderById(orderId);
            if("2".equals(wxRefundVo.getRefundKind())){
                //  管理端退款 判断实际金额与微信退款金额比较
                if(!refundM.equals(refundActualM)){
                    BigDecimal orderPrice = new BigDecimal(orderVo.getOrderPrice());
                    BigDecimal refundPrice = new BigDecimal(orderVo.getRefundPrice());
                    amountService.amRefund(orderVo,orderPrice,refundPrice,refund);
                }else {
                    log.info(">>>>>[{}]-管理端退款此次无余额退款<<<<<",orderId);
                }
            }else if ("1".equals(wxRefundVo.getRefundKind())) {
                // 移动端取消订单
                if(!refundM.equals(refundActualM)) {
                    // 如果退款金额不等于实际金额 说明余额还要退
                    // 余额将退 actualMoney - refundMoney
                    String openId = orderVo.getOpenId();
                    // 生成余额退款流水
                    amountService.createAmountFlow(openId, orderId, refund, "2");
                    // 增加账户余额
                    BigDecimal accountBalances = new BigDecimal(userInfoDao.getAmount(openId));
                    BigDecimal balance = accountBalances.add(refund);
                    userInfoDao.updateUserAmount(openId, String.valueOf(balance));
                }
                rebateService.cancelRebate(orderId);
                // 释放订单
                orderService.releaseOrder(new ArrayList<>(Collections.singleton(orderId)));
                // 处理销量和库存
                orderService.dealCancelVolumeAndStock(orderId);
            }
            String successTime = CommonUtil.getSuccessTime(
                    refundV3Result.getSuccessTime());
            WxRefundDto dto = new WxRefundDto();
            dto.setOrderId(orderId);
            dto.setRefundId(refundV3Result.getRefundId());
            dto.setTransactionId(refundV3Result.getTransactionId());
            dto.setRefundTime(successTime);
            dto.setRefundStatus("4");
            weChatDao.updateWxRefundFlow(dto);
        }else {
            log.info(">>>>>>[{}]-退款流程已完成<<<<<<",orderId);
        }
    }

    /**
     * 获取回调请求头：签名相关
     *
     * @param request HttpServletRequest
     * @return SignatureHeader
     */
    public SignatureHeader getRequestHeader(HttpServletRequest request) {
        try {
            // 获取通知签名
            String signature = request.getHeader("Wechatpay-Signature");
            // 检查与本地
            String nonce = request.getHeader("Wechatpay-Nonce");
            String serial = request.getHeader("Wechatpay-Serial");
            String timestamp = request.getHeader("Wechatpay-Timestamp");
            SignatureHeader signatureHeader = new SignatureHeader();
            signatureHeader.setSignature(signature);
            signatureHeader.setNonce(nonce);
            signatureHeader.setSerial(serial);
            signatureHeader.setTimeStamp(timestamp);
            return signatureHeader;
        }catch (Exception e){
            e.printStackTrace();
            return new SignatureHeader();
        }
    }

    /**
     * 请求报文：按官方接口示例键值 --- 排序(必须)
     * 官方文档：<a href="https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_5.shtml">...</a>，
     * <a href="https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay4_1.shtml">...</a>
     * 《微信支付API v3签名验证》 中注意：应答主体（response Body），需要按照接口返回的顺序进行验签，错误的顺序将导致验签失败。
     * @param originNotifyResponse OriginNotifyResponse
     * @return String
     */
    private String jsonStrSort(OriginNotifyResponse originNotifyResponse) {
        try {
            Map<String, Object> jsonSort = new LinkedHashMap<>();
            jsonSort.put("id", originNotifyResponse.getId());
            jsonSort.put("create_time", originNotifyResponse.getCreateTime());
            jsonSort.put("resource_type", originNotifyResponse.getResourceType());
            jsonSort.put("event_type", originNotifyResponse.getEventType());
            jsonSort.put("summary", originNotifyResponse.getSummary());
            Map<String, Object> resource = new LinkedHashMap<>();
            resource.put("original_type", originNotifyResponse.getResource().getOriginalType());
            resource.put("algorithm", originNotifyResponse.getResource().getAlgorithm());
            resource.put("ciphertext", originNotifyResponse.getResource().getCiphertext());
            resource.put("associated_data", originNotifyResponse.getResource().getAssociatedData());
            resource.put("nonce", originNotifyResponse.getResource().getNonce());
            jsonSort.put("resource", resource);
            return JSONUtil.toJsonStr(jsonSort);
        }catch (Exception e){
            e.printStackTrace();
            Map<String, Object> jsonSort = new LinkedHashMap<>();
            return JSONUtil.toJsonStr(jsonSort);
        }
    }


    @Transactional(rollbackFor = Exception.class)
    private void dealBalances(BigDecimal accountBalances, String orderId, String openId){
        amountService.createAmountFlow(openId,orderId,accountBalances,"0");
        log.info(">>>>>>[{}]余额支付:{}元<<<<<<",orderId,accountBalances);
        // 更新余额 余额全部扣除
        userInfoDao.updateUserAmount(openId, "0");
        // 更新订单余额支付信息
        orderDao.updateOrderAmPayInfoByCom(orderId,CommonUtil.getSeverTime(),String.valueOf(accountBalances));
    }

}
