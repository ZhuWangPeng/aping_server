package com.aping.common.exception;

public class AccessDeniedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public AccessDeniedException(){
		super();
	}
	
	public AccessDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	public AccessDeniedException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public AccessDeniedException(String message)
	{
		super(message);
	}
	
	public AccessDeniedException(String code,String message)
	{
		super(message);
		this.code = code;
	}
	
	public AccessDeniedException(Throwable cause)
	{
		super(cause);
	}
}
