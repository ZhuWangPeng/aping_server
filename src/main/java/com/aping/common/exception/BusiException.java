package com.aping.common.exception;

/**
 * 业务异常
 */
public class BusiException extends RuntimeException {

    private String code;
    private String message;

    public BusiException(String message) {
        super(message);
        this.message = message;
    }

    public BusiException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public BusiException(String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public BusiException(String code,String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public BusiException(Throwable cause) {
        super(cause);
    }

    public BusiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
