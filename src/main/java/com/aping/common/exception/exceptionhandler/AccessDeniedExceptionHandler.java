package com.aping.common.exception.exceptionhandler;

import com.alibaba.fastjson.JSON;
import com.aping.common.exception.AccessDeniedException;
import com.aping.common.model.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class AccessDeniedExceptionHandler {

	@ExceptionHandler(AccessDeniedException.class)
	@ResponseBody
	public String accessDenied(AccessDeniedException ex){
		return JSON.toJSONString(Result.resultInstance().fail(ex.getCode(), ex.getMessage()));
	}
}
