package com.aping.common.constant;

public enum SysCodeEnum {

    SUCCESS("200","成功"),
    FAIL("-1000","系统异常"),
    PARAM_ERROR("-1001","请求参数不合规"),
    SQL_ERROR("-1002","数据入库异常"),
    SYS_ERROR("1000","系统错误"),
    IMG_TYPE_ERROR("1001","图片类型错误"),
    IMG_UPLOAD_ERROR("1002","图片上传失败"),
    ADD_CART_ERROR("1003","添加购物车失败"),
    USER_TOKEN_EXPIRE("1004","userToken已过期"),
    USER_TOKEN_PARAM_ERROR("1005","userToken不可为空"),
    GOODS_STOCK_ERROR("1006","商品库存不足"),
    WX_REFUND_ERROR("1007","微信退款失败"),
    NO_ADMIN_USER("1008","您不是该系统管理员,请联系超级管理员"),
    GET_CODE_ERROR("1009","获取验证码失败"),
    CANCEL_ORDER_ERROR("1010","取消订单失败"),
    CODE_DEAD("1011","验证码已失效"),
    CODE_ERROR("1012","验证码错误"),
    LOGIN_FAILED("1013","登录失败"),
    ADMIN_TOKEN_EXPIRE("1014","adminToken已过期"),
    CANCEL_ORDER_FAIL("1015","取消订单失败,订单已发货"),
    ADD_ORDER_FAIL("1016","生成订单失败"),
    ADMIN_TOKEN_NULL("1014","adminToken不可为空"),
    USER_TOKEN_CHECK_FAIL("1015","userToken校验失败"),
    ORDER_EXPIRE_CANCEL("1016","订单到时未支付已取消"),
    GOODS_NO_EXISTS("1017","商品不存在"),
    GOODS_DEL("1018","商品已删除"),
    ORDER_EXPORT_ERROR("1019","页码参数错误"),
    SYS_BUSY("1020","系统繁忙,请稍后再试"),
    AMOUNT_PAY_SUCCESS("1021","余额支付订单成功"),
    PORTFOLIO_PAY("1022","组合支付"),
    WECHAT_PAY("1023","微信支付"),
    GOODS_NUM_IS_ZERO("1024","商品数量不可为0"),
    AMOUNT_UPDATE_ERROR("1025","余额支付流水编辑失败"),
    WECHAT_UPDATE_ERROR("1025","微信支付流水编辑失败"),
    RECHARGE_UPDATE_ERROR("1026","充值流水编辑失败"),
    GET_SIGN_ERROR("1027","获取签名失败"),
    BALANCES_NO_ENOUGH("1028","提现失败,余额不足"),
    ACCESS_TOKEN_ERROR("1029","获取accessToken失败"),
    GENERATE_CODE_ERROR("1030","获取小程序码失败"),
    BOTTOM_ERROR("1031","获取小程序码底部图片列表为空"),
    ADMIN_TOKEN_CHECK_ERROR("1032","adminToken校验失败"),
    GET_OPENID_ERROR("1033","获取OPENID失败"),
    JSAPI_ERROR("1034","JSAPI下单失败"),
    NO_RECEIPT("1035","订单未全部发货"),
    SYSTEM_BUSY("1036","系统繁忙,请稍后再试"),
    REFUND_MONEY_TO_BIG("1037","退款金额比订单金额大,无法退款"),
    ORDER_STATUS_NO_REFUND("1038","订单状态不满足退款条件"),
    MAN_REFUND_FAIL("1039","管理端退款失败"),
    NO_QUERY_ORDER("1040","未查询到该订单"),
    GOODS_OFF_THE_SHELF("1041","商品已下架"),
    ALREADY_ADD_REFUND("1042","已退款金额加上要退款金额比订单金额大,无法退款"),
    REFUND_EQUAL_ORDER("1043","已退款全部金额,无法退款"),
    CAN_REFUND_NO_ENOUGH("1044","能够退款金额不足,无法退款");

    private final String code;
    private final String msg;

    SysCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
