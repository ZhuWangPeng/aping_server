package com.aping.common.constant;

import java.util.HashMap;
import java.util.Map;

public class SysConstant {

    public static final int page_size = 10;

    public static final Map<String,String> operateMenuKeyMap = new HashMap<String,String>(){{
        put("1","/user/recharge");
        put("2","/user/withdrawal");
        put("3","/user/inBack");
        put("4","/user/outBack");
    }};

    public static final Map<String,String> operateMenuMap = new HashMap<String,String>(){{
//        put("/index/getStatData","首页统计");
//        put("/images/queryBanner","查询广告");
//        put("/images/upload"," 图片上传");
//        put("/images/del","删除图片");
//        put("/images/queryList","查询图片列表");
//        put("/menu/query","查询商品种类");
//        put("/menu/add","新增商品种类");
//        put("/menu/update","修改商品种类");
//        put("/menu/del","删除商品种类");
//        put("/menu/queryList","获取商品种类下拉列表");
//        put("/goods/query","查询商品列表");
//        put("/goods/add","新增商品");
//        put("/goods/update","修改商品");
//        put("/goods/del","删除商品");
//        put("/goods/queryDetail","查询商品详情");
//        put("/menuOrder/query","查询排序");
//        put("/menuOrder/update","修改排序");
//        put("/order/queryList","查询订单");
//        put("/order/updateExpress","编辑快递信息");
//        put("/order/export","导出订单信息");
//        put("/msg/queryList","查询消息");
//        put("/admin/queryList","查询用户");
//        put("/record/queryList","查询操作记录");
//        put("/sysParam/queryList","查询系统参数列表");
//        put("/sysParam/editSysParam","修改系统参数");
//        put("/sysParam/deleteSysParam","删除系统参数");
        put("/user/editMoney","修改金额");
        put("/user/inBack","拉入黑名单");
        put("/user/outBack","解除黑名单");
    }};
    public static final Map<String,String> whetherMap = new HashMap<String, String>(){{
        put("0","否");
        put("1","是");
    }};



    public static final Map<String,String> interestsStatus = new HashMap<String, String>(){{
        put("0","下架");
        put("1","上架");
    }};


    public static final String API_RESULT_PARAMS_ERROR = "201";



    public static final String APING_TODAY_VISIT_NUM = "APING-TODAY-VISIT-NUM";
    public static final String REDIS_KEY_APING_IMAGE_URL = "REDIS_KEY_APING_IMAGE_URL";

    public static final String REDIS_KEY_BANNER_IMAGE_URL = "REDIS_KEY_BANNER_IMAGE_URL";

    public static final String REDIS_KEY_APING_SYS_PARAM = "REDIS_KEY_APING_SYS_PARAM";
    public static final Long USER_TOKEN_EXPIRE_TIME = 30 * 24 * 3600L;
}
