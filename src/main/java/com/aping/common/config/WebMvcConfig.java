package com.aping.common.config;

import com.aping.platform.requestJson.handler.RequestJsonMethodArgumentResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
import java.util.List;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Resource
    private RequestJsonMethodArgumentResolver requestJsonMethodArgumentResolver;
    @Value("${image.localUrl}")
    private String fileUrl;

    // url为images/**时项目没有即static目录下没有，则会去映射本地的fileUrl找
    public void addResourceHandlers(ResourceHandlerRegistry registry){
        registry.addResourceHandler("/images/**").addResourceLocations("file:/"+fileUrl);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(requestJsonMethodArgumentResolver);
    }
}
