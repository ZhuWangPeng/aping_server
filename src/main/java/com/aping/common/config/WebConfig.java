package com.aping.common.config;

import com.aping.common.filter.TraceIdFilter;
import org.hibernate.validator.HibernateValidator;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.Validation;
import javax.validation.Validator;

@Configuration
public class WebConfig {

    @Bean
    public FilterRegistrationBean filterRegistrationBean(TraceIdFilter traceIdFilter){
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(traceIdFilter);
        filterRegistrationBean.setOrder(0);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    /**
     * 拦截器<br>对方法参数校验
     * @return
     */
    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor(){
        MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
        processor.setValidator(validator());
        return processor;
    }

    @Bean
    public static Validator validator(){
        return Validation
                .byProvider(HibernateValidator.class)
                .configure()
                .failFast(true)//有错误就返回
                .buildValidatorFactory()
                .getValidator();
    }



}
