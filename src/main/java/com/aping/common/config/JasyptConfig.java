package com.aping.common.config;

import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import org.apache.commons.lang3.StringUtils;
import org.jasypt.encryption.StringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

//@Configuration
public class JasyptConfig implements ApplicationContextAware {

    private Logger log = LoggerFactory.getLogger(JasyptConfig.class);
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext = context;
    }

    @Bean("JasyptSMStringEncrytor")
    public StringEncryptor stringEncryptor(){
        StringEncryptor encryptor = new JasyptSMStringEncrytor();
        return encryptor;
    }

    class JasyptSMStringEncrytor implements StringEncryptor {

        @Override
        public String encrypt(String str) {
            Environment environment = applicationContext.getEnvironment();
            String pwd = environment.getProperty("jasypt.encryptor.password","");
            if(StringUtils.isBlank(pwd)){
                log.error("JasyptSMStringEncrytor encrypt error:{}","Jasypt秘钥不存在");
                throw new RuntimeException("Jasypt秘钥不存在");
            }
            SymmetricCrypto crypto = SmUtil.sm4(pwd.getBytes());
            return crypto.encryptHex(str);
        }

        @Override
        public String decrypt(String str) {
            Environment environment = applicationContext.getEnvironment();
            String pwd = environment.getProperty("jasypt.encryptor.password","");
            if(StringUtils.isBlank(pwd)){
                log.error("JasyptSMStringEncrytor decrypt error:{}","Jasypt秘钥不存在");
                throw new RuntimeException("Jasypt秘钥不存在");
            }
            SymmetricCrypto crypto = SmUtil.sm4(pwd.getBytes());
            return crypto.decryptStr(str);
        }
    }

}
