package com.aping.common.handler;

import com.aping.common.constant.SysCodeEnum;
import com.aping.common.exception.BusiException;
import com.aping.common.exception.MyException;
import com.aping.common.model.Result;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.List;

@RestControllerAdvice
public class SysExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(SysExceptionHandler.class);

    /**
     * 405
     * @param ex
     * @return
     */
    @ExceptionHandler({BindException.class})
    public Result handlerSysException(BindException ex) {
        StringBuilder message = new StringBuilder();
        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        for (FieldError error : fieldErrors) {
            message.append(error.getField()).append(error.getDefaultMessage()).append(",");
        }
        message = new StringBuilder(message.substring(0, message.length() - 1));
        return Result.resultInstance().fail(SysCodeEnum.PARAM_ERROR,message);
    }

    @ExceptionHandler(Exception.class)
    public Result handlerSysException(Exception ex) {
        if(ex instanceof MyException) {
            logger.error("自定义异常："+ex);
            MyException e= (MyException) ex;
            if (StringUtils.isNotBlank(e.getErrorCode())) {
                return Result.resultInstance().fail(e.getErrorCode(), ex.getMessage());
            } else {
                return Result.resultInstance().fail(SysCodeEnum.FAIL, e.getMessage());
            }
        }else if(ex instanceof DataAccessException) {
            logger.error("数据库异常："+ex);
            Throwable cause=ex.getCause();
            if(ex.getMessage().contains("ORA-00001")) {
                return Result.resultInstance().fail(SysCodeEnum.FAIL, "该配置项已存在！");
            }else {
                return Result.resultInstance().fail(SysCodeEnum.SQL_ERROR);
            }
        }else if(ex instanceof MissingServletRequestParameterException) {
            logger.info("请求参数不合规:["+ex.getMessage()+"]");
            return Result.resultInstance().fail(SysCodeEnum.PARAM_ERROR,ex.getMessage());
        }else if(ex instanceof HttpMessageNotReadableException) {
            logger.info("请求参数不合规:["+ex.getMessage()+"]");
            return Result.resultInstance().fail(SysCodeEnum.PARAM_ERROR,ex.getMessage());
        }else if(ex instanceof BusiException){//业务异常
            logger.info("业务异常信息:[{}]",ex.getMessage());
            String code = ((BusiException) ex).getCode();
            if(org.apache.commons.lang3.StringUtils.isNotBlank(code)){
                return Result.resultInstance().fail(code,ex.getMessage());
            }else {
                return Result.resultInstance().fail(SysCodeEnum.FAIL,ex.getMessage());
            }
        }else if(ex instanceof MethodArgumentNotValidException){
            logger.error("请求参数校验不通过：{}",ex.getMessage());
            MethodArgumentNotValidException validException = (MethodArgumentNotValidException)ex;
            StringBuilder errMsg = new StringBuilder();
            List<FieldError> errors = validException.getBindingResult().getFieldErrors();
            for(FieldError err : errors){
                if(StringUtils.isNotBlank(err.getDefaultMessage())){
                    errMsg.append(err.getDefaultMessage()).append(",");
                }
            }
            errMsg = new StringBuilder(errMsg.substring(0, errMsg.length() - 1));
            return Result.resultInstance().fail(SysCodeEnum.PARAM_ERROR,errMsg);
        }else if(ex instanceof ConstraintViolationException){
            logger.info("请求参数不合规:[{}]",ex.getMessage());
            return Result.resultInstance().fail(SysCodeEnum.PARAM_ERROR,ex.getMessage());
        } else if (ex instanceof HttpRequestMethodNotSupportedException) {
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            assert attributes != null;
            HttpServletRequest request = attributes.getRequest();
            logger.info("请求Real-IP: {}",request.getHeader("X-Real-Ip"));
            logger.info("请求Address: {}",request.getRequestURL().toString());
            return Result.resultInstance().fail(SysCodeEnum.PARAM_ERROR,ex.getMessage());
        } else {
            logger.error("系统异常：",ex);
            return Result.resultInstance().fail(SysCodeEnum.FAIL);
        }
    }

}
