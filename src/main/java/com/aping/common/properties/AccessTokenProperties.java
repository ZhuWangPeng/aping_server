package com.aping.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "wx.accesstoken")
public class AccessTokenProperties {
    /**
     * 微信公众号或者小程序的appId
     */
    private String appId;

    /**
     * 微信公众号或者小程序的appSecret
     */
    private String  appSecret;

    private String grantType;
}
