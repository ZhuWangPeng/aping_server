package com.aping.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "wx.pay")
public class WxPayProperties {

    /**
     * 设置微信公众号或者小程序等的appId
     */
    private String appId;

    /**
     * 微信支付商户号
     */
    private String mchId;

    /**
     * 微信支付商户密钥
     */
    private String apiV3Key;

    /**
     * apiclient_key.pem文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
     */
    private String privateKeyPath;

    /**
     * apiclient_key.pem文件的绝对路径，或者如果放在项目中，请以classpath:开头指定
     */
    private String privateCertPath;

    /**
     * 支付成功回调地址：v3版本，必须是https
     */
    private String notifyUrl;
}
