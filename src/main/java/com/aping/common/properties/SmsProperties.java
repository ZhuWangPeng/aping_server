package com.aping.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "sms")
public class SmsProperties {
    /**
     * keyId
     */
    private String accessKeyId;

    /**
     * keySecret
     */
    private String accessKeySecret;

    /**
     * 微信支付商户密钥
     */
    private String url;

    private String signName;

    private String templateCode;
}
