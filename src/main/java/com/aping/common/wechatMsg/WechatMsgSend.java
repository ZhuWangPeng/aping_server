package com.aping.common.wechatMsg;

import com.alibaba.fastjson.JSONObject;
import com.aping.common.constant.SysConstant;
import com.aping.common.model.Result;
import com.aping.manage.sysparam.service.SysParamService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <Description> <br>
 * 发送模板消息
 * @author Limao <br>
 * @version 1.0<br>
 * @taskId: <br>

 */
@Component
@Slf4j
public class WechatMsgSend {

    @Resource
    private RestTemplate restTemplate;
//    @Resource
//    private SysParamService sysParamService;

    /**
     * 根据openid发送模板消息，
     * 如果消息内容相同的给多人发送消息，也可以用这个接口，openid用逗号隔开，但是一次的openid不要超过30个。
     * @param wechatMsgBean
     * @return
     */
//    public Result sendTplMsg(WechatMsgBean wechatMsgBean){
//        Result result = Result.resultInstance().success();
//        try{
//            String url = sysParamService.getSysParamRedis("MSG_WECHAT_PATH") + "/wechatMsg/sendTplMsg";
//            log.info(">>>>>>微信信息发送链接-{}<<<<<<",url);
//            log.info("---send ---" + JSONObject.toJSONString(wechatMsgBean));
//            String sendMsgRtn = restTemplate.postForObject(url,wechatMsgBean,String.class);
//            log.info("---return ---" + sendMsgRtn);
//            Map<String,Object> msgMap = JSONObject.parseObject(sendMsgRtn,Map.class);
//            if (msgMap.containsKey("code")){
//                result = JSONObject.parseObject(sendMsgRtn , Result.class);
//            }else{
//                result.code = SysConstant.API_RESULT_MSG_ERROR;
//                result.msg =  sendMsgRtn;
//            }
//
//        }catch (Exception e){
//            log.error("---------send msg error",e);
//            result.code = SysConstant.API_RESULT_MSG_ERROR;
//            result.msg = "---系统异常---"+e.getMessage();
//        }
//         return  result;
//    }
//
//    /**
//     * 根据openid发送模板消息 批量
//     * @param list
//     * @return
//     */
//    public Result sendTplMsgBatch(List<WechatMsgBean> list){
//        Result result = Result.resultInstance().success();
//        try{
//            String url = sysParamService.getSysParamRedis("MSG_WECHAT_PATH") + "/wechatMsg/sendTplMsgBatch";
//            log.info(">>>>>>微信信息发送链接-{}<<<<<<",url);
//            log.info("---send ---" + list.toString());
//            String sendMsgRtn = restTemplate.postForObject(url,list,String.class);
//            log.info("---return ---" + sendMsgRtn);
//            Map<String,Object> msgMap = JSONObject.parseObject(sendMsgRtn,Map.class);
//            if (msgMap.containsKey("code")){
//                result = JSONObject.parseObject(sendMsgRtn , Result.class);
//            }else{
//                result.code = SysConstant.API_RESULT_MSG_ERROR;
//                result.msg =  sendMsgRtn;
//            }
//
//        }catch (Exception e){
//            log.error("---------send msg error",e);
//            result.code = SysConstant.API_RESULT_MSG_ERROR;
//            result.msg = "---系统异常---"+e.getMessage();
//        }
//        return  result;
//    }
}
