package com.aping.common.wechatMsg;

import lombok.Data;

import java.util.List;

/**
 * <Description> <br>
 * 微信模板消息发送类
 * @author Limao <br>
 * @version 1.0<br>
 */
@Data
public class WechatMsgBean {
    private String tranType;//交易类型 非空
    private String openIds;//openid 如果批量发送则用逗号隔开，且保证不超过30个openid 非空
    private String url;//发送消息后客户点击消息跳转的地址，如果不需要点击消息则可为空
    private List<String> contentList;//消息的内容 按照消息模板的字段顺序给值放进list里

}
