package com.aping.common.model;

import com.aping.common.constant.SysCodeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Data
@ApiModel("返回数据统一包装")
public class Result implements Serializable {
    private static final long serialVersionUID = -8602728129469283330L;

    public String code;
    public String msg;
    public Object data;

    public static Result resultInstance(){
        return new Result();
    }

    public Result success(){
        return success(null,null,null);
    }

    public Result success(String msg){
        return success(null,msg,null);
    }

    public Result success(Object data){
        return success(null,null,data);
    }

    public Result success(SysCodeEnum sysCodeEnum){
        return success(sysCodeEnum.getCode(),sysCodeEnum.getMsg(),null);
    }

    public Result success(SysCodeEnum sysCodeEnum,Object data){
        return success(sysCodeEnum.getCode(),sysCodeEnum.getMsg(),data);
    }

    public Result success(String code,String msg){
        return success(code,msg,null);
    }

    public Result fail(){
        return fail(null,null,null);
    }

    public Result fail(String msg){
        return fail(null,msg,null);
    }

    public Result fail(Object data){
        return fail(null,null,data);
    }

    public Result fail(SysCodeEnum sysCodeEnum){
        return fail(sysCodeEnum.getCode(),sysCodeEnum.getMsg(),null);
    }

    public Result fail(String code,String msg){
        return fail(code,msg,null);
    }
    public Result fail(SysCodeEnum sysCodeEnum,Object data){
        return success(sysCodeEnum.getCode(),sysCodeEnum.getMsg(),data);
    }

    public Result success(String code,String msg,Object data){
        if(StringUtils.isBlank(code)){
            this.code = SysCodeEnum.SUCCESS.getCode();
        }else {
            this.code = code;
        }
        if(StringUtils.isBlank(msg)){
            this.msg = SysCodeEnum.SUCCESS.getMsg();
        }else {
            this.msg = msg;
        }
        this.data = data;
        return this;
    }

    public Result fail(String code,String msg,Object data){
        if(StringUtils.isBlank(code)){
            this.code = SysCodeEnum.FAIL.getCode();
        }else {
            this.code = code;
        }
        if(StringUtils.isBlank(msg)){
            this.msg = SysCodeEnum.FAIL.getMsg();
        }else {
            this.msg = msg;
        }
        this.data = data;
        return this;
    }

}
