package com.aping.common.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value = "classpath:redis.properties")
@ConfigurationProperties(prefix = "redis")
public class RedisConfigProperties {
	//集群节点
	private String clusterNodes;
	// 密码没有不填写
	public String password;
	// 最大活跃连接
	public int maxActive;
	// 连接池最大阻塞等待时间（使用负值表示没有限制）
	public int maxWait;
	// 连接池中的最大空闲连接
	public int maxIdle;
	// 连接池中的最小空闲连接
	public int minIdle;
	// 连接/执行命令超时时间（毫秒）
	public int commandTimeout;
	// token生效时间
	public long tokenTime;
	// 重试次数
	private Integer maxAttempts;
	// 跨集群执行命令时要遵循的最大重定向数量
	private Integer maxRedirects;
	// 是否在从池中取出连接前进行检验,如果检验失败,则从池中去除连接并尝试取出另一个
	private boolean testOnBorrow;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getMaxActive() {
		return maxActive;
	}
	public void setMaxActive(int maxActive) {
		this.maxActive = maxActive;
	}
	public int getMaxWait() {
		return maxWait;
	}
	public void setMaxWait(int maxWait) {
		this.maxWait = maxWait;
	}
	public int getMaxIdle() {
		return maxIdle;
	}
	public void setMaxIdle(int maxIdle) {
		this.maxIdle = maxIdle;
	}
	public int getMinIdle() {
		return minIdle;
	}
	public void setMinIdle(int minIdle) {
		this.minIdle = minIdle;
	}
	public int getCommandTimeout() {
		return commandTimeout;
	}
	public void setCommandTimeout(int commandTimeout) {
		this.commandTimeout = commandTimeout;
	}
	public long getTokenTime() {
		return tokenTime;
	}
	public void setTokenTime(long tokenTime) {
		this.tokenTime = tokenTime;
	}
	public Integer getMaxAttempts() {
		return maxAttempts;
	}
	public void setMaxAttempts(Integer maxAttempts) {
		this.maxAttempts = maxAttempts;
	}
	public Integer getMaxRedirects() {
		return maxRedirects;
	}
	public void setMaxRedirects(Integer maxRedirects) {
		this.maxRedirects = maxRedirects;
	}
	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}
	public void setTestOnBorrow(boolean testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}
	public String getClusterNodes() {
		return clusterNodes;
	}
	public void setClusterNodes(String clusterNodes) {
		this.clusterNodes = clusterNodes;
	}
	

}