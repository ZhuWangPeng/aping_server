
package com.aping.common.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：HappyGiraffe
 * @Description：Redis 配置类 @CreateDate：13:48 2018/7/4
 */
@Configuration
@ConditionalOnClass(JedisCluster.class)
public class RedisConfig {

//	@Autowired
//	private RedisConfigProperties redisConfigProperties;
//
//	/**
//	 * 配置 Redis 连接池信息
//	 */
//	@Bean
//	public JedisPoolConfig getJedisPoolConfig() {
//		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
//		jedisPoolConfig.setMaxIdle(redisConfigProperties.getMaxIdle());
//		jedisPoolConfig.setMaxWaitMillis(redisConfigProperties.getMaxWait());
//		jedisPoolConfig.setTestOnBorrow(redisConfigProperties.isTestOnBorrow());
//
//		return jedisPoolConfig;
//	}
//
//	/**
//	 * 配置 Redis Cluster 信息
//	 */
//	@Bean
//	public RedisClusterConfiguration getJedisCluster() {
//		RedisClusterConfiguration redisClusterConfiguration = new RedisClusterConfiguration();
//		redisClusterConfiguration.setMaxRedirects(redisConfigProperties.getMaxRedirects());
//
//		List<RedisNode> nodeList = new ArrayList<>();
//
//		String[] cNodes = redisConfigProperties.getClusterNodes().split(",");
//		// 分割出集群节点
//		for (String node : cNodes) {
//			String[] hp = node.split(":");
//			nodeList.add(new RedisNode(hp[0], Integer.parseInt(hp[1])));
//		}
//		redisClusterConfiguration.setClusterNodes(nodeList);
//
//		return redisClusterConfiguration;
//	}
//
//	/**
//	 * 配置 Redis 连接工厂
//	 */
//	@Bean
//	public JedisConnectionFactory getJedisConnectionFactory(RedisClusterConfiguration redisClusterConfiguration,
//                                                            JedisPoolConfig jedisPoolConfig) {
//		JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory(redisClusterConfiguration,
//				jedisPoolConfig);
//		jedisConnectionFactory.setPassword(redisConfigProperties.getPassword());
//		return jedisConnectionFactory;
//	}

	/**
	 * 设置数据存入redis 的序列化方式 redisTemplate序列化默认使用的jdkSerializeable
	 * 存储二进制字节码，导致key会出现乱码，所以自定义序列化类
	 */
	@Bean
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
		//1-配置连接工厂
		redisTemplate.setConnectionFactory(redisConnectionFactory);
		//2-使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值（默认使用JDK的序列化方式）
		Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
		ObjectMapper objectMapper = new ObjectMapper();
		//指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
		objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		//指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
		objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

		//使用StringRedisSerializer来序列化和反序列化redis的key值
		redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
		//值采用json序列化
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		//设置hash key 和value序列化模式
		redisTemplate.setHashKeySerializer(new StringRedisSerializer());
		redisTemplate.setHashValueSerializer(new StringRedisSerializer());
		redisTemplate.afterPropertiesSet();

		return redisTemplate;
	}

}