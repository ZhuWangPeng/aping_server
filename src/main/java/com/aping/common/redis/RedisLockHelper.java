package com.aping.common.redis;

/**
 * <Description> <br>
 *
 * @author Limao <br>
 * @version 1.0<br>
 * @taskId: <br>
 * @createDate 2020-08-11
 * @see com.boc.wzpj.redis
 */

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Redis设置的缓存锁
 */
@Slf4j
@Component
public class RedisLockHelper {
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    public boolean lock(String targetId,String timeStamp){
        if (stringRedisTemplate.opsForValue().setIfAbsent(targetId,timeStamp)){
            //对应setnx命令，可以成功设置，说明key不存在
            return true;
        }
        //判断锁超时-防止原来的操作异常没有进行解锁操作，防止死锁
        String currentLock = stringRedisTemplate.opsForValue().get(targetId);
        //如果锁过期 currentLock不为空且小于当前时间
        if (StringUtils.isNotEmpty(currentLock) && Long.parseLong(currentLock)<System.currentTimeMillis()){
            //获取上一个锁的时间 对应getset 如果lock存在 返回原来在锁在值，且把当前的值设置进去
            String preLock = stringRedisTemplate.opsForValue().getAndSet(targetId,timeStamp);
            //假设两个线程同时进来这里，因为key被占用，而且锁过期了，获取的值currentLock是旧的，两个新的线程A和B进来了，
            //因为上面的getandset 是顺序执行的，所以两个线程执行以后获取的preLock只有一个是旧的，还有一个是这两个线程之一
            //最先执行getAndSet的获取锁权限
            return StringUtils.isNotEmpty(preLock) && preLock.equals(currentLock);
        }
        return false;
    }

    /**
     * 加锁 等待时间
     * @param targetId
     * @param timeStamp
     * @param acquireTime
     * @return
     */
    public boolean lock(String targetId,String timeStamp,long acquireTime){
        long start = System.currentTimeMillis();
        long end = start + acquireTime;

        try {
            while (end >= System.currentTimeMillis()){
                boolean b = lock(targetId,timeStamp);
                if(b){
                    return b;
                }else {
                    Thread.sleep(10);
                }
            }
        }catch (Exception e){
            log.error("lock error:",e);
        }
        return false;
    }

    public void unlock(String key,String value){
        String currentValue = stringRedisTemplate.opsForValue().get(key);
        try{
            if (StringUtils.isNotEmpty(currentValue) && currentValue.equals(value)){
                stringRedisTemplate.opsForValue().getOperations().delete(key);
            }
            log.info("unlock suc,key:{},value:{}",key,value);
        }catch (Exception e){
            log.error("unlock fail",e);
        }
    }
}
