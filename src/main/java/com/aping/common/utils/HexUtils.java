package com.aping.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


/**
 * 16进制转换工具类
 * @author zhuwangpeng
 *
 */
@Slf4j
public class HexUtils {

	/*
	 * 16进制数字字符集
	 */
	private static final String hexChars = "0123456789ABCDEF";
	
	/*
	 * 将字节数组转成16进制数字字符串
	 */
	public static String byteArrayToHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder(bytes.length * 2);
		// 将字节数组中每个字节拆解成2位16进制整数
		for (byte aByte : bytes) {
			sb.append(hexChars.charAt((aByte & 0xf0) >> 4));
			sb.append(hexChars.charAt((aByte & 0x0f)));
		}
		return sb.toString();
	}

	/*
	 * 将16进制数字字符串转成字节数组
	 */
	public static byte[] hexToByteArray(String hexString) {
		ByteArrayOutputStream baos = null;
		try {
			baos = new ByteArrayOutputStream(hexString.length() / 2);
			// 将每2位16进制整数组装成一个字节
			for (int i = 0; i < hexString.length(); i += 2)
				try {
					baos.write(
							(hexChars.indexOf(hexString.charAt(i)) << 4 | hexChars.indexOf(hexString.charAt(i + 1))));
				} catch (Exception e) {
//					e.printStackTrace();
					log.info(e.getMessage());
				}
			return baos.toByteArray();
		} finally {
			if (null != baos) {
				try {
					baos.close();
				} catch (IOException e) {
//					e.printStackTrace();
					log.info(e.getMessage());
				}
			}
		}
	}
	
}
