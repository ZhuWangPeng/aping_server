package com.aping.common.utils;

import lombok.SneakyThrows;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

public class DESUtil {

    private static final  String ALGORITHMSTR = "DESede/ECB/PKCS5Padding";

    /**
     * 加密
     * @param plainText 需要加密的字符串
     * @param key 秘钥
     * @return 密文
     */
    @SneakyThrows
    public static String encrypt3DESToHex(String plainText, String key) {
        SecureRandom sr = new SecureRandom();
        DESedeKeySpec dks = new DESedeKeySpec(key.getBytes(StandardCharsets.UTF_8));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        SecretKey secretKey = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey,sr);
        byte[] bs = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        return HexUtils.byteArrayToHex(bs);
    }

    /**
     * 解密
     * @param encryptText 解密的密文
     * @param key 秘钥
     * @return 解密后的字符串
     */
    @SneakyThrows
    public static String decrypt3DESToHex(String encryptText,String key) {
        SecureRandom sr = new SecureRandom();
        DESedeKeySpec dks = new DESedeKeySpec(key.getBytes(StandardCharsets.UTF_8));
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
        SecretKey secretKey = keyFactory.generateSecret(dks);

        Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secretKey,sr);
        byte[] bs = cipher.doFinal(HexUtils.hexToByteArray(encryptText));
        return new String(bs, StandardCharsets.UTF_8);
    }

    /**
     * 获取秘钥
     * @return 秘钥
     */
    @SneakyThrows
    public static String generateKey() {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("DESede");
        // DES-56 DESede-112 or 168
        keyGenerator.init(112);
        return HexUtils.byteArrayToHex(keyGenerator.generateKey().getEncoded());
    }

    @SneakyThrows
    public static void test(String text,String key) {
        System.out.println("***************** 3DES秘钥加密解密开始 *****************");
        String text1 = encrypt3DESToHex(text,key);
        System.out.println(key);
        System.out.println("加密前：" + text);
        String text2 = decrypt3DESToHex(text1, key);
        System.out.println("加密后：" + text1);
        System.out.println("解密后：" + text2);
        if (text.equals(text2)) {
            System.out.println("解密字符串和原始字符串一致，解密成功");
        } else {
            System.out.println("解密字符串和原始字符串不一致，解密失败");
        }
        System.out.println("***************** 3DES秘钥加密解密开始 *****************");
    }

    public static void main(String[] args) {
//        System.out.println("\n");
        String key = generateKey();
        System.out.println(key);
//        test("123456",key);
//        String key = "1319BFBF7CB69DAE088C13A8EFB09E1A1319BFBF7CB69DAE";
//        String text = "AC896FD6BB0C0C16B97A8CB26622C7EA6B0DE8F912520101";
//        System.out.println(decrypt3DESToHex(text,key));
//        System.out.println(generateKey());
    }
}
