package com.aping.common.utils;

import com.aping.common.utils.JasyptUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * SpringContext工具类
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContextUtil.applicationContext = applicationContext;
        JasyptUtils.setEnvironment(applicationContext.getEnvironment());
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static <T> T getBeanByClass(Class<T> clazz){
        return applicationContext.getBean(clazz);
    }

    public static <T> T getBeanByName(String beanName,Class<T> clazz){
        return applicationContext.getBean(beanName,clazz);
    }

    public Object getBeanByName(String beanName){
        return applicationContext.getBean(beanName);
    }
}
