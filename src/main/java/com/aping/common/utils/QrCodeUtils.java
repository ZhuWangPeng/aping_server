package com.aping.common.utils;

import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URISyntaxException;
import java.util.*;
import java.util.List;

public class QrCodeUtils {
    private static Logger log = LoggerFactory.getLogger(QrCodeUtils.class);

    /**
     * 生成二维码
     * @param content 二维码内容
     * @return BitMatrix对象
     */
    public static void generateCode(String content,OutputStream outputStream){
        // 二维码的宽高
        int width = 200;
        int height = 200;
        // 其他参数
        Map<EncodeHintType,Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET,"UTF-8");
        // 容错级别H
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        // 白边的宽度 可取0~4
        hints.put(EncodeHintType.MARGIN,1);
        BufferedImage bufferedImage;
        BitMatrix bitMatrix;
        try{
            // 生成矩阵
            bitMatrix = new MultiFormatWriter().encode(content,BarcodeFormat.QR_CODE,width,height,hints);
            // 删除二维码周边
            bufferedImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
            for(int x = 0; x < width; x++){
                for(int y = 0; y < height; y++){
                    bufferedImage.setRGB(x,y,bitMatrix.get(x,y) ? 0x000000 : 0xFFFFFF);
                }
            }
            ImageIO.write(bufferedImage,"png",outputStream);
            log.info(">>>>>二维码图片生成到输出流成功<<<<<");
        }catch (WriterException | IOException e){
            e.printStackTrace();
            log.error("发生错误: {}!",e.getMessage());
        }
//        return bitMatrix;
    }


    private static BitMatrix deleteWhite(BitMatrix bitMatrix){
        int[] rec = bitMatrix.getEnclosingRectangle();
        int resWidth = rec[2] + 1;
        int resHeight = rec[3] + 1;
        BitMatrix resMatrix = new BitMatrix(resWidth,resHeight);
        resMatrix.clear();
        for(int i = 0; i < resWidth; i++){
            for(int j = 0; j < resHeight; j++){
                if(bitMatrix.get(i + rec[0],j + rec[1])){
                    resMatrix.set(i,j);
                }
            }
        }
        return resMatrix;
    }

    /**
     * 生成中间带logo二维码
     * @param content 二维码数据
     * @param width 二维码宽
     * @param height 二维码高
     * @param imgType 文件类型
     * @param logoIs 二维码中间logo图
     * @return
     * @throws Exception
     */
    public static BufferedImage generateLogoImg(String content, Integer width,
                                                Integer height, String imgType,
                                                InputStream logoIs) throws Exception{
        QrConfig qrConfig = new QrConfig();
        qrConfig.setWidth(width);
        qrConfig.setHeight(height);
        qrConfig.setForeColor(Color.BLACK);//二维码颜色
        qrConfig.setBackColor(Color.WHITE);//背景色
        qrConfig.setMargin(1);//二维码距离边框间距
        if(Objects.nonNull(logoIs) && logoIs.available() > 0){
            qrConfig.setImg(ImageIO.read(logoIs));
        }
        BarcodeFormat format = BarcodeFormat.QR_CODE;
        BufferedImage bimage = QrCodeUtil.generate(content,format,qrConfig);
        return ImgUtil.toBufferedImage(bimage,imgType);
    }

    /**
     * 生成底部带文字的二维码
     * @param content 二维码数据
     * @param width 二维码宽
     * @param height 二维码高
     * @param bottomWords 底部文字
     * @param imgType 文件类型
     * @param logoIs 二维码中间logo图
     * @return
     * @throws Exception
     */
    public static BufferedImage generateWordsImg(String content,Integer width,
                                                 Integer height,String bottomWords,
                                                 String imgType, InputStream logoIs) throws Exception{
        float fontWidth = 21.43f;//20大小字体宽度
        float fontHeight = 23.5f;//字体高度

        BufferedImage bgImage;
        //背景画布宽高
        //画布中增加二维码图片
        BufferedImage bufferedImage = generateLogoImg(content,width,height,imgType,logoIs);
        List<String> wordList = new ArrayList<>();
        if(StringUtils.isBlank(bottomWords)){
            //不绘制文字
            bgImage = bufferedImage;
        }else {
            //每行显示个数
            int rowSize = Double.valueOf(Math.ceil(width/fontWidth)).intValue();
            //计算文字行数
            int fontRow = Double.valueOf(Math.ceil(Float.valueOf(bottomWords.length())/Float.valueOf(rowSize))).intValue();
            for(int i=0;i<fontRow;i++){
                int start = i*rowSize;
                int end = (i+1)*rowSize;
                wordList.add(StringUtils.substring(bottomWords,start,end));
            }
            //根据文字计算画布高度
            //计算文字填充后的高度
            int bgHeight = height + Double.valueOf(Math.ceil(fontHeight*fontRow)).intValue();
            //整块背景画布
            bgImage = new BufferedImage(width,bgHeight,BufferedImage.TYPE_INT_RGB);
            Graphics2D bg2d = bgImage.createGraphics();
            bg2d.setColor(Color.WHITE);
            bg2d.fillRect(0,0, width,bgHeight);
            bg2d.drawImage(bufferedImage,0,0,width,height,null);
        }

        //画底部文字
        Graphics2D bottom2d = null;
        if(!wordList.isEmpty()){
            bottom2d = bgImage.createGraphics();
            //对线段锯齿状边缘处理
            bottom2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            setFont(bottom2d);
        }
        int index = 0;
        for(String word : wordList){
            //设置高度
            //文字居中 首文字左边距计算
            float marginLeft = (width -word.length()*fontWidth)/2;
            bottom2d.drawString(word,marginLeft,height + 10 + fontHeight*index);
            index++;
        }
        if(Objects.nonNull(bottom2d)){
            bottom2d.dispose();
        }
        return ImgUtil.toBufferedImage(bgImage,imgType);
    }

    //设置字体库
    private static void setFont(Graphics2D graphics2D) throws IOException, FontFormatException, URISyntaxException {
        //文字颜色
        graphics2D.setColor(Color.BLACK);
        //字体
        InputStream fontIs = getFontIs();
        Font font = Font.createFont(Font.TRUETYPE_FONT,fontIs);
        fontIs.close();
        font = font.deriveFont(Font.PLAIN,20);
        graphics2D.setFont(font);
    }

    //加载字体库文件
    private static InputStream getFontIs(){
        ClassPathResource classPathResource = new ClassPathResource("font"+ File.separator+"simhei.ttf");
        return classPathResource.getStream();
    }

    /**
     * 生成有背景图的二维码
     * @param content 二维码数据
     * @param width 二维码宽
     * @param height 二维码高
     * @param bottomWords 二维码底部文字信息
     * @param imgType 生产的二维码类型
     * @param logoIs 二维码中间logo图
     * @param bgIs 二维码背景图
     * @param positionX 二维码所在背景图X坐标
     * @param positionY 二维码所在背景图Y坐标
     * @return
     * @throws Exception
     */
    public static BufferedImage generateBackgroundImg(String content,Integer width,
                                                      Integer height,String bottomWords,
                                                      String imgType, InputStream logoIs,
                                                      InputStream bgIs,Integer positionX,
                                                      Integer positionY) throws Exception{
        //整个背景图片画布
        BufferedImage bgImage = ImageIO.read(bgIs);
        Graphics qrGraphics = bgImage.getGraphics();
        //二维码图像
        BufferedImage qrImage = generateWordsImg(content,width,height,bottomWords,imgType,logoIs);
        qrGraphics.drawImage(qrImage,positionX,positionY,null);
        qrGraphics.dispose();
        return ImgUtil.toBufferedImage(bgImage,imgType);
    }

    public static void main(String[] args) {
        String content = "http://www.baidu.com";
//        qrConfig.setImg("d://boc-logo.png");
        File outfile = new File("d://aa.png");
        try {
            FileInputStream logoFis = new FileInputStream(new File("d://boc-logo.png"));
            FileInputStream bgFis = new FileInputStream(new File("d://bg.jpg"));

            FileOutputStream fos = new FileOutputStream(outfile);
//            BufferedImage bfi = generate(content,300,300,"png");
            BufferedImage bfi = generateBackgroundImg(content,300,300,"这是我司推荐码,您可以扫一扫,或者长安识别进行分享","png",logoFis,bgFis,350,750);
//            BufferedImage bfi = generateWordsImg(content,300,300,"这是我司推荐码,您可以扫一扫,或者长安识别进行分享","png",logoFis);
            ImageIO.write(bfi,"png",fos);
            fos.flush();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

