package com.aping.common.utils;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.StringTokenizer;
import java.util.UUID;

public class StringUtil {
	
	

	/**
	 * 'userEnname':'vcpName';depositInfo{'lmtamt':'cardSeq','reqHeader.userid':'custNo'}
	 * 
	 * @param jsonStr
	 * @return String[0]= depositInfo String[1]={
	 *         'userEnname':'vcpName','lmtamt':'cardSeq','reqHeader.userid':'custNo'
	 *         }
	 */
	public static String[] parseJsonArrayKeyAndValue(String jsonStr) {

		StringTokenizer st = new StringTokenizer(jsonStr, ";");

		String[] strs = new String[2];

		StringBuilder strValues = new StringBuilder();

		while (st.hasMoreTokens()) {
			String s = st.nextToken();
			if (s.contains("{") && s.contains("}")) {
				strs[0] = s.substring(0, s.indexOf("{"));
				strValues.append(",").append(
						s.substring(s.indexOf("{") + 1, s.indexOf("}")));
			} else {
				strValues.append(",").append(s);
			}
		}
		strs[1] = "{" + strValues.toString().substring(1) + "}";
		return strs;
	}
	
	
	/**
	 * @param arg
	 * @return
	 */
	public static boolean isEmpty(String arg){
		if(null == arg || "".equals(arg.trim())){
			return true;
		}
		return false;
	}

	/**
	 * @param arg
	 * @return
	 */
	public static boolean isNotEmpty(String arg){
		if(null == arg || "".equals(arg.trim())){
			return false;
		}
		return true;
	}
	
	/**
	 * 左补0到length位数
	 * @param obj
	 * @param length
	 * @return
	 */
	public static String appendLeftZero(Object obj, int length) {
		String str = getString(obj);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < (length - str.length()); i++) {
			sb.append("0");
		}
		sb.append(str);
		return sb.toString();
	}
	
	
	// 返回obj对象字符串
	public static String getString(Object obj) {
		if (obj == null) {
			return "";
		}
		return obj.toString();
	}
	
	/**
	 * 替换字符串中的{x}
	 * x可以是字符串或数字
	 * 替换是按照{}出现的顺序，与{}中的内容无关
	 * @param value
	 * @param args
	 * @return
	 */
	public static String fillingValue(String value, Object[] args) {
		String result = value;
		if(args == null || args.length == 0) {
			return result;
		}
		
		for (Object arg : args) {
			if(arg instanceof CharSequence) {
				result = result.replaceFirst("\\{[^\\{]*\\}", arg.toString());
			}
		}
		return result;
	}
	
	/**
	 * 如果字符串中包含了回车、和换行符，则替换成空字符
	 * 去掉base64编码时出现的回车和换行符
	 * @param arg
	 * @return
	 */
	public static String replaceBaseStr(String arg) {
		return getString(arg).replaceAll("[\\n\\r]", "");
	}
	/**
	 * 如果字符串中包含了回车、制表符和换行符，则替换成空字符
	 * @param arg
	 * @return
	 */
	public static String replaceStr(String arg){
		return getString(arg).replaceAll("[\\t\\n\\r]","");
	}
	
	/**
	 * 如果参数为null，返回""，否则原样返回
	 * 
	 * @param str
	 * @return
	 */
	public static String getString(String str) {
		return str == null ? "" : str;
	}
    /**
	 * 获取系统表b_system_properties对应参数值
	 * @param sourValue
	 * @param destValue
	 * @return
	 */
	public static String getSystemProValue(String sourValue,String destValue){
		
		if(isNotEmpty(destValue))
		{
			return destValue;
		}
		return getString(sourValue);
	}
	
	/**
	 * Base64编码
	 * @param bytes 要编码的字节数组
	 * @return String 编码后的字符�?
	 */
	public static String encodeBase64(byte[] bytes) {

		try {
			return new String(Base64.encodeBase64(bytes, false),"utf-8");
		} catch (UnsupportedEncodingException e) {
			return new String(Base64.encodeBase64(bytes, false),Charset.defaultCharset());
		}
	}

	/**
	 * Base64解码
	 * @param conent 要解码的字符�?
	 * @return byte[] 解码后的字节数组
	 */
	public static byte[] decodeBase64(String conent) {
		byte[] bytes = null;
		try {
			bytes = conent.getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			bytes = conent.getBytes(Charset.defaultCharset());
		}
		bytes = Base64.decodeBase64(bytes);
		return bytes;
	}
	
	/**
	 * 生成AES密钥,密钥大于16位截取16位，小于16位末尾补0
	 *
	 * @param
	 * @return SecretKeySpec AES密钥
	 * @throws UnsupportedEncodingException
	 */
	public static String createSecretKey() throws UnsupportedEncodingException {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		StringBuilder sb = new StringBuilder();
		sb.append(uuid);
		while (sb.length() < 16) {
			sb.append("0");
		}
		if (sb.length() > 16) {
			sb.setLength(16);
		}

		return sb.toString();
	}
	
	// 去掉左边的0
	public static String leftZeroOff(String str) {
		if (str == null || "".equals(str.trim())) {
			return "";
		}
		String newStr = str.trim();
		int strLen = newStr.length();
		for (int i = 0; i < strLen; i++) {
			if ("0".equals(String.valueOf(newStr.charAt(i)))) {
				newStr = newStr.substring(1, strLen);
			} else {
				break;
			}
			strLen = newStr.length();
			i--;
		}

		return newStr;
	}
}
