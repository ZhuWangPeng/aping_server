package com.aping.common.utils;

import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

/**
 * Jasypt工具类
 */
public class JasyptUtils {
    private static Logger logger = LoggerFactory.getLogger(JasyptUtils.class);

    public static Environment environment;

    public static void setEnvironment(Environment environment) {
        JasyptUtils.environment = environment;
    }

    /**
     * 加密 秘钥环境变量中获取
     * @param plainText
     * @return
     */
    public static String encryptDES(String plainText){
        return encryptDES(plainText,null);
    }

    /**
     * 加密
     * @param plainText 加密明文
     * @param encryKey 秘钥
     * @return
     */
    public static String encryptDES(String plainText,String encryKey){
        if(StringUtils.isBlank(plainText)){
            throw new RuntimeException("参数不可为空");
        }
        String pwd = encryKey;
        if(StringUtils.isBlank(encryKey)){
            pwd = environment.getProperty("jasypt.encryptor.password","");
        }
        SymmetricCrypto crypto = SmUtil.sm4(pwd.getBytes());
        return crypto.encryptHex(plainText);
    }

    /**
     * 解密 秘钥环境变量中获取
     * @param encryText 密文
     * @return
     */
    public static String decryptDES(String encryText){
        return decryptDES(encryText,null);
    }

    /**
     * 解密
     * @param encryText 密文
     * @param encryKey 秘钥
     * @return
     */
    public static String decryptDES(String encryText,String encryKey){
        if(StringUtils.isBlank(encryText)){
            throw new RuntimeException("参数不可为空");
        }
        String pwd = encryKey;
        if(StringUtils.isBlank(encryKey)){
            pwd = environment.getProperty("jasypt.encryptor.password","");
        }
        SymmetricCrypto crypto = SmUtil.sm4(pwd.getBytes());
        return crypto.decryptStr(encryText);
    }

    public static void main(String[] args) throws Exception{
//        String str1 = "wz01",key = "boctest@o1234567";
//        System.out.println("加密数据:"+str1+"秘钥:"+key);
//        String encryText = encryptDES(str1,key);
//        System.out.println("加密后的密文:"+encryText);
//
//        System.out.println("解密的密文:"+encryText+",秘钥:"+key);
//        System.out.println("解密后明文:"+decryptDES(encryText,key));
    }

}
