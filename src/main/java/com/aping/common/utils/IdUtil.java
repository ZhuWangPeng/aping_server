package com.aping.common.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;

import java.util.Date;
import java.util.UUID;

public class IdUtil {
    /**
     * 生成uuid
     * @return uuid
     */
    public static String getDateStr(){
        return DateUtil.format(new Date(),"yyMMddHHmmss");
    }

    public static String getUUID(){
        return DateUtil.format(new Date(),"yyMMddHHmmss") +
                UUID.randomUUID().toString().replace("-", "").substring(0,15);
    }

    /**
     * 生成userId
     * @return id
     */
    public static String getUserId(){
        return RandomUtil.randomNumbers(12);
    }
    public static String getSpecLengthId(int length){
        return DateUtil.format(new Date(),"yyMMddHHmm") + RandomUtil.randomNumbers(length);
    }


    public static String getImgId(){
        return "IMG_"+ DateUtil.format(new Date(),"yyMMddHH") + RandomUtil.randomNumbers(8);
    }

    public static String getRandomNum(int length){
        return  RandomUtil.randomNumbers(length);
    }

}
