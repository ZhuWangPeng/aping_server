package com.aping.common.utils;

import java.util.UUID;

public class UUIDUtils {
    public static String[] chars = new String[]{
            "a","b","c","d","e","f","g","h","i","j","k","l","m",
            "n","o","p","q","r","s","t","u","v","w","x","y","z",
            "A","B","C","D","E","F","G","H","I","J","K","L","M",
            "N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
    };

    public static String getUUID(){
        // 调用Java提供生成随机字符串的对象:32位,16进制,中间包含-
        String uuid = UUID.randomUUID().toString().replace("-","");
        StringBuilder buffer = new StringBuilder();
        for(int i = 0; i < 8;i++){ // 分为8组
            String str = uuid.substring(i * 4,i * 4 + 4); // 每组4位
            int x = Integer.parseInt(str,16); // 输出str在16进制下的表示
            buffer.append(chars[x % 0x34]); // 用该16进制数取模62(16进制表示为314(14即E))
        }
        return buffer.toString(); // 生成8位字符
    }
}
