package com.aping.common.utils;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.aping.common.model.Result;
import com.google.common.base.Joiner;
import org.apache.poi.hpsf.Decimal;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CommonUtil {

    public static <T> List<List<T>> split(Collection<T> list, int split) {
        List<List<T>> result = new ArrayList<List<T>>();
        int count = 0;
        List<T> cList = new ArrayList<>();
        for (T t : list) {
            cList.add(t);
            ++count;
            if (count >= split) {
                count = 0;
                result.add(cList);
                cList = new ArrayList<>();
            }
        }
        if (count > 0) {
            result.add(cList);
        }
        return result;
    }

    /**
     * @author Hz
     * @Date 2020-06-16 16:43
     */
    public static <K, V> List<Map<K, V>> splitMap(Map<K, V> map, int split) {
        List<Map<K, V>> result = new ArrayList<Map<K, V>>();
        int count = 0;
        HashMap<K, V> hashMap = new HashMap<>();
        for (Map.Entry<K, V> ttEntry : map.entrySet()) {
            hashMap.put(ttEntry.getKey(), ttEntry.getValue());
            ++count;
            if (count >= split) {
                count = 0;
                result.add(hashMap);
                hashMap = new HashMap<>();
            }
        }
        if (count > 0) {
            result.add(hashMap);
        }
        return result;
    }

    /**
     * 获取当前用户总行工号
     *
     * @return
     */
    public static String getAdminId(String secret) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            return null;
        }
        String adminToken =  attributes.getRequest().getHeader("adminToken")==null ?
                attributes.getRequest().getParameter("adminToken"):attributes.getRequest().getHeader("adminToken");
        Map map =  JSONObject.parseObject(DESUtil.decrypt3DESToHex(adminToken,secret), HashMap.class);
        return map.get("adminId").toString();
    }
    /**
     * 获取当前菜单id
     *
     * @return
     */
    public static String getCurrentPerId() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            return null;
        }
        return attributes.getRequest().getHeader("perId");
    }
    /**
     * 获取当前客户端Ip
     *
     * @return
     * @author xym
     */
    public static String getUserIp() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            return "";
        }
        String ip = attributes.getRequest().getHeader("x-forwarded-for");
        if (StringUtils.isEmpty(ip)) {
            return "";
        }
        return ip.split(",")[0];
    }

    // /**
    // * 获取当前客户端Ip
    // * @author szp
    // * @Date 2019-12-20 18:06
    // * @return
    // */
    // public static String getCurrentUserIp() {
    // ServletRequestAttributes
    // attributes=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
    // HttpServletRequest request = attributes.getRequest();
    // if(attributes==null) {
    // return null;
    // }
    // return getIp(request);
    // }

    // //获取ip地址
    // private static String getIp(HttpServletRequest request) {
    // String ip = request.getHeader("x-forwarded-for");
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("Proxy-Client-IP");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("WL-Proxy-Client-IP");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("HTTP_CLIENT_IP");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    // }
    // if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
    // ip = request.getRemoteAddr();
    // }
    // return ip;
    // }

    /**
     * list转逗号隔开String
     *
     * @author Hz
     * @Date 2019-10-24 20:06
     */
    public static String listToString(List<String> list) {
        if (!CollectionUtils.isEmpty(list)) {
            return Joiner.on(",").join(list);
        } else {
            return null;
        }
    }

    public static List<String> stringToList(String str) {
        return Arrays.asList(str.split(","));
    }


        /**
		 * set转逗号隔开String
		 *
		 * @param list
		 * @return
		 */
    public static String setToString(Set<String> list) {
        if (!CollectionUtils.isEmpty(list)) {
            return Joiner.on(",").join(list);
        } else {
            return null;
        }
    }

    public static String getCurrentUserWorkDeptId() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (attributes == null) {
            return null;
        }
        return attributes.getRequest().getHeader("_user_work_dept_id");
    }


    /**
     * 生成权益id
     * @return id
     */

    /**
     * 系统当前日期
     * @return 日期
     */
    public static String getSeverDate(){
        return DateUtil.format(new Date(),"yyyy-MM-dd");
    }

    /**
     * 系统当前时间
     * @return 时间
     */
    public static String getSeverTime(){
        return DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss");
    }

    public static String getFinishOrderTime(){
        return DateUtil.format(DateUtil.offsetDay(new Date(),1),"yyyy-MM-dd HH:mm:ss");
    }

    public static String getConfirmReceiptTime(String time){
        return DateUtil.format(DateUtil.offsetDay(DateUtil.parse(time,"yyyy-MM-dd HH:mm:ss"),7),"yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 生成特定成都id
     * @return id
     */
    public static String getSpecLengthId(int length){
        return DateUtil.format(new Date(),"yyMMddHHmm") + RandomUtil.randomNumbers(length);
    }

    public static String getSuccessTime(String payTime){
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = df.parse(payTime);
            SimpleDateFormat df1 = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
            Date date1 = df1.parse(date.toString());
            DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return df2.format(date1);
        }catch (ParseException e){
            e.printStackTrace();
            return CommonUtil.getSeverTime();
        }
    }

    public static String getOrderExpireTime(){
        return DateUtil.offsetMinute(new Date(),30).toString();
    }

    public static void main(String[] args) {
//        Date nowDate = DateUtil.date();
//        int temp = DateUtil.compare(DateUtil.parse("2022-10-01 16:07:22","yyyy-MM-dd HH:mm:ss"),nowDate);
//        System.out.println(temp);
//        DecimalFormat df = new DecimalFormat("0.00");
//        double d1 = 12.50/100.00;
//        String a = "80.95";
//        double d2 = Double.parseDouble(a);
//        double d3 = d2 * d1;
//        String result = df.format(d3);
//        System.out.println(result);
        List<String> list = new ArrayList<String>(){{
            add("10");
            add("20");
        }};
        int randomInt = RandomUtil.randomInt(0,list.size()-1);
        System.out.println(randomInt);
        System.out.println(list.get(randomInt));
    }


}
