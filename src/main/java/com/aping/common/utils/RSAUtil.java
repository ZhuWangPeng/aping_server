package com.aping.common.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;


public class RSAUtil {

    /**
     * 加密算法RSA
     */
    private static final String KEY_ALGORITHM = "RSA";

    /**
     * 签名算法
     */
    private static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

    /**
     * 获取公钥的key
     */
    private static final String PUBLIC_KEY = "RSAPublicKey";

    /**
     * 获取私钥的key
     */
    private static final String PRIVATE_KEY = "RSAPrivateKey";

    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    /**
     * RSA 位数 如果采用2048 上面最大加密和最大解密则须填写:  245 256
     */
    private static final int INITIALIZE_LENGTH = 1024;

    /**
     * 生成密钥对(公钥和私钥)
     */
    public static Map<String, Object> genKeyPair() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        keyPairGen.initialize(INITIALIZE_LENGTH);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
        Map<String, Object> keyMap = new HashMap<String, Object>(2);
        keyMap.put(PUBLIC_KEY, publicKey);
        keyMap.put(PRIVATE_KEY, privateKey);
        return keyMap;
    }

    /**
     * 用私钥对信息生成数字签名
     * @param data 已加密数据
     * @param privateKey 私钥(BASE64编码)
     */
    public static String sign(byte[] data, String privateKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initSign(privateK);
        signature.update(data);
        return Base64.encodeBase64String(signature.sign());
    }

    /**
     * 校验数字签名
     * @param data 已加密数据
     * @param publicKey 公钥(BASE64编码)
     */
    public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(publicKey);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PublicKey publicK = keyFactory.generatePublic(keySpec);
        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
        signature.initVerify(publicK);
        signature.update(data);
        return signature.verify(Base64.decodeBase64(sign));
    }

    /**
     * 私钥解密
     * @param encryptedData 已加密数据
     * @param privateKey 私钥(BASE64编码)
     */
    public static byte[] decryptByPrivateKey(byte[] encryptedData, String privateKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, privateK);
        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();
        return decryptedData;
    }

    /**
     * 公钥解密
     * @param encryptedData 已加密数据
     * @param publicKey 公钥(BASE64编码)
     */
    public static byte[] decryptByPublicKey(byte[] encryptedData, String publicKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(publicKey);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicK = keyFactory.generatePublic(x509KeySpec);
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.DECRYPT_MODE, publicK);
        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();
        return decryptedData;
    }

    /**
     * 公钥加密
     * @param data 源数据
     * @param publicKey 公钥(BASE64编码)
     */
    public static byte[] encryptByPublicKey(byte[] data, String publicKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(publicKey);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key publicK = keyFactory.generatePublic(x509KeySpec);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, publicK);
        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(data, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();
        return encryptedData;
    }

    /**
     * 私钥加密数据
     * @param data 源数据
     * @param privateKey 私钥(BASE64编码)
     */
    public static byte[] encryptByPrivateKey(byte[] data, String privateKey) throws Exception {
        byte[] keyBytes = Base64.decodeBase64(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
        Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
        cipher.init(Cipher.ENCRYPT_MODE, privateK);
        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(data, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();
        return encryptedData;
    }

    /**
     * 获取私钥
     * @param keyMap 密钥对
     */
    public static String getPrivateKey(Map<String, Object> keyMap) throws Exception {
        Key key = (Key) keyMap.get(PRIVATE_KEY);
        return Base64.encodeBase64String(key.getEncoded());
    }

    /**
     * 获取公钥
     * @param keyMap 密钥对
     */
    public static String getPublicKey(Map<String, Object> keyMap) throws Exception {
        Key key = (Key) keyMap.get(PUBLIC_KEY);
        return Base64.encodeBase64String(key.getEncoded());
    }

    /**
     * 获取公钥加密后字符串
     */
    public static String encryptedData(String data, String publicKey) {
        try {
            data = Base64.encodeBase64String(encryptByPublicKey(data.getBytes(StandardCharsets.UTF_8), publicKey));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    /**
     * 获取私钥解密后字符串
     */
    public static String decryptData(String data, String privateKey) {
        String temp = "";
        try {
            byte[] rs = Base64.decodeBase64(data);
            temp = new String(RSAUtil.decryptByPrivateKey(rs, privateKey), StandardCharsets.UTF_8);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }



    public static void main(String[] args) throws  Exception{
        System.out.println("***************** RSA秘钥加密解密开始 *****************");
//        Map<String, Object> map = RSAUtil.genKeyPair();
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRcbTVRGKDBftI9Eup6Wmj1C/QQf5Tej9RxZS39NAYeT/yDRIBsloDIFJBhpRIjkxU9yqswdEvPaLqkamYSjtxqUElbTRvqXKpoxXpuLUQSVJkf/KQxtlbqcax4sW2GtfQd4TwTm+S/oymeBwRzqwfC4bUFcYulFJ2DxPqAgH+7wIDAQAB";
        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJFxtNVEYoMF+0j0S6npaaPUL9BB/lN6P1HFlLf00Bh5P/INEgGyWgMgUkGGlEiOTFT3KqzB0S89ouqRqZhKO3GpQSVtNG+pcqmjFem4tRBJUmR/8pDG2VupxrHixbYa19B3hPBOb5L+jKZ4HBHOrB8LhtQVxi6UUnYPE+oCAf7vAgMBAAECgYBqnRcZXc94i+HvUGRGYLr3RIrn9fgf6PSRVGv3jJUd24NsQvh4Fey1bfPd5HC93tFFVwkJKenU3zGVEMFoyMFZLNdqNMAc9AWuVGn4cXAv+zV9ODsgF38k+uPyRRrQerbx5H7cg48PKwT/MZ81BtS4WDUM1qbyk3zRMDD7MnUqAQJBAMcyKKbZNESu21Uj7pezRc+RkM543+OExMCdY53ixRtdSMgg2kl+HkhVowU2ovhX390ZJ0THr5DgKwDtrO+6/88CQQC664X7dmEDzr/iTM1TLjR7LGz24IP7wxuyKYi19bZD8oMa8f/saUcK8YNOlUBAeKh/JJ778jY6qDJYW87PFrbhAkAFsJkYqvujOptpQy19h9FmrHHGdA1xIkm8dsJxsBiRP4jkrouNrdyPlGKjs3vUPC5Y0m8ct9d7++s+i4t1DZJhAkBqKuxm784aw4vplYXqAQx+CbW6Y7uTfDZ283BbKbzkyxH8dAfQ19sqNBNIxy0/cUkDLmG+6Mw0dORjm4HTzisBAkEAhj32Vrcm+V73V3FNA81CX4TpT61TkxkHXbGNs3YlQMWpEW9m0RURoAoSiBSmRGanGkPLaam4KpCIHCe4G2u/Dg==";
//        String wechatPrivateKey = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC82He2ANwuLm7vNnLYFRo74HBML+SxGRjcTvCYbRufwqLyfw7pHZMZqXl5W23aM7Ai+c19sHWegYdAdzl0mnoVh4Q6fgRg68mFKwnnuQS5KVA7kRqKRkrpzjIOCXWx1b7fyz7bWivc9oFYKamlRZO1R0SZVUbNsMvA2+ShzIT7DkyD+v8R9AhdppXxp6m1XrOuQ1yHqGAeuOm0k5pzOcGkjWzy5NTuZTI4or37pOikFXK9MBlFXi4s44qdni+j/KDeOEu+ojkhwMCDkb5wnPA79u2NapU18C+K8tdYrBtazjUwRrrqPt1sFGq+XbngpT8nUxl7FM8+wHysG32dLMI/AgMBAAECggEAUxLu5pPOSEqI5paPQttVpQ2AmSNgeT/PHetkD8m/ozIeOIsIZihqc5mIJIjB9IFUdK5rZWVg+I+GjidkkRT7q8jntEuHZZOUHQ7n1pYfq8yuzOVyRW3SurPIdUBYFBkxVIW2awxpy1RtZfk99cR7y/1EAifvc8DYgUi6dSrhPQc0Nyq8T31BIuQTzP9d51JVF0yVZf+phI/DtCS0XWCGYP59VmmesofPfj3DC9bEvKT5/hJFlNd2nIsyJM62Xumcj1hQKbE4hPAZxmBOAeJfzOS1oB8sR4J2cJMNyioFxunXUNjL4fQlIrPTM9Hq7QgDpVY53Rp8W9TsppWEUWMe8QKBgQD6GlJKCAZZi/u6qU2Nb+RaSc5DTWPRU/uxnrJ9kZIRKjBSdRgwy9b+R2lkPyie2owldpbTyU+MrL2kTiyfT3gcMU4JK+3jCKLeEvVu6Y1K69lsZadggNqf8muLyIOTRqK4jK1dJyYzxXmrIvVHgbfTDARhOzOINMdOUfFePujnUwKBgQDBTGIeYoP96uR/Ppd/Cto6Va9BBV8KiV+koYjhVXkgb86VE2CVduhUvkLyZgj0USqfGen9jLTBicsE8e7zm5uy5PSixmg8ZiamupzgI8P+eJWI7S9x8/sitFgR7GbU5all6zikiz4/2euW7HgalAl+YwC6Yvge5nGDfprnWPE35QKBgQDMRx15XE6ba2vDja9LW93/JRN1A0wJWPhWnpGZYIIWxQfNAdktWgETlg5uE+xAu68GlnMhLudrtlf7UOK/iLBJDrh3Y0sdShuFGaUf32FJlZP0Jp9sgjKPu/i9ppxZebNE25pqYKSiWqRC9P20Vy5nEgStyiYQFVPJE8L6wJWJAwKBgQCaMEjQPftVr2oHSjFkaI69G55JenRenkgJtcTPIfQV+LnL1oIjl48qEf6rBtM7uAcMlIo+8184RqcIUcgu0EB5igLOWP/Kth08v/zlGgJ8WUnGyJQhDOKVw0/BNFaummfu4Zv0PRIxsq4nXIdNhCqyg1yQUOPlGiMQJgyAYEVCNQKBgQDRIgLqneEyXw7CpyKGWXloamXq8IARSYXzcxmZy8Fnyy+iZfFPQoJmQjINCREHLEu/YwwnkK3k6pc5vkubgQYhgDozCH0wI+hETmr4X5Xhcl+5ahvWJ0lrGixDvl0aN7qtpfs92+tPPlLcxHnwuXgN3bhPAo7ghi2AJpnix6X1Mg==";
//        String publicKey = ""
//        String privateKey = "";
//        System.out.println("公钥: publicKey = " + publicKey);
//        System.out.println("私钥: privateKey = " + privateKey);
        Map<String,String> map = new HashMap<>();
//        map.put("timeStamp","144643222");
//        map.put("nonceStr","kvn0hqu3buc");
//        map.put("canonicalUrl","/v3/certificates");
//        map.put("method","GET");
//        map.put("body",null);
        map.put("rechargeFlowId","22103019043f471e182cc8475792edc5559bd49fae");
        map.put("wechatOrderId","999999");
        String str = "221027202209011102";
//        String str = JSONObject.toJSONString(map);
        String publicData = encryptedData(str,publicKey);
//        String sign = sign(encryptByPrivateKey(data.getBytes(),privateKey),privateKey);
        System.out.println("公钥加密后: data = " + publicData);
//        System.out.println("私钥签名: sign = " + sign);
//        String privateData = decryptData(publicData,privateKey);
//        System.out.println("私钥解密后: data = " + privateData);
//        Map<String,String> map1 = JSONObject.parseObject(privateData,Map.class);
//        System.out.println(map1);
//        String sb = "wx73096759239dbd32" + "\n" + map1.get("timeStamp") + "\n" +
//                map1.get("nonceStr") + "\n" + map1.get("package") + "\n";
//        String sign = sign(sb.getBytes(),wechatPrivateKey);
//        System.out.println("微信签名"+sign);
//        System.out.println("签名验证: " + verify(encryptByPrivateKey(data.getBytes(),privateKey),publicKey,sign));
    }
}
